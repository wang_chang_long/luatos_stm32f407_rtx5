#include "driver_log.h"
#include <stdio.h>
#include <stdarg.h>
#include "periph_uart.h"
static uint8_t dbg_buff[256] __attribute__((section("ccmram")));											//调试数据缓存数组
static uint8_t debug_level = 0XFF;										//调试输出等级


/**
  * @brief  调试接口初始化
  * @note    
	* @param  无
  * @retval 无
  */
void drv_debug_init(void)
{
	per_uart_init(DEBUG_UART,115200,'N',8,1);
}

/**
  * @brief  设置调试数据输出等级.
  * @note    
	* @param  lev:输出等级0-0xff共9个等级
  * @retval 输出等级
  */
uint8_t drv_set_debug_level(uint8_t lev)
{
	debug_level = lev;
	return debug_level;
}

/**
  * @brief  查询调试数据输出等级.
  * @note    
	* @param  None
  * @retval 输出等级
  */
uint8_t drv_get_debug_level(void)
{
	return debug_level;
}

/**
  * @brief  调试数据串口输出.
  * @note    
	* @param  lev:输出等级
	*					format:输出格式
  * @retval None
  */
void drv_uart_debug(uint8_t lev, const char *format,...)
{
	uint16_t i=0;
	if(debug_level & lev)
	{
		va_list args;
		va_start(args,format);
		i=vsprintf((char *)dbg_buff,format,args);
		va_end(args);
		per_uart_send_data(DEBUG_UART,dbg_buff,i);
	}
	else
		return;
}

