#include "driver_at.h"
#include "cmsis_os2.h"
#include "common.h"
#include "periph_uart.h"
#include "string.h"

/**
	* @brief  发送数据后等待数据接收
  * @note   当正确接收到数据后将数据缓冲区返回
	* @param  _ackstr ：数据返回对比字符串
	*					_tick_timeOut ：等待时长，当值为0时表示无限等待
  * @retval 接收到的数据包
  */
static uint8_t *drv_at_wait_respond(drv_at_desc_t *desc,char *_ackstr, uint16_t _tick_timeOut)
{
	uint8_t *msg=NULL;
	uint32_t startostick=0;
	uint16_t recv_len=0;

	startostick=osKernelGetTickCount();
	while(1)
	{
		if(com_diff_time(startostick,osKernelGetTickCount())>=_tick_timeOut)
		{
			break;
		}
		msg = per_uart_wait_recv(desc->index,_tick_timeOut,&recv_len);

		if((msg != NULL)&&(recv_len > 0))
		{
			if(strstr((char *)msg,_ackstr) != NULL)
			{
				return msg;
			}
		}
	}
	return NULL;
}

void drv_at_init(drv_at_desc_t *desc,per_uart_index_t index,uint32_t bau, uint8_t even, uint8_t dat_len, uint8_t stop_bits)
{
	desc->index = PER_UART_INDEX_BLE;
	desc->outbuff = NULL;
	desc->echo_expect = NULL;
	desc->outbuff_size = 0;
	desc->status = AT_ECHO_STATUS_NONE;
	per_uart_init(index,bau,even,dat_len,stop_bits);
}

int drv_at_echo_create(drv_at_desc_t *desc,uint8_t *outbuff,uint16_t outbuff_size,char *echo_expect)
{
	if(desc == NULL)
	{
		return -1;
	}
	if(outbuff)
	{
		memset(outbuff,0,outbuff_size);
	}
	desc->outbuff = outbuff;
	desc->outbuff_size = outbuff_size;
	desc->echo_expect = echo_expect;
	desc->status = AT_ECHO_STATUS_NONE;
	return 0;
}

int drv_at_cmd_exec(drv_at_desc_t *desc,uint32_t timeout,char *inbuff)
{
	uint8_t *pmsg = NULL;
	if(desc == NULL)
	{
		return -1;
	}
	per_uart_send_data(desc->index,(uint8_t *)inbuff,strlen(inbuff));
	if(desc->echo_expect == NULL)
	{
		pmsg = drv_at_wait_respond(desc,"OK\r\n",timeout);
		if(pmsg != NULL)
		{
			desc->status = AT_ECHO_STATUS_OK;
			return 0;
		}
	}
	else
	{
		pmsg = drv_at_wait_respond(desc,desc->echo_expect,timeout);
		if(pmsg != NULL)
		{
			desc->status = AT_ECHO_STATUS_OK;
			if(desc->outbuff_size > strlen((char *)pmsg))
			{
				memcpy(desc->outbuff,pmsg,strlen((char *)pmsg));
				return 0;
			}
			else
			{
				memcpy(desc->outbuff,pmsg,desc->outbuff_size);
				return 0;
			}
		}
	}
	return 0;
}

void drv_at_parser(drv_at_desc_t *desc)
{
	
}






