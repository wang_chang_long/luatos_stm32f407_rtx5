#ifndef __DRIVER_LOG_H
#define __DRIVER_LOG_H
#include <stdint.h>

#define DEBUG_UART   PER_UART_INDEX_TTL

#define DEBUG_LOG								0x1
#define DEBUG_MULTIMETER				0x2			//万用表
#define DEBUG_FATFS							0x4			//FATFS调试信息
#define DEBUG_SENSOR						0x8			//采集调试信息
#define DEBUG_CFG								0x10
#define DEBUG_BLE								0x20
#define DEBUG_PROTOCOL					0x40			//协议处理

void drv_debug_init(void);
uint8_t drv_set_debug_level(uint8_t lev);
uint8_t drv_get_debug_level(void);
void drv_uart_debug(uint8_t lev, const char *format,...);

#endif

