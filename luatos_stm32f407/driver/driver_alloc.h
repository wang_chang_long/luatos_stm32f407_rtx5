#ifndef __DRIVER_ALLOC_H
#define __DRIVER_ALLOC_H

#include <stdint.h>

typedef enum
{
	DRV_ALLOC_SIZE_512,
	DRV_ALLOC_SIZE_256,
	DRV_ALLOC_SIZE_128
}drv_alloc_size_e;


void drv_alloc_init(void);
void *drv_alloc_malloc(drv_alloc_size_e alloc_size);
int drv_alloc_free(drv_alloc_size_e alloc_size,void *msg);

#endif


