#include "driver_multimeter.h"
#include "periph.h"
#include "stdlib.h"
#include "driver_log.h"

#define BEEP_PORT_NAME     BSP_GPIOE8_39
#define BEEP_ON            bsp_gpio_write(BEEP_PORT_NAME,GPIO_PIN_SET)
#define BEEP_OFF           bsp_gpio_write(BEEP_PORT_NAME,GPIO_PIN_RESET)
typedef struct
{
	asw_e asw;
	uint8_t state[SWITCH_IO_NONE];
}asw_state_t;

typedef struct
{
	Analog_switch_name_e switch_name;
	bsp_gpio_name_t	port_name;
}analog_port_map_t;

const analog_port_map_t analog_switch_port_list[SWITCH_IO_NONE] =
{
	{SWITCH_IO_K1,BSP_GPIOE0_97},
	{SWITCH_IO_K2,BSP_GPIOE1_98},
	{SWITCH_IO_K3,BSP_GPIOE5_4},
	{SWITCH_IO_K4,BSP_GPIOE6_5},
	{SWITCH_IO_K6,BSP_GPIOE2_1},
	{SWITCH_IO_K7,BSP_GPIOE4_3},
	{SWITCH_IO_K8,BSP_GPIOE3_2},
};

const asw_state_t asw_state_list[ASW_END] =
{
	{ASW_OFF	,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L},
	{ASW_DCV1	,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L},
	{ASW_DCV2	,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H},
	{ASW_DCV3	,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H},
	{ASW_ACV1	,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L},
	{ASW_ACV2	,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H},
	{ASW_ACV3	,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H},
	{ASW_DCMA	,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H},
	{ASW_DCA	,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H},
	{ASW_ACMA	,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H},
	{ASW_ACA	,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H},
	{ASW_R2		,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H},
	{ASW_R3		,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H},
	{ASW_R4		,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L},
	{ASW_R5		,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L},
	{ASW_C1		,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_H},
	{ASW_C2		,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H},
	{ASW_C3		,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_H},
	{ASW_C4		,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L},
	{ASW_C5		,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L},
	{ASW_BEEP	,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_H,ANALOG_PORT_L,ANALOG_PORT_L,ANALOG_PORT_L},
};

/**
  * @brief  保存参数
  * @note    
	* @param  save_param：待保存的数据
  * @retval 无
  */
static void drv_multimeter_save_cfg(drv_multimeter_save_t *save_param)
{
	per_fm24clxx_write(0x0,(uint8_t *)save_param,sizeof(drv_multimeter_save_t));
}
/**
  * @brief  读取保存参数
  * @note    
	* @param  save_param：数据缓存指针
  * @retval 无
  */
static void drv_multimeter_read_cfg(drv_multimeter_save_t *save_param)
{
	per_fm24clxx_read(0x0,(uint8_t *)save_param,sizeof(drv_multimeter_save_t));
}
/**
  * @brief  模拟开关控制函数
  * @note    
	* @param  switch_name：模拟开关端口
						level：电平
  * @retval 无
  */
static void drv_analog_switch_ctrl(Analog_switch_name_e switch_name,uint8_t level)
{
	if(level)
	{
		bsp_gpio_write(analog_switch_port_list[switch_name].port_name,GPIO_PIN_SET);
	}
	else
	{
		bsp_gpio_write(analog_switch_port_list[switch_name].port_name,GPIO_PIN_RESET);
	}
}
/**
  * @brief  模拟开关切换
  * @note    
	* @param  desc：描述符
						sw：功能
  * @retval 无
  */
static void drv_analog_switch(drv_multimeter_desc_t *desc,uint8_t sw)
{
	if(sw != desc->current_sw)
	{
		desc->current_sw = sw;
		for(uint8_t i = 0;i<SWITCH_IO_NONE;i++)
		{
			drv_analog_switch_ctrl(analog_switch_port_list[i].switch_name,asw_state_list[sw].state[i]);
		}
	}
}
/**
  * @brief  万用表初始化
  * @note    
	* @param  desc：描述符
  * @retval 无
  */
void drv_multimeter_init(drv_multimeter_desc_t *desc)
{
	uint8_t i = 0;
	desc->current_fun = 3;
	for(i = 0;i < SWITCH_IO_NONE; i++ )//模拟开关引脚初始化
	{
		bsp_gpio_init(analog_switch_port_list[i].port_name,GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_LOW);
	}
	bsp_gpio_init(BEEP_PORT_NAME,GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_LOW);
	per_mcp3421_init();//ad采集芯片初始化
	drv_multimeter_read_cfg(&desc->save_param);
}

/**
  * @brief  测量直流电压
  * @note    
	* @param  desc：描述符
	*					ad_dat:采集到的ADC数据
  * @retval 0：测量完成  -1：测量未完成
  */
int drv_measure_dcv(drv_multimeter_desc_t *desc,uint32_t ad_dat)
{
	long vol = 0;
	int lsdat = 0;
	int dat = 0;
	
	dat = ((int)(ad_dat << 14));

	dat -= desc->save_param.zero[1];
	/***************/

	lsdat = abs(dat); //求a的绝对值

	vol = lsdat * 0.00095367; //算出电压 单位1uV

	if (desc->current_sw == ASW_DCV1) //最大1000V
	{
		vol *= 513.45;
//		drv_uart_debug(DEBUG_MULTIMETER,"DCV1 vol=%lld\r\n", vol);
		if (vol < 70000000) //电压小于70V 切换档位
		{
			drv_analog_switch(desc,ASW_DCV2);
//			osDelay(10);
			return -1;
		}
	}
	else if (desc->current_sw == ASW_DCV2) //最大100V
	{
		vol *= 52.13;
//		drv_uart_debug(DEBUG_MULTIMETER,"DCV2 vol=%lld\r\n", vol);
		if (vol < 8000000) //电压小于8V 切换档位
		{
			drv_analog_switch(desc,ASW_DCV3);
			osDelay(10);
			return -1;
		}
		else if (vol > 65000000) //电压大于65V 切换档位
		{
			drv_analog_switch(desc,ASW_DCV1);
			osDelay(10);
			return -1;
		}
	}
	else if (desc->current_sw == ASW_DCV3) //最大10V
	{
		vol *= 6.11;
		if (vol > 9000000) //电压大于9V 切换档位
		{
			drv_analog_switch(desc,ASW_DCV2);
			osDelay(10);
			return -1;
		}
	}
	else
	{
		drv_analog_switch(desc,ASW_DCV2);
		osDelay(10);
		return -1;
	}
		/***直流电压档调零***/
	if (desc->zero_b == 1)
	{
		desc->zero_b = 0;
		desc->save_param.zero[1] = dat;
		drv_multimeter_save_cfg(&desc->save_param);
		return 0;
	}
	if (dat >= 0) //测量值为正数
	{
		desc->sign = 0;
		desc->measured_curval = 1;
	}
	else
	{
		desc->sign = 1;
		desc->measured_curval = -1;
	}
	desc->measured_curval = desc->measured_curval * vol;
	if (vol < 1000000) //uV
	{
		desc->unit = 0x01;
		desc->measured_value = vol;
	}
	else //mV
	{
		desc->unit = 0x02;
		desc->measured_value = vol / 1000;
	}
	drv_uart_debug(DEBUG_LOG,"vol=%d\r\n", desc->measured_curval);
	return 0;
}


/**
  * @brief  测量交流电压
  * @note    
	* @param  desc：描述符
	*					ad_dat:采集到的ADC数据
  * @retval 0：测量完成  -1：测量未完成
  */
int drv_measure_acv(drv_multimeter_desc_t *desc,uint32_t ad_dat)
{
	long vol = 0;
	int lsdat = 0;

	lsdat = ((int)(ad_dat << 14));

	lsdat -= desc->save_param.zero[2];
	/***************/
	vol = lsdat * 0.00095367 / 0.71; //算出电压 单位1uV
//	drv_uart_debug(DEBUG_LOG,"vol=%lld\r\n", vol);

	if (desc->current_sw == ASW_ACV1) //最大1000V
	{
		vol *= 367.85;
//		drv_uart_debug(DEBUG_LOG,"ACV1 vol=%lld\r\n", vol);
		if (vol < 24000000) //电压小于24V 切换档位
		{
			drv_analog_switch(desc,ASW_ACV2);
			osDelay(10);
			return -1;
		}
	}
	else if (desc->current_sw == ASW_ACV2) //最大100V
	{
		vol *= 36.89;
//		drv_uart_debug(DEBUG_LOG,"ACV2 vol=%lld\r\n", vol);
		if (vol < 3000000) //电压小于3V 切换档位
		{
			drv_analog_switch(desc,ASW_ACV3);
			osDelay(10);
			return -1;
		}
		else if (vol > 25000000) //电压小于大于24V 切换档位
		{
			drv_analog_switch(desc,ASW_ACV1);
			osDelay(10);
			return -1;
		}
	}
	else if (desc->current_sw == ASW_ACV3) //最大10V
	{
		vol *= 4.38;
//		drv_uart_debug(DEBUG_LOG,"ACV3 vol=%lld\r\n", vol);
		if (vol > 4000000) //电压大于4V 切换档位
		{
			drv_analog_switch(desc,ASW_ACV2);
			osDelay(10);
			return -1;
		}
	}
	else
	{
		drv_analog_switch(desc,ASW_ACV2);
		return -1;
	}
	desc->measured_curval = vol;
	/***交流电压档调零***/
	if (desc->zero_b == 1)
	{
		desc->zero_b = 0;
		desc->save_param.zero[2] = lsdat;
		drv_multimeter_save_cfg(&desc->save_param);
		return 0;
	}
	if (vol >= 0) //测量值为正数
	{
		desc->sign = 0;
	}
	else
	{
		desc->sign = 1;
	}

	vol = labs(vol);
	if (vol < 1000000) //uV
	{
		desc->unit = 0x01;
		desc->measured_value = vol;
	}
	else //mV
	{
		desc->unit = 0x02;
		desc->measured_value = vol / 1000;
	}
	return 0;
}

//测量直流电流mA
int drv_measure_dcma(drv_multimeter_desc_t *desc,uint32_t ad_dat)
{
	long Aol = 0;
	int lsdat = 0;

	lsdat = ((int)(ad_dat << 14));

	lsdat -= desc->save_param.zero[3];
	/***************/

	Aol = lsdat * 0.00095367; //算出电压 单位1uV

	if (desc->current_sw == ASW_DCMA) //最大250mA
	{
		Aol /= 3; //单位 uA
//		drv_uart_debug(DEBUG_LOG,"ASW_DCMA Aol=%lld\r\n", Aol);
	}
	else
	{
		drv_analog_switch(desc,ASW_DCMA);
		return -1;
	}
	/***直流电流档调零***/
	if (desc->zero_b == 1)
	{
		desc->zero_b = 0;
		desc->save_param.zero[3] = lsdat;
		drv_multimeter_save_cfg(&desc->save_param);
		return 0;
	}
	drv_uart_debug(DEBUG_LOG,"ASW_DCMA Aol=%d\r\n", Aol);
	desc->measured_curval = Aol;
	if (Aol >= 0) //测量值为正数
	{
		desc->sign = 0;
	}
	else
	{
		desc->sign = 1;
	}
	Aol = labs(Aol);
	desc->unit = 0x01;
	desc->measured_value = Aol;
	return 0;
}

//测量直流电流A
int drv_measure_dca(drv_multimeter_desc_t *desc,uint32_t ad_dat)
{
	long Aol = 0;
	int lsdat = 0;

	lsdat = ((int)(ad_dat << 14));

	lsdat -= desc->save_param.zero[4];
	/***************/

	Aol = lsdat * 0.00095367; //算出电压 单位1uV

	if (desc->current_sw == ASW_DCA) //最大2.5A
	{
		Aol *= 6.3; //单位 uA
//		drv_uart_debug(DEBUG_LOG,"ASW_DCA Aol=%lld\r\n", Aol);
	}
	else
	{
		drv_analog_switch(desc,ASW_DCA);
		return -1;
	}
	/***直流电流档调零***/
	if (desc->zero_b == 1)
	{
		desc->zero_b = 0;
		desc->save_param.zero[4] = lsdat;
		drv_multimeter_save_cfg(&desc->save_param);
		return 0;
	}
	drv_uart_debug(DEBUG_LOG,"ASW_DCA Aol=%d\r\n", Aol);
	desc->measured_curval = Aol;
	if (Aol >= 0) //测量值为正数
	{
		desc->sign = 0;
	}
	else
	{
		desc->sign = 1;
	}
	Aol = labs(Aol);
	desc->unit = 0x01;
	desc->measured_value = Aol / 1000;
	return 0;
}

//测量交流电流mA
int drv_measure_acma(drv_multimeter_desc_t *desc,uint32_t ad_dat)
{
	long Aol = 0;
	int lsdat = 0;

	lsdat = ((int)(ad_dat << 14));

	lsdat -= desc->save_param.zero[5];
	/***************/

	Aol = lsdat * 0.00095367 * 1.4; //算出电压 单位1uV

	if (desc->current_sw == ASW_ACMA) //最大250mA
	{
		Aol /= 4.24; //单位 uA
//		drv_uart_debug(DEBUG_LOG,"ASW_MACA Aol=%lld\r\n", Aol);
	}
	else
	{
		drv_analog_switch(desc,ASW_ACMA);
		return -1;
	}
	/***交流电流档调零***/
	if (desc->zero_b == 1)
	{
		desc->zero_b = 0;
		desc->save_param.zero[5] = lsdat;
		drv_multimeter_save_cfg(&desc->save_param);
		return 0;
	}
	drv_uart_debug(DEBUG_LOG,"ASW_MACA Aol=%d\r\n", Aol);
	desc->measured_curval = Aol;
	if (Aol >= 0) //测量值为正数
	{
		desc->sign = 0;
	}
	else
	{
		desc->sign = 1;
	}
	Aol = labs(Aol);
	desc->unit = 0x01;
	desc->measured_value = Aol;
	return 0;
}

//测量交流电流A
int drv_measure_aca(drv_multimeter_desc_t *desc,uint32_t ad_dat)
{
	long Aol = 0;
	int lsdat = 0;

	lsdat = ((int)(ad_dat << 14));

	lsdat -= desc->save_param.zero[6];
	/***************/
//		drv_uart_debug(DEBUG_LOG,"lsdat=%d\r\n", lsdat);
	Aol = lsdat * 0.00095367 * 1.4; //算出电压 单位1uV

	if (desc->current_sw == ASW_ACA) //最大2.5A
	{
		Aol /= 0.116; //单位 uA
		
//			drv_uart_debug(DEBUG_LOG,"ASW_ACA Aol=%lld\r\n", Aol);
	}
	else
	{
		drv_analog_switch(desc,ASW_ACA);
		return -1;
	}
	/***交流电流档调零***/
	if (desc->zero_b == 1)
	{
		desc->zero_b = 0;
		desc->save_param.zero[6] = lsdat;
		drv_multimeter_save_cfg(&desc->save_param);
		return 0;
	}
	drv_uart_debug(DEBUG_LOG,"ASW_ACA Aol=%d\r\n", Aol);
	desc->measured_curval = Aol;
	if (Aol >= 0) //测量值为正数
	{
		desc->sign = 0;
	}
	else
	{
		desc->sign = 1;
	}

	Aol = labs(Aol);
	desc->unit = 0x01;
	desc->measured_value = Aol / 1000;
	return 0;
}

//测量电阻
int drv_measure_r(drv_multimeter_desc_t *desc,uint32_t ad_dat)
{
	uint64_t vol;
	uint64_t r;
	int lsdat = 0;

	lsdat = ((int)(ad_dat << 14));

	lsdat -= desc->save_param.zero[7];
	/***************/

//	drv_uart_debug(DEBUG_LOG,"lsdat=%d\r\n", lsdat);
	if (lsdat > 1200000000)
	{
		drv_analog_switch(desc,ASW_R2);
		desc->sign = 1;
		desc->unit = 0x00;
		desc->measured_value = 0xFFFFFFFF;
		return -1;
	}
	else if (lsdat < 0) //电阻档的电压值不能为负数
	{
		drv_analog_switch(desc,ASW_R5);
		desc->sign = 1;
		desc->unit = 0x00;
		desc->measured_value = 0;
		return -1;
	}
	else
	{
		if (desc->current_sw == ASW_R2) //1M
		{
			vol = lsdat * 0.00095367; //算出电压 单位1uV
//		drv_uart_debug(DEBUG_LOG,"1M vol=%llu\r\n", vol);
			r = ((uint64_t)vol * 1000000000) / (1248000 - vol); //算出电阻 单位1mR
			if (r < 200000000) //电阻小于200K 切换档位
			{
				drv_analog_switch(desc,ASW_R3);
				osDelay(10);
				return -1;
			}
		}
		else if (desc->current_sw == ASW_R3) //100K
		{
			vol = lsdat * 0.00095367; //算出电压 单位1uV
//			drv_uart_debug(DEBUG_LOG,"100K vol=%llu\r\n", vol);
			r = ((uint64_t)vol * 100000000) / (1248000 - vol); //算出电阻 单位1mR
//			drv_uart_debug(DEBUG_LOG,"100K r=%llu\r\n", r);
			if (r < 20000000) //电阻小于20K 切换档位
			{
				drv_analog_switch(desc,ASW_R4);
				osDelay(10);
				return -1;
			}
			else if (r > 250000000) //电阻大于250K 切换档位
			{
				drv_analog_switch(desc,ASW_R2);
				osDelay(10);
				return -1;
			}
		}
		else if (desc->current_sw == ASW_R4) //10K
		{
			vol = lsdat * 0.00095367; //算出电压 单位1uV
//				drv_uart_debug(DEBUG_LOG,"10K vol=%llu\r\n", vol);
			r = ((uint64_t)vol * 10000000) / (1248000 - vol); //算出电阻 单位1mR
//				drv_uart_debug(DEBUG_LOG,"10K r=%llu\r\n", r);
			if (r < 2000000) //电阻小于2K 切换档位
			{
				drv_analog_switch(desc,ASW_R5);
				osDelay(10);
				return -1;
			}
			else if (r > 25000000) //电阻大于25K 切换档位
			{
				drv_analog_switch(desc,ASW_R3);
				osDelay(10);
				return -1;
			}
		}
		else if (desc->current_sw == ASW_R5) //1K
		{
			vol = lsdat * 0.00095367; //算出电压 单位1uV
//		drv_uart_debug(DEBUG_LOG,"1K vol=%llu\r\n", vol);
			r = ((uint64_t)vol * 1000000) / (1248000 - vol); //算出电阻 单位1mR
//	  drv_uart_debug(DEBUG_LOG,"1K r=%llu\r\n", r);
			if (r > 2500000) //电阻大于2.5K 切换档位
			{
				drv_analog_switch(desc,ASW_R4);
				osDelay(10);
				return -1;
			}
		}
		else
		{
			drv_analog_switch(desc,ASW_R2);
			return -1;
		}
		/***电阻档调零***/
		if (desc->zero_b == 1)
		{
			desc->zero_b = 0;
			desc->save_param.zero[7] = lsdat;
			drv_multimeter_save_cfg(&desc->save_param);
			return 0;
		}
		desc->measured_curval = r;
		desc->sign = 0;
		if (r < 1000000) //mR
		{
			desc->unit = 0x01;
			desc->measured_value = r;
		}
		else if (r < 1000000000) //R
		{
			desc->unit = 0x02;
			desc->measured_value = r / 1000;
		}
		else if (r < 5000000000) //KR 最大5M
		{
			desc->unit = 0x03;
			desc->measured_value = r / 1000000;
		}
		else //超量程
		{
			desc->unit = 0x00;
			desc->measured_value = 0xFFFFFFFF;
			desc->measured_curval = 0x7FFFFFF;
		}
	}
	return 0;
}

//测量电容
int drv_measure_c(drv_multimeter_desc_t *desc,uint32_t ad_dat)
{
	uint64_t vol;
	uint64_t c;
	int lsdat = 0;

	lsdat = ((int)(ad_dat << 14));

	lsdat -= desc->save_param.zero[8];
	/***************/

	if (lsdat < 0) //电容档的电压值不能为负数
	{
		drv_analog_switch(desc,ASW_C5);
		
			drv_uart_debug(DEBUG_LOG,"vol= - \r\n");
		desc->sign = 1;
		desc->unit = 0x00;
		desc->measured_value = 0;
		drv_analog_switch(desc,ASW_C1);
	}
	else
	{

		if (desc->current_sw == ASW_C5) //最大10uF
		{
			vol = lsdat * 0.00095367; //算出电压 单位1uV
			
				drv_uart_debug(DEBUG_LOG,"C4 vol=%u\r\n", vol);
			c = vol * 156; //单位0.1pF

			
				drv_uart_debug(DEBUG_LOG,"C4 C=%u\r\n", c);

			if (c < 6000000) //小于600nF 切换档位
			{
				drv_analog_switch(desc,ASW_C4);
				osDelay(10);

			}
		}
		else if (desc->current_sw == ASW_C4) //最大0.7uF
		{
			vol = lsdat * 0.00095367; //算出电压 单位1uV
			
				drv_uart_debug(DEBUG_LOG,"C4 vol=%llu\r\n", vol);
			c = vol * 15; //单位0.1pF

			
				drv_uart_debug(DEBUG_LOG,"C4 C=%llu\r\n", c);

			if (c > 7000000) //大于700nF 切换档位
			{
				drv_analog_switch(desc,ASW_C5);
				osDelay(10);

			}
			else if (c < 600000) //小于60nF 切换档位
			{
				drv_analog_switch(desc,ASW_C3);
				osDelay(10);

			}
		}

		else if (desc->current_sw == ASW_C3) //最大70nF
		{
			vol = lsdat * 0.00095367; //算出电压 单位1uV
			
				drv_uart_debug(DEBUG_LOG,"C3 vol=%llu\r\n", vol);
			c = vol * 1.467; //单位0.1pF

			
				drv_uart_debug(DEBUG_LOG,"C3 C=%llu\r\n", c);

			if (c > 700000) //大于70nF 切换档位
			{
				drv_analog_switch(desc,ASW_C4);
				osDelay(10);

			}
			else if (c < 60000) //小于6nF 切换档位
			{
				drv_analog_switch(desc,ASW_C2);
				osDelay(10);

			}
		}

		else if (desc->current_sw == ASW_C2) //最大7nF
		{
			vol = lsdat * 0.00095367; //算出电压 单位1uV
			
			drv_uart_debug(DEBUG_LOG,"C2 vol=%llu\r\n", vol);
			c = vol * 0.166; //单位0.1pF

			
			drv_uart_debug(DEBUG_LOG,"C2 C=%llu\r\n", c);

			if (c > 70000) //大于7nF 切换档位
			{
				drv_analog_switch(desc,ASW_C3);
				osDelay(10);
			}
			else if (c < 6000) //小于0.6nF 切换档位
			{
				drv_analog_switch(desc,ASW_C1);
				osDelay(10);
			}
		}

		else if (desc->current_sw == ASW_C1) //最大0.7nF
		{
			vol = lsdat * 0.00095367; //算出电压 单位1uV
			
				drv_uart_debug(DEBUG_LOG,"C1 vol=%llu\r\n", vol);
			c = vol / 66; //单位0.1pF

			
				drv_uart_debug(DEBUG_LOG,"C1 C=%llu\r\n", c);
			if (c > 7000) //大于0.7nF 切换档位
			{
				drv_analog_switch(desc,ASW_C2);
				osDelay(10);

			}
		}
		else
		{
			drv_analog_switch(desc,ASW_C1);
		}
		/***电容档调零***/
		if (desc->zero_b == 1)
		{
			desc->zero_b = 0;
			desc->save_param.zero[8] = lsdat;
			drv_multimeter_save_cfg(&desc->save_param);
		}
		desc->sign = 0;
		if (c < 10000000) //0.1pF
		{
			desc->unit = 0x01;
			desc->measured_value = c;
		}
		else if (c < 200000000) //最大测量20uF
		{
			desc->unit = 0x02;
			desc->measured_value = c / 10000;
		}
		else //超量程
		{
			desc->unit = 0x00;
			desc->measured_value = 0xFFFFFFFF;
		}
	}
	return 0;
}

//蜂鸣器档
int drv_measure_beep(drv_multimeter_desc_t *desc,uint32_t ad_dat)
{
	uint64_t vol = 0;
	uint64_t r = 0;
	int lsdat = 0;

	lsdat = ((int)(ad_dat << 14));//方便判断正负号

	lsdat -= desc->save_param.zero[9];
	if(desc->current_sw != ASW_BEEP)
	{
		drv_analog_switch(desc,ASW_BEEP);
		return -1;
	}
	/***电蜂鸣器档调零***/
	if (desc->zero_b == 1)
	{
		desc->zero_b = 0;
		desc->save_param.zero[9] = lsdat;
		drv_multimeter_save_cfg(&desc->save_param);//保存参数
		return 0;
	}
	/***************/
	if ((lsdat > 1200000000) | (lsdat < -500000000)) //开路
	{
		desc->sign = 1;
		desc->unit = 0x00;
		desc->measured_value = 0xFFFFFFFF;
		desc->measured_curval = 0;
		return 0;
	}
	else if (lsdat < 0) //电阻档的电压值不能为负数
	{
		desc->sign = 1;
		desc->unit = 0x00;
		desc->measured_value = 0;
		desc->measured_curval = 0;
		return -1;
	}
	else
	{
		vol = lsdat * 0.000058662;					//算出电压 单位1uV
		r = ((uint64_t)vol * 1000000) / (1248000 - vol); //算出电阻 单位1mR
		desc->sign = 0;
		if (r < 1000000) //mR
		{
			desc->unit = 0x01;
			desc->measured_value = r;
			desc->measured_curval = 1;
		}
		else //R
		{
			desc->unit = 0x00;
			desc->measured_value = 0xFFFFFFFF;
			desc->measured_curval = 0;
		}
	}
	return 0;
}


