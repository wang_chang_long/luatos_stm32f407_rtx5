#ifndef __DRIVER_AT_H
#define __DRIVER_AT_H

#include "bsp.h"
#include "periph_uart.h"
//typedef uint8_t *(*drv_at_wait_recv)(uint32_t timeout,uint16_t *len);
//typedef void (*drv_at_send)(uint8_t *pbuff,uint16_t len);
typedef enum at_echo_status_en {
    AT_ECHO_STATUS_NONE,
    AT_ECHO_STATUS_OK,
    AT_ECHO_STATUS_FAIL,
    AT_ECHO_STATUS_ERROR,
    AT_ECHO_STATUS_EXPECT,
} at_echo_status_t;



typedef struct
{
	per_uart_index_t index;
	uint8_t *outbuff;
	uint16_t outbuff_size;
	char    *echo_expect;
	at_echo_status_t status;
//	drv_at_wait_recv wait_recv_cb;
//	drv_at_send send_cb;
}drv_at_desc_t;


int drv_at_echo_create(drv_at_desc_t *desc,uint8_t *outbuff,uint16_t outbuff_size,char *echo_expect);
int drv_at_cmd_exec(drv_at_desc_t *desc,uint32_t timeout,char *inbuff);


#endif

