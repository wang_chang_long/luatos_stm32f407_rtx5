#ifndef __DRIVER_MULTIMETER_H
#define __DRIVER_MULTIMETER_H

#include "stdint.h"
#include "bsp.h"

#define ANALOG_PORT_H   1
#define ANALOG_PORT_L    0

typedef enum
{
	SWITCH_IO_K1 = 0,
	SWITCH_IO_K2,
	SWITCH_IO_K3,
	SWITCH_IO_K4,
	SWITCH_IO_K6,
	SWITCH_IO_K7,
	SWITCH_IO_K8,
	SWITCH_IO_NONE
}Analog_switch_name_e;

typedef enum 
{
    ASW_OFF  = 0,
    ASW_DCV1, //1000-70v
    ASW_DCV2,	//70-8v
    ASW_DCV3,
    ASW_ACV1,
    ASW_ACV2,
    ASW_ACV3,
    ASW_DCMA,
    ASW_DCA,
    ASW_ACMA,
    ASW_ACA,
    ASW_R2,
    ASW_R3,
    ASW_R4,
    ASW_R5,
    ASW_C1,
    ASW_C2,
    ASW_C3,
    ASW_C4,
    ASW_C5,
    ASW_BEEP,
		ASW_END,
}asw_e;

typedef struct
{	
	int zero[10];
}drv_multimeter_save_t;

typedef struct
{
	uint8_t zero_b;//校准标志
	uint8_t current_sw;//当前挡位
	uint8_t sign;////正负号 为1负数
	uint8_t unit;//单位
	uint32_t measured_value; //测量值 全F为超量程
	int   measured_curval;//测量值，带正负号
	uint8_t current_freq;//测量周期 单位0.1秒
	uint8_t current_fun; //当前功能 01直流电压  02交流电压  03直流电流mA  04直流电流A  05交流电流mA  06交流电流A  07电阻  08电容  09蜂鸣器
	drv_multimeter_save_t save_param;//需要保存的参数
}drv_multimeter_desc_t;

void drv_multimeter_init(drv_multimeter_desc_t *desc);
int drv_measure_dcv(drv_multimeter_desc_t *desc,uint32_t ad_dat);
int drv_measure_acv(drv_multimeter_desc_t *desc,uint32_t ad_dat);
int drv_measure_dcma(drv_multimeter_desc_t *desc,uint32_t ad_dat);
int drv_measure_dca(drv_multimeter_desc_t *desc,uint32_t ad_dat);
int drv_measure_acma(drv_multimeter_desc_t *desc,uint32_t ad_dat);
int drv_measure_aca(drv_multimeter_desc_t *desc,uint32_t ad_dat);
int drv_measure_r(drv_multimeter_desc_t *desc,uint32_t ad_dat);
int drv_measure_c(drv_multimeter_desc_t *desc,uint32_t ad_dat);
int drv_measure_beep(drv_multimeter_desc_t *desc,uint32_t ad_dat);

#endif



