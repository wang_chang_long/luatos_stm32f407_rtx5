/*
	利用rtx5的内存池共享一部分内存，用于动态申请和释放内存，供应用使用
*/
#include "driver_alloc.h"
#include "cmsis_os2.h"
#include "rtx_os.h"

#define OSMEM_BLOCK512_CNT      20
#define OSMEM_BLOCK512_SIZE     512

#define OSMEM_BLOCK256_CNT      20
#define OSMEM_BLOCK256_SIZE     256

#define OSMEM_BLOCK128_CNT      20
#define OSMEM_BLOCK128_SIZE     128


osMemoryPoolId_t osmem_combuf512_id = NULL;
osMemoryPoolId_t osmem_combuf256_id = NULL;
osMemoryPoolId_t osmem_combuf128_id = NULL;

static uint64_t osmem_cp_stack512[OSMEM_BLOCK512_CNT*OSMEM_BLOCK512_SIZE/8] __attribute__((section("ccmram")));
static uint64_t osmem_cp_stack256[OSMEM_BLOCK512_CNT*OSMEM_BLOCK512_SIZE/8] __attribute__((section("ccmram")));
static uint64_t osmem_cp_stack128[OSMEM_BLOCK512_CNT*OSMEM_BLOCK512_SIZE/8] __attribute__((section("ccmram")));

static osRtxMemoryPool_t osmem_cb_statck512 __attribute__((section( ".bss.os.mempool.cb" )));
static osRtxMemoryPool_t osmem_cb_statck256 __attribute__((section( ".bss.os.mempool.cb" )));
static osRtxMemoryPool_t osmem_cb_statck128 __attribute__((section( ".bss.os.mempool.cb" )));
osMemoryPoolAttr_t osmem_conbuf512_attr = 
{
	.name = "drv_alloc_name",
	.cb_mem = &osmem_cb_statck512,
	.cb_size = osRtxMemoryPoolCbSize,
	.mp_mem = osmem_cp_stack512,
	.mp_size = sizeof(osmem_cp_stack512),
};
osMemoryPoolAttr_t osmem_conbuf256_attr = 
{
	.name = "drv_alloc_name",
	.cb_mem = &osmem_cb_statck256,
	.cb_size = osRtxMemoryPoolCbSize,
	.mp_mem = osmem_cp_stack256,
	.mp_size = sizeof(osmem_cp_stack256),
};
osMemoryPoolAttr_t osmem_conbuf128_attr = 
{
	.name = "drv_alloc_name",
	.cb_mem = &osmem_cb_statck128,
	.cb_size = osRtxMemoryPoolCbSize,
	.mp_mem = osmem_cp_stack128,
	.mp_size = sizeof(osmem_cp_stack128),
};
/**
  * @brief  设备申请堆初始化.
  * @note    
  * @param  None
  * @retval None
  */
void drv_alloc_init(void)
{
	osmem_combuf512_id = osMemoryPoolNew(OSMEM_BLOCK512_CNT,OSMEM_BLOCK512_SIZE,&osmem_conbuf512_attr);
	osmem_combuf256_id = osMemoryPoolNew(OSMEM_BLOCK256_CNT,OSMEM_BLOCK256_SIZE,&osmem_conbuf256_attr);
	osmem_combuf128_id = osMemoryPoolNew(OSMEM_BLOCK128_CNT,OSMEM_BLOCK128_SIZE,&osmem_conbuf128_attr);
}
/**
  * @brief  动态申请内存
  * @note    
  * @param  None
  * @retval None
  */
void *drv_alloc_malloc(drv_alloc_size_e alloc_size)
{
	uint8_t cnt = 0;
	switch(alloc_size)
	{
		case DRV_ALLOC_SIZE_512:
			cnt = osMemoryPoolGetSpace(osmem_combuf512_id);
			if( cnt> 0)
			{
				return osMemoryPoolAlloc(osmem_combuf512_id,0);//
			}
		break;
		case DRV_ALLOC_SIZE_256:
			cnt = osMemoryPoolGetSpace(osmem_combuf256_id);
			if( cnt> 0)
			{
				return osMemoryPoolAlloc(osmem_combuf256_id,0);//
			}
		break;
		case DRV_ALLOC_SIZE_128:
			cnt = osMemoryPoolGetSpace(osmem_combuf128_id);
			if( cnt> 0)
			{
				return osMemoryPoolAlloc(osmem_combuf128_id,0);//
			}
		break;
	}
	return NULL;
}
/**
  * @brief  动态释放内存
  * @note    
  * @param  None
  * @retval None
  */
int drv_alloc_free(drv_alloc_size_e alloc_size,void *msg)
{
	osStatus_t status = osOK;
	switch(alloc_size)
	{
		case DRV_ALLOC_SIZE_512:
			status = osMemoryPoolFree(osmem_combuf512_id,msg);
		break;
		case DRV_ALLOC_SIZE_256:
			status = osMemoryPoolFree(osmem_combuf256_id,msg);
		break;
		case DRV_ALLOC_SIZE_128:
			status = osMemoryPoolFree(osmem_combuf128_id,msg);
		break;
	}
	if(status == osOK)
	{
		return 0;
	}
	return -1;
}

