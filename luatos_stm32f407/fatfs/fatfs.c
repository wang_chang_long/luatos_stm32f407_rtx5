#include "fatfs.h"
#include "ff.h"
#include "ff_gen_drv.h"
#include "diskio.h"
#include "sd_diskio_dma.h"
#include "driver_log.h"
#include "string.h"
#include "stdio.h"

FATFS fs; 
char SDPath[4]; /* SD逻辑驱动器路径 */
/* FatFs API的返回值 */
const char * FR_Table[]= 
{
	"FR_OK：成功",				                             /* (0) Succeeded */
	"FR_DISK_ERR：底层硬件错误",			                 /* (1) A hard error occurred in the low level disk I/O layer */
	"FR_INT_ERR：断言失败",				                     /* (2) Assertion failed */
	"FR_NOT_READY：物理驱动没有工作",			             /* (3) The physical drive cannot work */
	"FR_NO_FILE：文件不存在",				                 /* (4) Could not find the file */
	"FR_NO_PATH：路径不存在",				                 /* (5) Could not find the path */
	"FR_INVALID_NAME：无效文件名",		                     /* (6) The path name format is invalid */
	"FR_DENIED：由于禁止访问或者目录已满访问被拒绝",         /* (7) Access denied due to prohibited access or directory full */
	"FR_EXIST：文件已经存在",			                     /* (8) Access denied due to prohibited access */
	"FR_INVALID_OBJECT：文件或者目录对象无效",		         /* (9) The file/directory object is invalid */
	"FR_WRITE_PROTECTED：物理驱动被写保护",		             /* (10) The physical drive is write protected */
	"FR_INVALID_DRIVE：逻辑驱动号无效",		                 /* (11) The logical drive number is invalid */
	"FR_NOT_ENABLED：卷中无工作区",			                 /* (12) The volume has no work area */
	"FR_NO_FILESYSTEM：没有有效的FAT卷",		             /* (13) There is no valid FAT volume */
	"FR_MKFS_ABORTED：由于参数错误f_mkfs()被终止",	         /* (14) The f_mkfs() aborted due to any parameter error */
	"FR_TIMEOUT：在规定的时间内无法获得访问卷的许可",		 /* (15) Could not get a grant to access the volume within defined period */
	"FR_LOCKED：由于文件共享策略操作被拒绝",				 /* (16) The operation is rejected according to the file sharing policy */
	"FR_NOT_ENOUGH_CORE：无法分配长文件名工作区",		     /* (17) LFN working buffer could not be allocated */
	"FR_TOO_MANY_OPEN_FILES：当前打开的文件数大于_FS_SHARE", /* (18) Number of open files > _FS_SHARE */
	"FR_INVALID_PARAMETER：参数无效"	                     /* (19) Given parameter is invalid */
};
/**
	* @brief  fatfs文件系统初始化
	* @note   主要用于挂载文件系统
	* @param  None
  * @retval None
  */
int fatfs_init(void)
{
	FRESULT res_sd;                /* 文件操作结果 */
	FATFS_LinkDriver(&SD_Driver, SDPath);
	res_sd = f_mount(&fs,SDPath,1);
	/* 如果没有文件系统就格式化创建创建文件系统 */
	if(res_sd != FR_OK)
	{
//		drv_uart_debug(DEBUG_FATFS,"FATFS mount err\r\n");
		return -1;
	}
	return 0;
}


#ifdef FATFS_TEST
#include "pdflib.h"
#include "luat_pdflib.h"
const LPDF_BYTE RAW_IMAGE_DATA[128] = {
    0xff, 0xff, 0xff, 0xfe, 0xff, 0xff, 0xff, 0xfc,
    0xff, 0xff, 0xff, 0xf8, 0xff, 0xff, 0xff, 0xf0,
    0xf3, 0xf3, 0xff, 0xe0, 0xf3, 0xf3, 0xff, 0xc0,
    0xf3, 0xf3, 0xff, 0x80, 0xf3, 0x33, 0xff, 0x00,
    0xf3, 0x33, 0xfe, 0x00, 0xf3, 0x33, 0xfc, 0x00,
    0xf8, 0x07, 0xf8, 0x00, 0xf8, 0x07, 0xf0, 0x00,
    0xfc, 0xcf, 0xe0, 0x00, 0xfc, 0xcf, 0xc0, 0x00,
    0xff, 0xff, 0x80, 0x00, 0xff, 0xff, 0x00, 0x00,
    0xff, 0xfe, 0x00, 0x00, 0xff, 0xfc, 0x00, 0x00,
    0xff, 0xf8, 0x0f, 0xe0, 0xff, 0xf0, 0x0f, 0xe0,
    0xff, 0xe0, 0x0c, 0x30, 0xff, 0xc0, 0x0c, 0x30,
    0xff, 0x80, 0x0f, 0xe0, 0xff, 0x00, 0x0f, 0xe0,
    0xfe, 0x00, 0x0c, 0x30, 0xfc, 0x00, 0x0c, 0x30,
    0xf8, 0x00, 0x0f, 0xe0, 0xf0, 0x00, 0x0f, 0xe0,
    0xe0, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x00,
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};


void luat_pdf_test(void)
{
	int ret = 0;
	uint8_t page = 0;
	uint16_t height = 0; 
	ret = luat_pdflib_init();
	if(ret == -1)
	{
		drv_uart_debug(DEBUG_FATFS,"pdf new err\r\n");
		return;
	}
	ret = luat_pdflib_add_page(&page);
	if(ret == -1)
	{
		drv_uart_debug(DEBUG_FATFS,"pdf add page err\r\n");
		return;
	}
	ret = luat_pdflib_Page_GetHeight(page,&height);
	if(ret == -1)
	{
		drv_uart_debug(DEBUG_FATFS,"pdf get height err\r\n");
		return;
	}
	ret = luat_pdflib_Page_SetFontAndSize(page,"SimSun",24);
	if(ret == -1)
	{
		drv_uart_debug(DEBUG_FATFS,"pdf SetFont err\r\n");
		return;
	}
	for(uint8_t i =0; i< 8;i++)
	{
		ret = luat_pdflib_page_write(page,50,height-(i+1)*30,"文档测试 ",1);
		if(ret == -1)
		{
			drv_uart_debug(DEBUG_FATFS,"pdf write err\r\n");
			return;
		}
	}
	ret = luat_pdflib_page_SaveContext(page);
	if(ret == -1)
	{
		drv_uart_debug(DEBUG_FATFS,"pdf save context err\r\n");
		return;
	}
	ret = luat_pdflib_SaveToFile("0:log/red.pdf");
}

void pdf_test(void)
{
	LPDF_Doc pdf = { 0 };
	LPDF_Page page[2] = {0};
	LPDF_Image image = {0};
	LPDF_UINT16 width = 0,height = 0, i = 0;
	
	pdf = LPDF_New();
	if(pdf == NULL)
	{
		return;
	}
	page[0] = LPDF_AddPage(pdf);
	if(page[0] == NULL)
	{
		return;
	}
	width = LPDF_Page_GetWidth(page[0]);
	height = LPDF_Page_GetHeight(page[0]);
	LPDF_Page_SetFontAndSize(page[0], "SimSun", 24);
	LPDF_Page_BeginText(page[0]);
	LPDF_Page_SetRGBFill(page[0], 0.0, 1.0, 0.0);
	LPDF_Page_MoveTextPos(page[0], 50, height-20);
	while(font_list[i])
	{
		
		LPDF_Page_ShowText(page[0], "abcdefgABCDEFG12345!\043$\045&+-@?");
		LPDF_Page_MoveTextPos(page[0], 50, -30);
		i++;
	}
	LPDF_Page_SetFontAndSize(page[0],"SimSun",20);
	LPDF_Page_ShowText(page[0], "中文字体");
	LPDF_Page_EndText(page[0]);

	image = LPDF_LoadRawImageFromMem(pdf,RAW_IMAGE_DATA,32,32,LPDF_CS_DEVICE_GRAY,1);
	LPDF_Page_DrawImage(page[0], image, 50, 50, 32, 32);

	LPDF_Page_SaveContext(page[0]);
	page[1] = LPDF_AddPage(pdf);
	LPDF_Page_MoveTo(page[1], width/2-50, height-200);
	LPDF_Page_LineTo(page[1], width/2-50, height-300);
	LPDF_Page_LineTo(page[1], width/2-50+100, height-300);
	LPDF_Page_LineTo(page[1], width/2-50+100, height-200);
	LPDF_Page_LineTo(page[1], width/2-50, height-200);
	LPDF_Page_LineTo(page[1], width/2-50+40, height-140);
	LPDF_Page_LineTo(page[1], width/2-50+40+100, height-140);
	LPDF_Page_LineTo(page[1], width/2-50+100, height-200);
	LPDF_Page_Stroke(page[1]);
	LPDF_Page_MoveTo(page[1], width/2-50+40+100, height-140);
	LPDF_Page_LineTo(page[1], width/2-50+40+100, height-240);
	LPDF_Page_LineTo(page[1], width/2-50+100, height-300);
	LPDF_Page_Stroke(page[1]);
	LPDF_Page_SetLineWidth(page[1],2.5);
	LPDF_Page_MoveTo(page[1],width/2-50,height-300);
	LPDF_Page_LineTo(page[1],width/2-50,height-500);
	LPDF_Page_Stroke(page[1]);
	LPDF_Page_SaveContext(page[1]);
	LPDF_SaveToFile(pdf, "0:log/red.pdf");
  
}
char FsReadBuf[1024];
char FsWriteBuf[1024] = {"FatFS Write Demo \r\n www.armfly.com \r\n"};
//uint8_t g_TestBuf[BUF_SIZE];
/*
*********************************************************************************************************
*	函 数 名: CreateNewFile
*	功能说明: 在SD卡创建一个新文件，文件内容填写“www.armfly.com”
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void CreateNewFile(void)
{
	FRESULT result;
	uint32_t bw;
	char path[32];
	FIL file;
	/* 打开文件 */
	sprintf(path, "%sarmfly.txt", SDPath);
	result = f_open(&file, path, FA_CREATE_ALWAYS | FA_WRITE);
	if (result == FR_OK)
	{
		
		drv_uart_debug(DEBUG_FATFS,"armfly.txt 文件打开成功\r\n");
	}
	else
	{
		drv_uart_debug(DEBUG_FATFS,"armfly.txt 文件打开失败  (%s)\r\n", FR_Table[result]);
	}

	/* 写一串数据 */
	result = f_write(&file, FsWriteBuf, strlen(FsWriteBuf), &bw);
	if (result == FR_OK)
	{
		drv_uart_debug(DEBUG_FATFS,"armfly.txt 文件写入成功\r\n");
	}
	else
	{
		drv_uart_debug(DEBUG_FATFS,"armfly.txt 文件写入失败  (%s)\r\n", FR_Table[result]);
	}

	/* 关闭文件*/
	f_close(&file);

	/* 卸载文件系统 */
//	f_mount(NULL, DiskPath, 0);
}

void fatfs_test(void)
{
	luat_pdf_test();
//	CreateNewFile();
}
#endif



