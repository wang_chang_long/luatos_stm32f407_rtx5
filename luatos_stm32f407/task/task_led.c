#include "task_led.h"
#include "cmsis_os2.h"
#include "bsp.h"

#define LED_PORT_NAME     BSP_GPIOE9_40

#define TASK_LED_STACK_SIZE  128
osThreadId_t threadid_task_led = NULL;
static uint64_t task_led_stack[TASK_LED_STACK_SIZE/8]; //__attribute__((section("ccmram")));
const osThreadAttr_t threadattr_task_led = 
{
	.name = "task_led",
	.attr_bits = osThreadDetached, 
	.priority = osPriorityNormal,
	.stack_mem = task_led_stack,
	.stack_size = sizeof(task_led_stack),
};

/**
	* @brief  led运行任务
	* @note  
	* @param  None
  * @retval None
  */
void task_led_thread(void *argument)
{
	bsp_gpio_init(LED_PORT_NAME,GPIO_MODE_OUTPUT_PP,GPIO_PULLUP,GPIO_SPEED_LOW);
	while(1)
	{
		bsp_gpio_write(LED_PORT_NAME,GPIO_PIN_RESET);
		osDelay(1000);
		bsp_gpio_write(LED_PORT_NAME,GPIO_PIN_SET);
		osDelay(1000);
	}
}

/**
	* @brief  任务开始初始化
	* @note   用于创建开始线程任务
	* @param  None
  * @retval None
  */
void task_led_init(void)
{
	threadid_task_led = osThreadNew(task_led_thread,NULL,&threadattr_task_led);
}


