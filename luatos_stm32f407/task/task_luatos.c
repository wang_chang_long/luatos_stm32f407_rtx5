#include "task_luatos.h"
#include "cmsis_os2.h"
#include "luat_base.h"
#include "bget.h"
#include "stdio.h"
#define LUAT_HEAP_SIZE (64*1024)
#define TASK_LUATOS_STACK_SIZE   10240

osThreadId_t threadid_task_luatos = NULL;
static uint64_t task_luatos_stack[TASK_LUATOS_STACK_SIZE/8]; //__attribute__((section("ccmram")));
const osThreadAttr_t threadattr_task_luatos = 
{
	.name = "task_luatos",
	.attr_bits = osThreadDetached, 
	.priority = osPriorityNormal1,
	.stack_mem = task_luatos_stack,
	.stack_size = sizeof(task_luatos_stack),
};

uint8_t luavm_heap[LUAT_HEAP_SIZE] __attribute__((section("ccmram")));
//uint8_t luavm_heap[LUAT_HEAP_SIZE];
void task_luatos_thread(void *argument)
{
	bpool(luavm_heap,LUAT_HEAP_SIZE);
	luat_main();
	while(1)
	{
		osDelay(5000);
	}
}

/**
	* @brief  任务开始初始化
	* @note   用于创建开始线程任务
	* @param  None
  * @retval None
  */
void task_luatos_init(void)
{
	threadid_task_luatos = osThreadNew(task_luatos_thread,NULL,&threadattr_task_luatos);
}
