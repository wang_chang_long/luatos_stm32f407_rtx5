#include "task.h"
#include "cmsis_os2.h"
#include "driver.h"
#include "bsp.h"
#include "common.h"
#include "periph.h"
#include "fatfs.h"
#include "luat_multimeter.h"

#define TASK_START_STACK_SIZE  512

osThreadId_t threadid_task_start = NULL;
static uint64_t task_start_stack[TASK_START_STACK_SIZE/8];
const osThreadAttr_t threadattr_task_start = 
{
	.name = "task_start",
	.attr_bits = osThreadDetached, 
	.priority = osPriorityAboveNormal,
	.stack_mem = task_start_stack,
	.stack_size = sizeof(task_start_stack),
};

/**
	* @brief  开始线程初始化
	* @note   
	* @param  None
  * @retval None
  */
void task_start_thread(void *argument)
{
	bsp_init();
	drv_init();
	per_init();
//	fatfs_test();
	task_luatos_init();
	task_led_init();
	osThreadExit();
}

/**
	* @brief  开始任务初始化
	* @note   
	* @param  None
  * @retval None
  */
void task_start_init(void)
{
	threadid_task_start = osThreadNew(task_start_thread,NULL,&threadattr_task_start);
}






