#ifndef __TASK_NET_H
#define __TASK_NET_H

#include "Net_Config_TCP.h"
#include "rl_net.h"
#include "stdint.h"
//#define WIRE_IP1           	 124
//#define WIRE_IP2           	 128
//#define WIRE_IP3             194
//#define WIRE_IP4             82
//#define WIRE_PORT	         1888//服务器端口号

#define WIRE_IP1           	 112
#define WIRE_IP2           	 125
#define WIRE_IP3             89
#define WIRE_IP4             8
#define WIRE_PORT	         35850//服务器端口号
#define LocalPort_NUM    	 1024//本地端口

#define ETH_DOWN_NUM					0
#define ETH_DOWN_EVENT					(1<<ETH_DOWN_NUM)
#define ETH_UPLINK_NUM					1
#define ETH_UPLINK_EVENT				(1<<ETH_UPLINK_NUM)

#define TCP_CONNECT_NUM                 2
#define TCP_CONNECT_ENVENT				(1<<TCP_CONNECT_NUM)
#define TCP_ESTABLISHED_NUM             3
#define TCP_ESTABLISHED_EVNET			(1<<TCP_ESTABLISHED_NUM)
#define TCP_CLOSE_NUM                 	4
#define TCP_CLOSE_EVENT					(1<<TCP_CLOSE_NUM)
#define TCP_ABROATED_NUM                5
#define TCP_ABROATED_EVENT				(1<<TCP_ABROATED_NUM)
//#define TCP_EVENTACK_NUM                6
//#define TCP_EVENTACK_EVENT				(1<<TCP_EVENTACK_NUM)
//#define TCP_RECVDATA_NUM                7
//#define TCP_RECVDATA_EVENT				(1<<TCP_RECVDATA_NUM)
#define TCP_RECV_NUM                    4

#define WIRE_ALL_EVENT					(ETH_DOWN_EVENT|TCP_CLOSE_EVENT|TCP_ABROATED_EVENT)
typedef enum
{
	ETH_INIT,
	ETH_UPLINK,
	WIRE_TCP_INIT,
	WIRE_TCP_CONNECT,
	WIRE_TCP_ESTABLISHED,
	WIRE_TCP_PROC,
}Wire_State_Enum;


#define WIRE_MAC_LEN  						6
#define WIRE_HOST_NAME_LEN					10

typedef enum 
{
	NET_INIT=0,
	NET_TCPUP,
	NET_PROC
}WireNet_State_e;

typedef struct
{
	uint8_t Speed;
	uint8_t Duplex;
}WireNet_Info_t;

typedef struct
{
	uint16_t len;
	uint8_t  *buff;
}tcp_data_t;

uint8_t WireNet_GetEthState(void);
uint8_t WireNet_GetTcpState(void);
void WireNet_SetNetPara(netIF_Option Option,uint8_t *pData,uint8_t len);
void WireNet_TaskCreated(void);
uint8_t Tcp_SendData(uint8_t *pData,uint32_t Len);
uint8_t Tcp_CheckStatus(void);
uint32_t Tcp_GetRecvCount(void);
void task_net_init(void);
tcp_data_t tcp_wait_recv_dat(uint32_t timeout);
void tcp_clear_recv_dat(tcp_data_t *tcp_data);
#endif

