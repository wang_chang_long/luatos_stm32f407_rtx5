#include "task_swd.h"
#include "cmsis_os2.h"
#include "ff.h"
#include "swd_host.h"
#include "SWD_flash.h"
#include "common.h"
#include "ff_test.h"
#include "ff_gen_drv.h"
#include "sd_diskio.h"
#include "string.h"
#include "stdio.h"
#include "luat_swd.h"
#define Flash_Start_Addr 0x08000000
#define FLASH_BUFF_MAX_SIZE   0X400
#define TASK_SWD_TEST

#ifdef TASK_SWD_TEST
uint8_t task_swd_erase_test(char *firmware_name,char *program_name);

#endif

osThreadId_t threadid_task_swd = NULL;
const osThreadAttr_t threadattr_task_swd = 
{
	.name = "task_swd",
	.attr_bits = osThreadDetached, 
	.priority = osPriorityAboveNormal1,
	.stack_size = 10240,
};

//extern  program_target_t flash_algo;
/**
	* @brief  任务开始线程，此线程用于创建其他应用程序线程
	* @note   用于创建开始线程任务
	* @param  None
  * @retval None
  */
void task_swd_thread(void *argument)
{
	int ret = 0;
	task_swd_erase_test(NULL,NULL);
	ret = luat_swd_init("stm32f10x_512.bin");
	if(ret == 0)
	{
		ret = luat_swd_erase(Flash_Start_Addr);
		if(ret == 0)
		{
			ret = luat_swd_program("test_program.bin",Flash_Start_Addr);
			if(ret == 0)
			{
				luat_swd_reset();
			}
		}
	}
//	task_swd_erase_test("stm32f10x_512.bin","test_program.bin");
	while(1)
	{
		osDelay(1000);
	}
}

/**
	* @brief  任务开始初始化
	* @note   用于创建开始线程任务
	* @param  None
  * @retval None
  */
void task_swd_init(void)
{
	threadid_task_swd = osThreadNew(task_swd_thread,NULL,&threadattr_task_swd);
}

#ifdef TASK_SWD_TEST
char SDPath_a[4]; /* SD逻辑驱动器路径 */
FATFS fs_test;													/* FatFs文件系统对象 */
uint8_t task_swd_erase_test(char *firmware_name,char *program_name)
{
	uint32_t flash_code[256] = {0};
	program_target_t flash_algo = {0};
	char temp[50] = {0};
	uint8_t re_satus = 1;
	
	FRESULT res_sd;                /* 文件操作结果 */
	UINT fnum;            			  /* 文件成功读写数量 */
	FIL fnew_swd;													/* 文件对象 */
	BYTE ReadBuffer[FLASH_BUFF_MAX_SIZE]={0};        /* 读缓冲区 */
	BYTE Check_Data[FLASH_BUFF_MAX_SIZE]={0};
	
	uint32_t cnt = 0;
	uint32_t i = 0;
	error_t err = ERROR_SUCCESS;
	cnt = 0;
	FATFS_LinkDriver(&SD_Driver, SDPath_a);
//在外部SD卡挂载文件系统，文件系统挂载时会对SD卡初始化
 res_sd = f_mount(&fs_test,"0:",1);  

 /*----------------------- 格式化测试 ---------------------------*/  
 /* 如果没有文件系统就格式化创建创建文件系统 */
 if(res_sd == FR_NO_FILESYSTEM)
 {
		/* 格式化 */
		res_sd=f_mkfs("0:",0,0);							

		if(res_sd == FR_OK)
		{
			/* 格式化后，先取消挂载 */
			res_sd = f_mount(NULL,"0:",1);			
			/* 重新挂载	*/			
			res_sd = f_mount(&fs_test,"0:",1);
		}
		else
		{
//			uart_debug(1,"sd format error\r\n");
			return 0;;
		}
 }
 else if(res_sd!=FR_OK)
 {
		while(1);
 }
 sprintf(temp,"0:firmware/%s",firmware_name);
 res_sd = f_open(&fnew_swd,temp,FA_OPEN_EXISTING | FA_READ);
 if(res_sd != FR_OK)
 {
	 uart_debug(1,"固件名字错误\r\n");
		return -1; 
 }
 res_sd = f_read(&fnew_swd,&flash_algo,sizeof(program_target_t),&fnum);
 flash_algo.algo_blob = flash_code;
 res_sd = f_read(&fnew_swd,flash_code,flash_algo.algo_size,&fnum);
 f_close(&fnew_swd);
	err = target_flash_init(&flash_algo,Flash_Start_Addr);
	err = target_flash_erase_chip(&flash_algo);
	if(err == ERROR_SUCCESS)
	{
		uart_debug(1,"flash erase ok\r\n");
	}
	else
	{
		re_satus = 0;
		uart_debug(1,"flash erase err\r\n");
	}
	sprintf(temp,"0:program/%s",program_name);
	res_sd = f_open(&fnew_swd, temp, FA_OPEN_EXISTING | FA_READ);
	if(res_sd == FR_OK)
	{
		cnt = fnew_swd.fsize / FLASH_BUFF_MAX_SIZE;
		if((fnew_swd.fsize % FLASH_BUFF_MAX_SIZE) != 0)
		{
			cnt+=1;
		}
		for(i = 0;i<cnt;i++)
		{
			res_sd = f_read(&fnew_swd, ReadBuffer, sizeof(ReadBuffer), &fnum); 
			if(fnum > 0)
			{
				if(target_flash_program_page(&flash_algo,Flash_Start_Addr + i * FLASH_BUFF_MAX_SIZE,ReadBuffer,FLASH_BUFF_MAX_SIZE) != ERROR_SUCCESS)
				{
					uart_debug(1,"Program Fault = %d\r\n",i);
					re_satus = 0;
					break;
				}
				else
				{
					uart_debug(1,"Program write cnt = %d\r\n",i);
				}
				swd_read_memory(Flash_Start_Addr + i * FLASH_BUFF_MAX_SIZE,Check_Data,FLASH_BUFF_MAX_SIZE);
				if(memcmp(Check_Data,ReadBuffer,fnum)!=0)
				{
					uart_debug(1,"Verify Flash Fault %d\r\n",i);
					re_satus = 0;
					break;
				}
				memset(ReadBuffer,0xff,FLASH_BUFF_MAX_SIZE);
				memset(Check_Data,0xff,FLASH_BUFF_MAX_SIZE);
			}
			else
			{
				uart_debug(1,"program flie error\r\n");
				re_satus = 0;
				break;
			}
		}
		if(re_satus == 1)
		{
			uart_debug(1,"program flie ok\r\n");
			if(target_flash_uninit() != ERROR_SUCCESS)
			{
				uart_debug(1,"program reset error,please manual reset\r\n");
			}
		}
		f_close(&fnew_swd);
	}
	return re_satus;
}
#endif
