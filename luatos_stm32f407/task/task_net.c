#include "task_net.h"
#include "cmsis_os2.h"
#include "common.h"
#include "string.h"
#include "main.h"
#include "driver.h"

#define TASK_NET_STACK_SIZE  512

osThreadId_t threadid_task_net = NULL;
static uint64_t task_net_stack[TASK_NET_STACK_SIZE/8]; //__attribute__((section("ccmram")));
const osThreadAttr_t threadattr_task_net = 
{
	.name = "task_net",
	.attr_bits = osThreadDetached, 
	.priority = osPriorityNormal,
	.stack_mem = task_net_stack,
	.stack_size = sizeof(task_net_stack),
};
osTimerId_t Timer_TcpCheck=NULL;
osMessageQueueId_t MsgQueue_TcpRecvData=NULL;
int32_t WireNet_TcpSocket=-1;
/*
*********************************************************************************************************
*	函 数 名: tcp_cb_client
*	功能说明: TCP Socket的回调函数
*	形    参: socket  句柄
*             event   事件类型
*             addr    NET_ADDR类型变量，记录IP地址，端口号。
*             buf     ptr指向的缓冲区记录着接收到的TCP数据。
*             len     记录接收到的数据个数。
*	返 回 值: 
*********************************************************************************************************
*/
uint32_t tcp_cb_client (int32_t socket, netTCP_Event event,
                        const NET_ADDR *addr, const uint8_t *buf, uint32_t len) 
{
	tcp_data_t tcp_recv_data = {0};
	switch (event) 
	{
		/*
			远程客户端连接消息
		    1、数组ptr存储远程设备的IP地址，par中存储端口号。
		    2、返回数值1允许连接，返回数值0禁止连接。
		*/
		case netTCP_EventConnect:
			return (1);

		/* Socket远程连接已经建立 */
		case netTCP_EventEstablished:
//			uart_debug(DEBUG_LEVEL_RUN,"TCP Established\n");
			osThreadFlagsSet(threadid_task_net,TCP_ESTABLISHED_EVNET);
			break;

		/* 连接断开 */
		case netTCP_EventClosed:
//			debug_logger(DEBUG_LEVEL_RUN,"TCP Closed\n");
			osThreadFlagsSet(threadid_task_net,TCP_CLOSE_EVENT);
			break;

		/* 连接终止 */
		case netTCP_EventAborted:
//			debug_logger(DEBUG_LEVEL_RUN,"TCP aborted\n");
			osThreadFlagsSet(threadid_task_net,TCP_ABROATED_EVENT);
			break;

		/* 发送的数据收到远程设备应答 */
		case netTCP_EventACK:
//			debug_logger(DEBUG_LEVEL_RUN,"TCP ack\n");
			break;
		/* 接收到TCP数据帧，ptr指向数据地址，par记录数据长度，单位字节 */
		case netTCP_EventData:
				tcp_recv_data.len = len;
				tcp_recv_data.buff = drv_alloc_malloc(DRV_ALLOC_SIZE_512);
				osMessageQueuePut(MsgQueue_TcpRecvData,&tcp_recv_data,NULL,0);
			break;
	}
	return (0);
}
/**
	* @brief  任务开始线程，此线程用于创建其他应用程序线程
	* @note   用于创建开始线程任务
	* @param  None
  * @retval None
  */
void task_net_thread(void *argument)
{
	uint32_t Eth_EventFlag=0;
	uint8_t CheckCount=0;
	Wire_State_Enum Wire_State=ETH_INIT;
	NET_ADDR4 WireAddr_Ip4 = { NET_ADDR_IP4, WIRE_PORT, WIRE_IP1,WIRE_IP2,WIRE_IP3,WIRE_IP4};//网络
	netInitialize();//初始化网络
	while(1)
	{
		switch(Wire_State)
		{
			case ETH_INIT:
				netDHCP_Enable(NET_IF_CLASS_ETH|0);//使能DHCP
				Wire_State=ETH_UPLINK;
			break;
			case ETH_UPLINK:
				Eth_EventFlag=osThreadFlagsWait(ETH_UPLINK_EVENT,osFlagsWaitAny,osWaitForever);//等待网线插入
				if(osflag_check(Eth_EventFlag)==0)
				{
					break;
				}
				if((Eth_EventFlag&ETH_UPLINK_EVENT)==ETH_UPLINK_EVENT)
				{
					Wire_State=WIRE_TCP_INIT;
				}
			break;
			case WIRE_TCP_INIT:
				WireNet_TcpSocket=netTCP_GetSocket (tcp_cb_client);	//创建SOCKET
				if (WireNet_TcpSocket > 0) 
				{
					/* 使能TCP_TYPE_KEEP_ALIVE，会一直保持连接 */
					netTCP_SetOption(WireNet_TcpSocket,netTCP_OptionTimeout,120);
					Wire_State=WIRE_TCP_CONNECT;
				}
			case WIRE_TCP_CONNECT://tcp连接
				CheckCount=0;
				if (WireNet_TcpSocket > 0)
				{
					if(netTCP_GetState(WireNet_TcpSocket) != netTCP_StateESTABLISHED)
					{
						netTCP_Connect (WireNet_TcpSocket, (NET_ADDR *)&WireAddr_Ip4, LocalPort_NUM);
						Eth_EventFlag=osThreadFlagsWait(TCP_ESTABLISHED_EVNET,osFlagsWaitAny,30000);//等待TCP连接建立
						if(osflag_check(Eth_EventFlag)==0)
						{
							break;
						}
						if((Eth_EventFlag&TCP_ESTABLISHED_EVNET)==TCP_ESTABLISHED_EVNET)
						{
							Wire_State=WIRE_TCP_ESTABLISHED;
						}
					}
					else
					{
						Wire_State=WIRE_TCP_ESTABLISHED;
					}
				}
			break;
			case WIRE_TCP_ESTABLISHED:
				if(Tcp_CheckStatus())
				{
					CheckCount=0;
					Wire_State=WIRE_TCP_PROC;
				}
				else
				{
					osDelay(100);
					if(CheckCount++>50)
					{
						Wire_State=WIRE_TCP_CONNECT;
					}
				}
			break;
			case WIRE_TCP_PROC:
				Eth_EventFlag=osThreadFlagsWait(WIRE_ALL_EVENT,osFlagsWaitAny,osWaitForever);//等待其他事件产生
				if(osflag_check(Eth_EventFlag)==0)
				{
					break;
				}
				if((Eth_EventFlag&ETH_DOWN_EVENT)==ETH_DOWN_EVENT)
				{
					netUninitialize();//释放所有网络资源，含TCPnet内核任务和ETH接口任务
					osDelay(10);
					netInitialize();//重新初始化
					osDelay(10);
					Wire_State=ETH_INIT;
				}
				else if(((Eth_EventFlag&TCP_CLOSE_EVENT)==TCP_CLOSE_EVENT)||((Eth_EventFlag&TCP_ABROATED_EVENT)==TCP_ABROATED_EVENT))//tcp连接断开
				{
					netTCP_Close(WireNet_TcpSocket);//断开TCP连接
					osDelay(10);
					Wire_State=WIRE_TCP_CONNECT;
				}
			break;
			default:
			break;
		}
	}	
}

/*
*********************************************************************************************************
*	函 数 名: netETH_Notify
*	功能说明: 以太网状态消息
*	形    参: 
*	返 回 值: 无
*********************************************************************************************************
*/
void netETH_Notify (uint32_t if_num, netETH_Event event, uint32_t val) 
{
//	NET_ETH_LINK_INFO *info;
	__IO static uint8_t g_ucEthLinkStatus=0;//以太网连接状态，0和1都表示初始临时状态，2表示连接上，3表示断开
	switch (event) 
	{
	case netETH_LinkDown:
		if(g_ucEthLinkStatus == 2)
		{
			osThreadFlagsSet(threadid_task_net,ETH_DOWN_EVENT);
			g_ucEthLinkStatus = 3;
		}
		else
		{
			osThreadFlagsClear(ETH_DOWN_EVENT);
			g_ucEthLinkStatus = 1;
		}
		break;
	case netETH_LinkUp:
		g_ucEthLinkStatus = 2;
		osThreadFlagsSet(threadid_task_net,ETH_UPLINK_EVENT);
		break;
	case netETH_Wakeup:

		break;
	case netETH_TimerAlarm:

		break;
	}
}
/**
	* @brief  监测TCP连接状态
	* @note   
	* @param  
  * @retval None
  */
uint8_t Tcp_CheckStatus(void)
{
	if(WireNet_TcpSocket>0)
	{
		if(netTCP_GetState(WireNet_TcpSocket) == netTCP_StateESTABLISHED)
		{
			return 1;
		}
	}
	return 0;
}

/**
	* @brief  定时器回调函数
	* @note   
	* @param  
  * @retval None
  */
void Eth_Check_Cb(void *argument)
{
	if(Tcp_CheckStatus()==0)
	{
		osThreadFlagsSet(threadid_task_net,TCP_ABROATED_EVENT);
	}
}
/**
	* @brief  TCP数据
	* @note  
	* @param  pData：数据缓存区
	*					len：数据长度
  * @retval None
  */
uint8_t Tcp_SendData(uint8_t *pData,uint32_t Len)
{
	uint8_t *sendbuf;
	uint32_t maxlen=0;
	uint8_t timeout=10;//重试次数
	if(Tcp_CheckStatus())
	{
		do
		{
			if(netTCP_SendReady(WireNet_TcpSocket)==true)//检查现在是否可以发送数据
			{
				maxlen  = netTCP_GetMaxSegmentSize(WireNet_TcpSocket);
				if(Len<maxlen)
				{
					sendbuf = netTCP_GetBuffer (Len);//创建发送数据空间
					memcpy(sendbuf,pData,Len);
					if(netTCP_Send (WireNet_TcpSocket, sendbuf, Len)==netOK)/* 必须使用申请的内存空间 */
					{
						return 1;
					}
				}
			}
			osDelay(1);
		}while(timeout--);
	}
	return 0;
}
/**
	* @brief  TCP等待数据接收
	* @note  
	* @param  timeout：超时时间
  * @retval None
  */
tcp_data_t tcp_wait_recv_dat(uint32_t timeout)
{
	tcp_data_t tcp_recv_dat = {0};
	osMessageQueueGet(MsgQueue_TcpRecvData,&tcp_recv_dat,NULL,timeout);
	return tcp_recv_dat;
}
/**
	* @brief  TCP等待数据接收
	* @note  
	* @param  timeout：超时时间
  * @retval None
  */
void tcp_clear_recv_dat(tcp_data_t *tcp_data)
{
	if(tcp_data->buff != NULL)
	{
		drv_alloc_free(DRV_ALLOC_SIZE_512,tcp_data->buff);
		tcp_data->buff = NULL;
	}
}
/**
	* @brief  TCP数据
	* @note  
	* @param  pData：数据缓存区
	*					len：数据长度
  * @retval None
  */


/**
	* @brief  网络任务初始化
	* @note  
	* @param  None
  * @retval None
  */
void task_net_init(void)
{
	threadid_task_net = osThreadNew(task_net_thread,NULL,&threadattr_task_net);
	Timer_TcpCheck=osTimerNew(Eth_Check_Cb,osTimerPeriodic,NULL,NULL);
	MsgQueue_TcpRecvData = osMessageQueueNew(TCP_RECV_NUM,sizeof(tcp_data_t),NULL);
}


