#include "task_multimeter.h"
#include "cmsis_os2.h"
#include "driver_multimeter.h"
#include "periph.h"
osThreadId_t threadid_task_multimeter = NULL;
const osThreadAttr_t threadattr_task_multimeter = 
{
	.name = "task_multimeter",
	.attr_bits = osThreadDetached, 
	.priority = osPriorityNormal1,
	.stack_size = 1024,
};

drv_multimeter_desc_t multimeter_desc;

/**
	* @brief  任务开始线程，此线程用于创建其他应用程序线程
	* @note   用于创建开始线程任务
	* @param  None
  * @retval None
  */
void task_multimeter_thread(void *argument)
{
	uint32_t adc_value;
	uint32_t thread_flag = 0;
	
	if(multimeter_desc.current_fun == 9)
	{
		per_mcp3421_set_param(MCP3421_MODE_CONTINUITY,MCP3421_PGA_X1,MCP3421_SPS_60);
	}
	else
	{
		per_mcp3421_set_param(MCP3421_MODE_CONTINUITY,MCP3421_PGA_X1,MCP3421_SPS_3_75);
	}
	while(1)
	{
		thread_flag = osThreadFlagsWait(MULTIMETER_EVENT_BEEP | MULTIMETER_EVENT_OTHER,osFlagsWaitAny,10);
		if(osflag_check(thread_flag))
		{
			if(thread_flag & MULTIMETER_EVENT_BEEP)
			{
				per_mcp3421_set_param(MCP3421_MODE_CONTINUITY,MCP3421_PGA_X1,MCP3421_SPS_60);
			}
			else if(thread_flag & MULTIMETER_EVENT_OTHER)
			{
				per_mcp3421_set_param(MCP3421_MODE_CONTINUITY,MCP3421_PGA_X1,MCP3421_SPS_3_75);
			}
		}
		if(multimeter_desc.current_fun == 9)
		{
			osDelay(10);
			adc_value = per_mcp3421_read();
			drv_measure_beep(&multimeter_desc,adc_value);
		}
		else
		{
			osDelay(100 * multimeter_desc.current_freq);
			adc_value = per_mcp3421_read();
			switch(multimeter_desc.current_fun)
			{
				case 1:
					drv_measure_dcv(&multimeter_desc,adc_value);
				break;
				case 2:
					drv_measure_acv(&multimeter_desc,adc_value);
				break;
				case 3:
					drv_measure_dcma(&multimeter_desc,adc_value);
				break;
				case 4:
					drv_measure_dca(&multimeter_desc,adc_value);
				break;
				case 5:
					drv_measure_acma(&multimeter_desc,adc_value);
				break;
				case 6:
					drv_measure_aca(&multimeter_desc,adc_value);
				break;
				case 7:
					drv_measure_r(&multimeter_desc,adc_value);
				break;
				case 8:
					drv_measure_c(&multimeter_desc,adc_value);
				break;
				case 9:
					drv_measure_beep(&multimeter_desc,adc_value);
				break;
			}
		}
		osDelay(500);
	}
}

/**
	* @brief  任务开始初始化
	* @note   用于创建开始线程任务
	* @param  None
  * @retval None
  */
void task_multimeter_init(void)
{
	drv_multimeter_init(&multimeter_desc);
	threadid_task_multimeter = osThreadNew(task_multimeter_thread,NULL,&threadattr_task_multimeter);
}


void task_multimeter_set_fun(uint8_t fun)
{
	multimeter_desc.current_fun = fun;
}

void task_multimeter_set_zero(void)
{
	multimeter_desc.zero_b = 1;
}







