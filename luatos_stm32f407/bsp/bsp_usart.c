/**
  ******************************************************************************
  * File Name          : USART.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "bsp_usart.h"

bsp_uart_list_t bsp_uart_list[] =
{
	{USART1,0,NULL},
	{USART2,0,NULL},
	{USART3,0,NULL},
	{USART6,0,NULL}
};
/**
	* @brief  注册串口接收函数
	* @note   
	* @param  uart_name：串口名字
	*					uart_recv_cb：串口接收函数
  * @retval -1 错误  0 正确
  */
int bsp_uart_recv_register(bsp_uart_name_t uart_name,uart_cb_t uart_recv_cb)
{
	if(uart_name >= BSP_UART_END)
	{
		return -1;
	}
	if(uart_recv_cb != NULL)
	{
		bsp_uart_list[uart_name].uart_cb = uart_recv_cb;
	}
	return 0;
}
/**
	* @brief  通过串口发送数据
	* @note   
	* @param  uart_name：串口名字
	*					dat：数据指针
	*					len：数据长度
  * @retval -1 错误  0 正确
  */
int bsp_uart_send_dat(bsp_uart_name_t uart_name,uint8_t *dat,uint16_t len)
{
	HAL_StatusTypeDef status = HAL_OK;
	if(uart_name >= BSP_UART_END)
	{
		return -1;
	}
	status = HAL_UART_Transmit(&bsp_uart_list[uart_name].huart,dat,len,0xffff);
	if(status == HAL_OK)
	{
		return 0;
	}
	return -1;
}
/**
	* @brief  串口接收1字节数据回调函数
	* @note   
	* @param  huart：串口描述符
  * @retval -1 错误  0 正确
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	uint8_t Ret=HAL_OK;
	uint8_t i=0;
	for(i = 0;i < BSP_UART_END;i++)
	{
		if(huart->Instance == bsp_uart_list[i].huart.Instance)
		{
			break;
		}
	}
	if(i == BSP_UART_END)
	{
		return;
	}
	if(bsp_uart_list[i].uart_cb != NULL)
	{
		bsp_uart_list[i].uart_cb((bsp_uart_name_t)i,bsp_uart_list[i].dat);
	}
	do
	{
		Ret=HAL_UART_Receive_IT(huart,&bsp_uart_list[i].dat,1);
	}while(Ret!=HAL_OK);
}
/**
	* @brief  串口初始化函数
	* @note   
	* @param  uart_name：串口名字
	*					bau：串口波特率
	*					even：校验位
	*					dat_len：数据长度
	*					stop_bits：停止位
  * @retval -1 错误  0 正确
  */
int bsp_uart_init(bsp_uart_name_t uart_name,uint32_t bau, uint8_t even, uint8_t dat_len, uint8_t stop_bits)
{
	UART_HandleTypeDef *huart = NULL;
	if(uart_name >= BSP_UART_END)
	{
		return -1;
	}
	huart = &bsp_uart_list[uart_name].huart;
	huart->Instance = bsp_uart_list[uart_name].uart_port;
	if(bau >= 1200 && bau <= 115200)
		huart->Init.BaudRate = bau;
	else
		huart->Init.BaudRate = 115200;
	
	switch(even)
	{
		case 'N':	huart->Init.Parity = UART_PARITY_NONE;break;
		case 'E':	huart->Init.Parity = UART_PARITY_EVEN;break;
		case 'O':	huart->Init.Parity = UART_PARITY_ODD;break;
		default:	huart->Init.Parity = UART_PARITY_NONE;break;
	}
	switch(dat_len)
	{
		case 8:	huart->Init.WordLength = UART_WORDLENGTH_8B;break;
		case 9:	huart->Init.WordLength = UART_WORDLENGTH_9B;break;
		default:huart->Init.WordLength = UART_WORDLENGTH_8B;break;
	}
	switch(stop_bits)
	{
		case 1:	huart->Init.StopBits = UART_STOPBITS_1;break;
		case 2:	huart->Init.StopBits = UART_STOPBITS_2;break;
		default:huart->Init.StopBits = UART_STOPBITS_1;break;
	}
  huart->Init.Mode = UART_MODE_TX_RX;
  huart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart->Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(huart) != HAL_OK)
  {
    Error_Handler();
  }
	HAL_UART_Receive_IT(huart,&bsp_uart_list[uart_name].dat,1);
	return 0;
}
/**
	* @brief  串口反初始化函数
	* @note   
	* @param  uart_name：串口名字
  * @retval -1 错误  0 正确
  */
int bsp_uart_deinit(bsp_uart_name_t uart_name)
{
	if(uart_name >= BSP_UART_END)
	{
		return -1;
	}
	HAL_UART_DeInit(&bsp_uart_list[uart_name].huart);
	return 0;
}
/**
	* @brief  串口引脚及中断相关初始化
	* @note   
	* @param  uartHandle：串口描述符
  * @retval -1 错误  0 正确
  */
void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspInit 0 */

  /* USER CODE END USART1_MspInit 0 */
    /* USART1 clock enable */
    __HAL_RCC_USART1_CLK_ENABLE();

    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**USART1 GPIO Configuration
    PB6     ------> USART1_TX
    PB7     ------> USART1_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* USART1 interrupt Init */
    HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspInit 1 */

  /* USER CODE END USART1_MspInit 1 */
  }
  else if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspInit 0 */

  /* USER CODE END USART2_MspInit 0 */
    /* USART2 clock enable */
    __HAL_RCC_USART2_CLK_ENABLE();

    __HAL_RCC_GPIOD_CLK_ENABLE();
    /**USART2 GPIO Configuration
    PD5     ------> USART2_TX
    PD6     ------> USART2_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    /* USART2 interrupt Init */
    HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* USER CODE BEGIN USART2_MspInit 1 */

  /* USER CODE END USART2_MspInit 1 */
  }
  else if(uartHandle->Instance==USART3)
  {
  /* USER CODE BEGIN USART3_MspInit 0 */

  /* USER CODE END USART3_MspInit 0 */
    /* USART3 clock enable */
    __HAL_RCC_USART3_CLK_ENABLE();

    __HAL_RCC_GPIOD_CLK_ENABLE();
    /**USART3 GPIO Configuration
    PD8     ------> USART3_TX
    PD9     ------> USART3_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART3;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

		/* USART6 interrupt Init */
    HAL_NVIC_SetPriority(USART3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART3_IRQn);
  /* USER CODE BEGIN USART3_MspInit 1 */

  /* USER CODE END USART3_MspInit 1 */
  }
  else if(uartHandle->Instance==USART6)
  {
  /* USER CODE BEGIN USART6_MspInit 0 */

  /* USER CODE END USART6_MspInit 0 */
    /* USART6 clock enable */
    __HAL_RCC_USART6_CLK_ENABLE();

    __HAL_RCC_GPIOC_CLK_ENABLE();
    /**USART6 GPIO Configuration
    PC6     ------> USART6_TX
    PC7     ------> USART6_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* USART6 interrupt Init */
    HAL_NVIC_SetPriority(USART6_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART6_IRQn);
  /* USER CODE BEGIN USART6_MspInit 1 */

  /* USER CODE END USART6_MspInit 1 */
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspDeInit 0 */

  /* USER CODE END USART1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART1_CLK_DISABLE();

    /**USART1 GPIO Configuration
    PB6     ------> USART1_TX
    PB7     ------> USART1_RX
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_6|GPIO_PIN_7);

    /* USART1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspDeInit 1 */

  /* USER CODE END USART1_MspDeInit 1 */
  }
  else if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspDeInit 0 */

  /* USER CODE END USART2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART2_CLK_DISABLE();

    /**USART2 GPIO Configuration
    PD5     ------> USART2_TX
    PD6     ------> USART2_RX
    */
    HAL_GPIO_DeInit(GPIOD, GPIO_PIN_5|GPIO_PIN_6);

    /* USART2 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART2_IRQn);
  /* USER CODE BEGIN USART2_MspDeInit 1 */

  /* USER CODE END USART2_MspDeInit 1 */
  }
  else if(uartHandle->Instance==USART3)
  {
  /* USER CODE BEGIN USART3_MspDeInit 0 */

  /* USER CODE END USART3_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART3_CLK_DISABLE();

    /**USART3 GPIO Configuration
    PD8     ------> USART3_TX
    PD9     ------> USART3_RX
    */
    HAL_GPIO_DeInit(GPIOD, GPIO_PIN_8|GPIO_PIN_9);
		
		/* USART3 interrupt Deinit */
		HAL_NVIC_DisableIRQ(USART3_IRQn);
  /* USER CODE BEGIN USART3_MspDeInit 1 */

  /* USER CODE END USART3_MspDeInit 1 */
  }
  else if(uartHandle->Instance==USART6)
  {
  /* USER CODE BEGIN USART6_MspDeInit 0 */

  /* USER CODE END USART6_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART6_CLK_DISABLE();

    /**USART6 GPIO Configuration
    PC6     ------> USART6_TX
    PC7     ------> USART6_RX
    */
    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_6|GPIO_PIN_7);

    /* USART6 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART6_IRQn);
  /* USER CODE BEGIN USART6_MspDeInit 1 */

  /* USER CODE END USART6_MspDeInit 1 */
  }
}

