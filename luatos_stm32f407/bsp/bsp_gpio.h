/**
  ******************************************************************************
  * File Name          : gpio.h
  * Description        : This file contains all the functions prototypes for
  *                      the gpio
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_GPIO_H
#define __BSP_GPIO_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

typedef enum
{
	BSP_GPIOA4_29,
	BSP_GPIOA5_30,
	BSP_GPIOA10_69,
	BSP_GPIOA11_70,
	BSP_GPIOA15_77,
	BSP_GPIOB3_89,
	BSP_GPIOB4_90,
	BSP_GPIOB5_91,
	BSP_GPIOB8_95,
	BSP_GPIOB9_96,
	BSP_GPIOD0_81,
	BSP_GPIOD1_82,
	BSP_GPIOD3_84,
	BSP_GPIOD4_85,
	BSP_GPIOD7_88,
	BSP_GPIOD10_57,
	BSP_GPIOD11_58,
	BSP_GPIOD12_59,
	BSP_GPIOD15_62,
	BSP_GPIOE0_97,
	BSP_GPIOE1_98,
	BSP_GPIOE2_1,
	BSP_GPIOE3_2,
	BSP_GPIOE4_3,
	BSP_GPIOE5_4,
	BSP_GPIOE6_5,
	BSP_GPIOE8_39,
	BSP_GPIOE9_40,
	BSP_GPIOE12_43,
	BSP_GPIOE13_44,
	BSP_GPIOE14_45,
	BSP_GPIOE15_46,
	BSP_GPIO_NONE
}bsp_gpio_name_t;

typedef void (*bsp_gpio_irq_cb)(bsp_gpio_name_t gpio_name);

int bsp_gpio_irq_register(bsp_gpio_name_t gpio_name,bsp_gpio_irq_cb irq_cb);
int bsp_gpio_init(bsp_gpio_name_t gpio_name,uint32_t gpio_mode,uint32_t gpio_pull,uint32_t gpio_speed);
int bsp_gpio_deinit(bsp_gpio_name_t gpio_name);
int bsp_gpio_write(bsp_gpio_name_t gpio_name, GPIO_PinState PinState);
GPIO_PinState bsp_gpio_read(bsp_gpio_name_t gpio_name);
#ifdef __cplusplus
}
#endif
#endif /*__ pinoutConfig_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
