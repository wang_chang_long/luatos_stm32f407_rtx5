#include "bsp_gpio.h"

typedef struct
{
	bsp_gpio_name_t gpio_name;
	GPIO_TypeDef *gpio_port;
	uint16_t gpio_pin;
	IRQn_Type irq;
	uint8_t irq_flag;
	bsp_gpio_irq_cb irq_cb;
}bsp_gpio_list_t;

bsp_gpio_list_t bsp_gpio_list[BSP_GPIO_NONE] =
{
	{BSP_GPIOA4_29	,GPIOA,GPIO_PIN_4		,EXTI4_IRQn			,0,NULL},
	{BSP_GPIOA5_30	,GPIOA,GPIO_PIN_5		,EXTI9_5_IRQn		,0,NULL},
	{BSP_GPIOA10_69	,GPIOA,GPIO_PIN_10	,EXTI15_10_IRQn	,0,NULL},
	{BSP_GPIOA11_70	,GPIOA,GPIO_PIN_11	,EXTI15_10_IRQn	,0,NULL},
	{BSP_GPIOA15_77	,GPIOA,GPIO_PIN_15	,EXTI15_10_IRQn	,0,NULL},
	{BSP_GPIOB3_89	,GPIOB,GPIO_PIN_3		,EXTI3_IRQn			,0,NULL},
	{BSP_GPIOB4_90	,GPIOB,GPIO_PIN_4		,EXTI4_IRQn			,0,NULL},
	{BSP_GPIOB5_91	,GPIOB,GPIO_PIN_5		,EXTI9_5_IRQn		,0,NULL},
	{BSP_GPIOB8_95	,GPIOB,GPIO_PIN_8		,EXTI9_5_IRQn		,0,NULL},
	{BSP_GPIOB9_96	,GPIOB,GPIO_PIN_9		,EXTI9_5_IRQn		,0,NULL},
	{BSP_GPIOD0_81	,GPIOD,GPIO_PIN_0		,EXTI0_IRQn			,0,NULL},
	{BSP_GPIOD1_82	,GPIOD,GPIO_PIN_1		,EXTI1_IRQn			,0,NULL},
	{BSP_GPIOD3_84	,GPIOD,GPIO_PIN_3		,EXTI3_IRQn			,0,NULL},
	{BSP_GPIOD4_85	,GPIOD,GPIO_PIN_4		,EXTI4_IRQn			,0,NULL},
	{BSP_GPIOD7_88	,GPIOD,GPIO_PIN_7		,EXTI9_5_IRQn		,0,NULL},
	{BSP_GPIOD10_57	,GPIOD,GPIO_PIN_10	,EXTI15_10_IRQn	,0,NULL},
	{BSP_GPIOD11_58	,GPIOD,GPIO_PIN_11	,EXTI15_10_IRQn	,0,NULL},
	{BSP_GPIOD12_59	,GPIOD,GPIO_PIN_12	,EXTI15_10_IRQn	,0,NULL},
	{BSP_GPIOD15_62	,GPIOD,GPIO_PIN_15	,EXTI15_10_IRQn	,0,NULL},
	{BSP_GPIOE0_97	,GPIOE,GPIO_PIN_0		,EXTI0_IRQn			,0,NULL},
	{BSP_GPIOE1_98	,GPIOE,GPIO_PIN_1		,EXTI1_IRQn			,0,NULL},
	{BSP_GPIOE2_1		,GPIOE,GPIO_PIN_2		,EXTI2_IRQn			,0,NULL},
	{BSP_GPIOE3_2		,GPIOE,GPIO_PIN_3		,EXTI3_IRQn			,0,NULL},
	{BSP_GPIOE4_3		,GPIOE,GPIO_PIN_4		,EXTI4_IRQn			,0,NULL},
	{BSP_GPIOE5_4		,GPIOE,GPIO_PIN_5		,EXTI9_5_IRQn		,0,NULL},
	{BSP_GPIOE6_5		,GPIOE,GPIO_PIN_6		,EXTI9_5_IRQn		,0,NULL},
	{BSP_GPIOE8_39	,GPIOE,GPIO_PIN_8		,EXTI9_5_IRQn		,0,NULL},
	{BSP_GPIOE9_40	,GPIOE,GPIO_PIN_9		,EXTI9_5_IRQn		,0,NULL},
	{BSP_GPIOE12_43	,GPIOE,GPIO_PIN_12	,EXTI15_10_IRQn	,0,NULL},
	{BSP_GPIOE13_44	,GPIOE,GPIO_PIN_13	,EXTI15_10_IRQn	,0,NULL},
	{BSP_GPIOE14_45	,GPIOE,GPIO_PIN_14	,EXTI15_10_IRQn	,0,NULL},
	{BSP_GPIOE15_46	,GPIOE,GPIO_PIN_15	,EXTI15_10_IRQn	,0,NULL},
};

int bsp_gpio_irq_register(bsp_gpio_name_t gpio_name,bsp_gpio_irq_cb irq_cb)
{
	if(gpio_name >= BSP_GPIO_NONE)
	{
		return -1;
	}
	bsp_gpio_list[gpio_name].irq_cb = irq_cb;
	return 0;
}

void bsp_gpio_clock_enable(GPIO_TypeDef *gpio_port)
{
	if(gpio_port == GPIOA)
	{
		__HAL_RCC_GPIOA_CLK_ENABLE();
	}
	else if(gpio_port == GPIOB)
	{
		__HAL_RCC_GPIOB_CLK_ENABLE();
	}
	else if(gpio_port == GPIOC)
	{
		__HAL_RCC_GPIOC_CLK_ENABLE();
	}
	else if(gpio_port == GPIOD)
	{
		__HAL_RCC_GPIOD_CLK_ENABLE();
	}
	else if(gpio_port == GPIOE)
	{
		__HAL_RCC_GPIOE_CLK_ENABLE();
	}
	else if(gpio_port == GPIOH)
	{
		__HAL_RCC_GPIOH_CLK_ENABLE();
	}
}


int bsp_gpio_init(bsp_gpio_name_t gpio_name,uint32_t gpio_mode,uint32_t gpio_pull,uint32_t gpio_speed)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(gpio_name >= BSP_GPIO_NONE)
	{
		return -1;
	}
	bsp_gpio_clock_enable(bsp_gpio_list[gpio_name].gpio_port);//GPIOʱ��ʹ��
	GPIO_InitStruct.Mode = gpio_mode;
	GPIO_InitStruct.Pin = bsp_gpio_list[gpio_name].gpio_pin;
	GPIO_InitStruct.Pull = gpio_pull;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(bsp_gpio_list[gpio_name].gpio_port, &GPIO_InitStruct);
	
	if(gpio_mode == GPIO_MODE_IT_RISING || gpio_mode == GPIO_MODE_IT_FALLING || gpio_mode == GPIO_MODE_IT_RISING_FALLING)
	{
		bsp_gpio_list[gpio_name].irq_flag = 1;
		HAL_NVIC_SetPriority(bsp_gpio_list[gpio_name].irq, 0, 0);
		HAL_NVIC_EnableIRQ(bsp_gpio_list[gpio_name].irq);
	}
	return 0;
}

int bsp_gpio_deinit(bsp_gpio_name_t gpio_name)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(gpio_name >= BSP_GPIO_NONE)
	{
		return -1;
	}
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pin = bsp_gpio_list[gpio_name].gpio_pin;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(bsp_gpio_list[gpio_name].gpio_port, &GPIO_InitStruct);
	if(bsp_gpio_list[gpio_name].irq_flag == 1)
	{
		bsp_gpio_list[gpio_name].irq_flag = 0;
		HAL_NVIC_DisableIRQ(bsp_gpio_list[gpio_name].irq);
	}
	return 0;
}

int bsp_gpio_write(bsp_gpio_name_t gpio_name, GPIO_PinState PinState)
{
	if(gpio_name >= BSP_GPIO_NONE)
	{
		return -1;
	}
	HAL_GPIO_WritePin(bsp_gpio_list[gpio_name].gpio_port, bsp_gpio_list[gpio_name].gpio_pin, PinState);
	return 0;
}

GPIO_PinState bsp_gpio_read(bsp_gpio_name_t gpio_name)
{
	if(gpio_name >= BSP_GPIO_NONE)
	{
		return GPIO_PIN_RESET;
	}
	return HAL_GPIO_ReadPin(bsp_gpio_list[gpio_name].gpio_port, bsp_gpio_list[gpio_name].gpio_pin);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_pin)
{
	uint16_t i = 0;
	for(i=0;i<BSP_GPIO_NONE;i++)
	{
		if(bsp_gpio_list[i].gpio_pin == GPIO_pin && bsp_gpio_list[i].irq_flag == 1)
		{
			break;
		}
	}
	if(bsp_gpio_list[i].irq_cb != NULL)
	{
		bsp_gpio_list[i].irq_cb((bsp_gpio_name_t)i);
	}
}

