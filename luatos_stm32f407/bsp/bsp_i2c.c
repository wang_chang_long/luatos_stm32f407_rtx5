#include "common.h"
#include "bsp_i2c.h"
#include "bsp_gpio.h"

typedef struct
{
	bsp_gpio_name_t gpio_scl_name;
	bsp_gpio_name_t gpio_sda_name;
}bsp_i2c_list_t;

const bsp_i2c_list_t bsp_i2c_list[] =
{
	{BSP_GPIOA4_29,BSP_GPIOA5_30},
	{BSP_GPIOB8_95,BSP_GPIOB9_96},
	{BSP_GPIOD0_81,BSP_GPIOD1_82}
};

/**
  * @brief  SDA管脚设置为输出.
	* @note   
  * @param  None
  * @retval None
  */
static void bsp_i2c_sda_out(bsp_i2c_name_t i2c_name)
{
  bsp_gpio_init(bsp_i2c_list[i2c_name].gpio_sda_name,GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_FREQ_HIGH);
}

/**
  * @brief  SDA管脚设置为输入.
	* @note   
  * @param  None
  * @retval None
  */
static void bsp_i2c_sda_in(bsp_i2c_name_t i2c_name)
{
  bsp_gpio_init(bsp_i2c_list[i2c_name].gpio_sda_name,GPIO_MODE_INPUT,GPIO_NOPULL,GPIO_SPEED_FREQ_HIGH);
}

/**
  * @brief  SCL管脚设置为输出.
	* @note   
  * @param  None
  * @retval None
  */
static void bsp_i2c_scl_out(bsp_i2c_name_t i2c_name)
{
	bsp_gpio_init(bsp_i2c_list[i2c_name].gpio_scl_name,GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_FREQ_HIGH);
}
/**
  * @brief  i2c初始化
	* @note   
  * @param  None
  * @retval None
  */
void bsp_i2c_init(bsp_i2c_name_t i2c_name)
{
	bsp_i2c_scl_out(i2c_name);
	bsp_i2c_sda_out(i2c_name);
}

/**
  * @brief  i2c模拟总线发出start信号.
	* @note   
  * @param  None
  * @retval None
  */
void bsp_i2c_start(bsp_i2c_name_t i2c_name)
{
	bsp_i2c_sda_out(i2c_name);     //sda线输出
	IIC_SDA_H(bsp_i2c_list[i2c_name].gpio_sda_name);//IIC_SDA=1;	  	  
	IIC_SCL_H(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=1;
	com_delay_us(4);
 	IIC_SDA_L(bsp_i2c_list[i2c_name].gpio_sda_name);//IIC_SDA=0;//START:when CLK is high,DATA change form high to low 
	com_delay_us(4);
	IIC_SCL_L(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=0;//钳住I2C总线，准备发送或接收数据 
}	 

/**
  * @brief  i2c模拟总线发出stop信号.
	* @note   
  * @param  None
  * @retval None
  */
void bsp_i2c_stop(bsp_i2c_name_t i2c_name)
{
	bsp_i2c_sda_out(i2c_name);//sda线输出
	IIC_SCL_L(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=0;
	IIC_SDA_L(bsp_i2c_list[i2c_name].gpio_sda_name);//IIC_SDA=0;//STOP:when CLK is high DATA change form low to high
	com_delay_us(4);
	IIC_SCL_H(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=1; 
	IIC_SDA_H(bsp_i2c_list[i2c_name].gpio_sda_name);//IIC_SDA=1;//发送I2C总线结束信号
	com_delay_us(4);	
}

/**
  * @brief  i2c模拟总线等待ack信号.
	* @note   
  * @param  None
  * @retval None
  */
int bsp_i2c_wait_ack(bsp_i2c_name_t i2c_name)
{
	uint32_t ucErrTime=0;
	bsp_i2c_sda_in(i2c_name);      //SDA设置为输入  
	IIC_SDA_H(bsp_i2c_list[i2c_name].gpio_sda_name);//IIC_SDA=1; 
	com_delay_us(1);  
	IIC_SCL_H(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=1;
	com_delay_us(1);
	while(IIC_SDA_IN(bsp_i2c_list[i2c_name].gpio_sda_name) == GPIO_PIN_SET)
	{
		ucErrTime++;
		if(ucErrTime>0xffff)
		{
			bsp_i2c_stop(i2c_name);
			return -1;
		}
	}
	IIC_SCL_L(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=0;//时钟输出0 	   
	return 0;  
} 

/**
  * @brief  i2c模拟总线产生ack信号.
	* @note   
  * @param  None
  * @retval None
  */
void bsp_i2c_ack(bsp_i2c_name_t i2c_name)
{
	IIC_SCL_L(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=0;
	bsp_i2c_sda_out(i2c_name);
	IIC_SDA_L(bsp_i2c_list[i2c_name].gpio_sda_name);//IIC_SDA=0;
	com_delay_us(2);
	IIC_SCL_H(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=1;
	IIC_SCL_L(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=0;
}

/**
  * @brief  i2c模拟总线不产生ack应答.
	* @note   
  * @param  None
  * @retval None
  */	    
void bsp_i2c_noack(bsp_i2c_name_t i2c_name)
{
	IIC_SCL_L(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=0;
	bsp_i2c_sda_out(i2c_name);
	IIC_SDA_H(bsp_i2c_list[i2c_name].gpio_sda_name);//IIC_SDA=1;
	com_delay_us(2);
	IIC_SCL_H(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=1;
	IIC_SCL_L(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=0;
	com_delay_us(2);
}					 				     
	
/**
	* @brief  i2c模拟总线写一个字节.
	* @note   
  * @param  None
  * @retval None
  */	
void bsp_i2c_send_byte(bsp_i2c_name_t i2c_name,uint8_t txd)
{                        
    uint8_t t;   
    bsp_i2c_sda_out(i2c_name); 	    
    IIC_SCL_L(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=0;//拉低时钟开始数据传输
    for(t=0;t<8;t++)
    {              
        //IIC_SDA=(txd&0x80)>>7;
		if((txd&0x80)>>7)
			IIC_SDA_H(bsp_i2c_list[i2c_name].gpio_sda_name);//IIC_SDA=1;
		else
			IIC_SDA_L(bsp_i2c_list[i2c_name].gpio_sda_name);//IIC_SDA=0;
		txd<<=1; 	  
		IIC_SCL_H(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=1;
		com_delay_us(2);
		IIC_SCL_L(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=0;
		com_delay_us(2);		
    }	 
} 	    

/**
	* @brief  i2c模拟总线读一个字节.
	* @note   
  * @param  None
  * @retval None
  */
uint8_t bsp_i2c_read_byte(bsp_i2c_name_t i2c_name,uint8_t ack)
{
    uint8_t i,receive=0;
    bsp_i2c_sda_in(i2c_name);//SDA设置为输入
    for(i=0;i<8;i++ )
    {
      IIC_SCL_L(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=0; 
			com_delay_us(2);
			IIC_SCL_H(bsp_i2c_list[i2c_name].gpio_scl_name);//IIC_SCL=1;
      receive<<=1;
      if(IIC_SDA_IN(bsp_i2c_list[i2c_name].gpio_sda_name) == GPIO_PIN_SET)receive++;
			com_delay_us(1);			
    }					 
    if (!ack)
      bsp_i2c_noack(i2c_name);//发送nACK
    else
      bsp_i2c_ack(i2c_name); //发送ACK   
    return receive;
}

