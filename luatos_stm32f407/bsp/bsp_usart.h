/**
  ******************************************************************************
  * File Name          : USART.h
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_USART_H
#define __BSP_USART_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

typedef enum
{
//	BSP_UART0,
	BSP_UART1 = 0,
	BSP_UART2,
	BSP_UART3,
//	BSP_UART4,
//	BSP_UART5,
	BSP_UART6,
	BSP_UART_END,
}bsp_uart_name_t;


typedef void (*uart_cb_t)(bsp_uart_name_t uart_name,uint8_t dat);

typedef struct
{
	USART_TypeDef *uart_port;
	UART_HandleTypeDef huart;
	uart_cb_t uart_cb; 
	uint8_t dat;
}bsp_uart_list_t;

int bsp_uart_recv_register(bsp_uart_name_t uart_name,uart_cb_t uart_recv_cb);
int bsp_uart_init(bsp_uart_name_t uart_name,uint32_t bau, uint8_t even, uint8_t dat_len, uint8_t stop_bits);
int bsp_uart_send_dat(bsp_uart_name_t uart_name,uint8_t *dat,uint16_t len);
int bsp_uart_deinit(bsp_uart_name_t uart_name);
#ifdef __cplusplus
}
#endif
#endif /*__ usart_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
