#ifndef __BSP_I2C_H
#define __BSP_I2C_H

#include "stdint.h"

typedef enum{
	BSP_INDEX_IIC0 = 0,
	BSP_INDEX_IIC1,
	BSP_INDEX_IIC2
}bsp_i2c_name_t;

#define IIC_SCL_H(gpio_name)	bsp_gpio_write(gpio_name,GPIO_PIN_SET)
#define IIC_SCL_L(gpio_name)	bsp_gpio_write(gpio_name,GPIO_PIN_RESET)
#define IIC_SDA_H(gpio_name)  bsp_gpio_write(gpio_name,GPIO_PIN_SET)
#define IIC_SDA_L(gpio_name)  bsp_gpio_write(gpio_name,GPIO_PIN_RESET)
#define IIC_SDA_IN(gpio_name) bsp_gpio_read(gpio_name)

void bsp_i2c_init(bsp_i2c_name_t i2c_name);
void bsp_i2c_start(bsp_i2c_name_t i2c_name);
void bsp_i2c_stop(bsp_i2c_name_t i2c_name);
int bsp_i2c_wait_ack(bsp_i2c_name_t i2c_name);
void bsp_i2c_ack(bsp_i2c_name_t i2c_name);
void bsp_i2c_noack(bsp_i2c_name_t i2c_name);
void bsp_i2c_send_byte(bsp_i2c_name_t i2c_name,uint8_t txd);
uint8_t bsp_i2c_read_byte(bsp_i2c_name_t i2c_name,uint8_t ack);


#endif
