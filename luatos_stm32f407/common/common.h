#ifndef __COMMON_H
#define __COMMON_H

#include <stdint.h>
#include "time.h"
#define USER_RTOS

#define COM_SET_BIT(val,xbit)   do{val |= (1<<xbit);}while(0)
#define COM_CLEAR_BIT(val,xbit) do{val &= ~(1<<xbit);}while(0)

typedef enum
{
	TIME_TYPE_YEAR = 0,
	TIME_TYPE_MON,
	TIME_TYPE_DAY,
	TIME_TYPE_HOUR,
	TIME_TYPE_MIN,
	TIME_TYPE_SEC,
}time_type_t;

void com_delay_us(uint16_t xus);
void com_delay_ms(unsigned short xms);
uint8_t com_bin2bcd(uint8_t value);
uint8_t com_bcd2bin(uint8_t value);
char com_chars_cmp(uint8_t *srcchars,uint8_t *dstchars,uint8_t numb);
uint8_t com_asc2hex(uint8_t achar);
uint8_t com_hex2asc(uint8_t ahex);
uint8_t com_hex2str(char* inbuff,char* outbuff,uint8_t len);
uint8_t com_str2hex(char* inbuff,char* outbuff,uint8_t len);
uint16_t com_crc16_check(uint8_t* msg, uint16_t len);
uint16_t com_crc8_check(uint8_t* msg, uint16_t len);
uint32_t com_buf4_to_uint32(uint8_t *buf);
void com_uint32_to_buf4(uint8_t *buf,uint32_t val);
uint32_t com_diff_time(uint32_t start, uint32_t end);
void com_hex2bcd_multiple(int64_t int_val,uint8_t *pout_dat,uint8_t len,uint8_t flag);
uint32_t com_pow10(uint8_t y);
float com_pow10y(unsigned char flg,unsigned char y);
uint32_t com_bcd2hex_big(uint8_t *datbuf,uint8_t size);
uint32_t com_bcd2hex_little(uint8_t *datbuf,uint8_t size);
unsigned char com_pub_float2bcd(float value, int lint, int lfloat, unsigned char *pbuff);
char *com_findstr(char *Str_Local,char *Compare_Local,uint8_t Num);
uint32_t com_get_str(char *pdata,uint8_t max_len);
uint16_t com_big_little_change(uint16_t val);
unsigned char com_and_check(unsigned char *msg,unsigned char lenth);
void com_tim_add(struct tm *time,uint8_t val,time_type_t time_type);
unsigned int com_pub_time2sec(struct tm *time);
uint16_t com_calc_day(struct tm *start_tim,struct tm *end_tim);
int com_tim_format(struct tm *time,uint32_t year,uint32_t mon,uint32_t day,uint32_t hour,uint32_t min,uint32_t sec);
uint32_t com_calc_time_sec(struct tm start_tim,struct tm end_tim);

#ifdef USER_RTOS
void thread_switch_disable(void);
void thread_switch_enable(void);
uint8_t osflag_check(uint32_t status);
#endif

#endif
