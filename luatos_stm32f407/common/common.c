#include <stdint.h>
#include <string.h>
#include <math.h>
#include "common.h"
#include "time.h"
#include "stm32f4xx_hal.h"
#ifdef USER_RTOS
#include "cmsis_os2.h"
#endif

/**
  * @brief  us级延时.基于168Mhz时钟
  * @note    
  * @param  xus:us数
  * @retval None
  */
void com_delay_us_clock168m(uint16_t xus)
{
	uint16_t i,j;
	for(i=xus;i>0;i--)
		 for(j=28;j>0;j--)
				__asm("NOP");
}

/**
  * @brief  us级延时.基于72Mhz时钟
  * @note    
  * @param  xus:us数
  * @retval None
  */
void com_delay_us_clock72m(uint16_t xus)
{
	uint16_t i,j;
	for(i=xus;i>0;i--)
		 for(j=11;j>0;j--)
				__asm("NOP");
}

/**
  * @brief  us级延时.基于48Mhz时钟
  * @note    
  * @param  xus:us数
  * @retval None
  */
void com_delay_us_clock48m(uint16_t xus)
{
	uint16_t i,j;
	for(i=xus;i>0;i--)
		 for(j=7;j>0;j--)
				__asm("NOP");
}
/**
  * @brief  us级延时.
  * @note    
  * @param  xus:16进制
  * @retval None
  */
void com_delay_us(uint16_t xus)
{
	com_delay_us_clock168m(xus);
}

/**
  * @brief  us级延时.
  * @note    
  * @param  xus:16进制
  * @retval None
  */
void com_delay_ms(uint16_t xms)
{
	uint16_t i;
	for(i=xms;i>0;i--)
	{
		com_delay_us(1000);
	}
}
/**
  * @brief  十六进制与BCD码转换.
  * @note   十进制54321，用BCD码表示就是0x54321，十六进制表示0xD431
  * @param  value:16进制
  * @retval BCD码
  */
uint8_t com_bin2bcd(uint8_t value) 
{
	return (((value / 10U) << 4U) | (value % 10U));
}
/**
  * @brief  十六进制与BCD码转换.
  * @note    
  * @param  value:BCD码
  * @retval 16进制
  */
uint8_t com_bcd2bin(uint8_t value) 
{
	return ((((value & 0xF0U) >> 0x4U) * 10U ) + (value & 0x0FU));
}
/**
  * @brief  多位十进制转换成BCD.
  * @note    
	* @param  hex_val：64位数据，
	*					pout_dat：BCD码缓存指针
	*					len:BCD长度
	*					flag:0:小端，1：大端
  * @retval 16进制
  */
void com_hex2bcd_multiple(int64_t int_val,uint8_t *pout_dat,uint8_t len,uint8_t flag)
{
	uint32_t pow_val1 = 0;
	uint32_t pow_val2 = 0;
	uint32_t pow_val3 = 0;
	uint64_t hex_val =int_val;
	if(int_val < 0)
	{
		hex_val = (~hex_val)+1;
	}
	else
	{
		hex_val = int_val;
	}
	for(uint8_t i=0;i<len;i++)
	{
		if(flag)
		{
			pow_val1 = pow(10.0,(len-i)*2);
			pow_val2 = pow(10.0,(len-i)*2-1);
			pow_val3 = pow(10.0,(len-i)*2-2);
			pout_dat[i] = (hex_val % pow_val1 / pow_val2)<<4;
			pout_dat[i] |= hex_val % pow_val2 / pow_val3;
		}	
		else
		{
			pow_val1 = pow(10.0,(i+1)*2);
			pow_val2 = pow(10.0,(i+1)*2-1);
			pow_val3 = pow(10.0,(i+1)*2-2);
			pout_dat[i] = (hex_val % pow_val1 / pow_val2)<<4;
			pout_dat[i] |= hex_val % pow_val2 / pow_val3;
		} 
	}
}
/**
  * @brief  比较2个字符串指定长度的内容是否一致.
  * @note    
	* @param  srcchars:源字符串 
	*					dstchars:目标字符串 
	*					numb:比较字符个数
  * @retval 1内容一致 0内容不一致
  */
char com_chars_cmp(uint8_t *srcchars,uint8_t *dstchars,uint8_t numb) 
{
    uint8_t *p,*q;uint8_t i;
    p = srcchars;
    q = dstchars;
    for(i=0;i<numb;i++)
    {
        if(*p!=*q)
        {
            return 0;
        }
        p++;
        q++;
    }
    return 1;
}

/**
  * @brief  十六进制与ASCII转换.
  * @note    
	* @param  achar:ASCII码 
  * @retval 16进制
  */
uint8_t com_asc2hex(uint8_t achar)
{
    if((achar>=0x30)&&(achar<=0x39))
        achar -= 0x30;
    else if((achar>=0x41)&&(achar<=0x46))
        achar -= 0x37;
    else if((achar>=0x61)&&(achar<=0x66))
        achar -= 0x57;
    else achar = 0xff;
    return achar; 
} 
/**
  * @brief  十六进制与ASCII转换.
  * @note    
	* @param  achar:16进制
  * @retval ASCII码 
  */
uint8_t com_hex2asc(uint8_t ahex)
{
    if(ahex<=9)
        ahex += 0x30;
    else if((ahex>=10)&&(ahex<=15))//A-F
        ahex += 0x37;
    else ahex = 0xff;
    return ahex;
}

/**
  * @brief  十六进制与字符串转换.
  * @note    
	* @param  inbuff:16进制 
	*					outbuff:字符串 
	*					len:16进制长度
  * @retval 16进制长度
  */
uint8_t com_hex2str(char* inbuff,char* outbuff,uint8_t len)
{
	uint8_t i=0,j=0;
	for(i=0;i<len;i++)
	{
		outbuff[j++]=com_hex2asc(inbuff[i]>>4);
		outbuff[j++]=com_hex2asc(inbuff[i]&0x0F);
	}
	return i;
}
/**
  * @brief  十六进制与字符串转换.
  * @note    
	* @param  inbuff:字符串  
	*					outbuff:16进制
	*					len:字符串长度
  * @retval 字符串长度
  */
uint8_t com_str2hex(char* inbuff,char* outbuff,uint8_t len)
{
	uint8_t i=0,j=0;
	for(i=0;i<len;i++)
	{
		outbuff[i] = (com_asc2hex(inbuff[j++])<<4);
		outbuff[i] |= com_asc2hex(inbuff[j++]);
	}
	return i;
}

/**
  * @brief  modubus crc16查表校验算法.
  * @note    
	* @param  msg:需要校验的序列  
	*					len:序列长度
  * @retval 校验值
  */
const uint16_t wCRCTalbeAbs[] ={0x0000, 0xCC01, 0xD801, 0x1400, 0xF001, 0x3C00, 0x2800, 0xE401, 0xA001, 0x6C00, 0x7800, 0xB401, 0x5000, 0x9C01, 0x8801, 0x4400,};
uint16_t com_crc16_check(uint8_t* msg, uint16_t len)
{
	uint16_t  wCRC = 0xFFFF;
	uint16_t  i;
	uint8_t chChar;
	for (i = 0; i < len; i++)
	{
		chChar = *msg++;
		wCRC = wCRCTalbeAbs[(chChar ^ wCRC) & 15] ^ (wCRC >> 4);
		wCRC = wCRCTalbeAbs[((chChar >> 4) ^ wCRC) & 15] ^ (wCRC >> 4);
	}
	return wCRC;
}
/**
  * @brief  crc8校验算法.
  * @note    
	* @param  msg:需要校验的序列  
	*					len:序列长度
  * @retval 校验值
  */
uint16_t com_crc8_check(uint8_t* msg, uint16_t len)
{
	uint8_t ui_crc8;
	int i, j;
	ui_crc8 = 0x00; //init
	for(i = 0; i < len; i++)
	{
		 
			ui_crc8 = (uint8_t)(msg[i] ^ ui_crc8);

			for(j = 0; j < 8; j++)
			{
					if((ui_crc8 & 0x80) == 0x80)
					{
							ui_crc8 = (uint8_t)(ui_crc8 << 1);
							ui_crc8 = (uint8_t)(ui_crc8 ^ 0xe5);
					}
					else
					{
							ui_crc8 = (uint8_t)(ui_crc8 << 1);
					}
			}
	}
	return ui_crc8 ;
}
/**
  * @brief  4字节十六进制数组转换为4字节十六进制数.
  * @note   {0x37,0x00,0x12,0x03}=0x37001203
	* @param  buf:十六进制数组 
  * @retval 十六进制数
  */
uint32_t com_buf4_to_uint32(uint8_t *buf)
{
    uint32_t val;
    
    val=(uint32_t)((buf[0]<<24)|(buf[1]<<16)|(buf[2]<<8)|buf[3]);
    return val;
}

/**
  * @brief  4字节十六进制数转换为4字节十六进制数组.
  * @note   0x37001203={0x37,0x00,0x12,0x03}
	* @param  buf:4字节十六进制数组 
	*					val:4字节十六进制数
  * @retval None
  */
void com_uint32_to_buf4(uint8_t *buf,uint32_t val)
{
    buf[0]=(uint8_t)(val>>24);
    buf[1]=(uint8_t)(val>>16);
    buf[2]=(uint8_t)(val>>8);
    buf[3]=(uint8_t)val;
}


/**
  * @brief  计算数值差.
	* @note   
  * @param  start起始时间   end截止时间
  * @retval dwValue返回的数值差
  */
uint32_t com_diff_time(uint32_t start, uint32_t end)
{
	uint32_t dwValue;

	if (end >= start)
	{
		dwValue = end - start;
	}
	else 
	{
		dwValue = (0xFFFFFFFF - start) + end ;
	}

	return dwValue;
}

/**
  * @brief  计算10的y次幂（10^y）,0<=y<=8
	* @note   
  * @param  y
  * @retval 计算结果
  */
uint32_t com_pow10(uint8_t y)
{
    uint8_t i;
    uint32_t wresult = 1;
    for(i=0;i<y;i++)
    {
        wresult = (wresult*10);
    }
    return wresult;
}
/**
  * @brief  计算10的y次幂（10^y）,flag:0,0<=y<=6 1,0<=y<=9
	* @note   
  * @param  flg-指数的符号位,1为正 0为负 y-指数
  * @retval 计算结果
  */
float com_pow10y(unsigned char flg,unsigned char y)
{
	float wresult=1.0;
	unsigned char  i;
	
	for(i=0;i<y;i++)
	{
		if(flg==1)//正数  乘
		{
			wresult = (wresult*10);
		}
		else if(flg==0)
		{
			wresult = (wresult/10);
		}
	}
	return wresult;
}

/**
  * @brief  多位BCD码转换为4字节十六进制
	* @note   
  * @param  datbuf-BCD码高位在前(大端模式)   size-半字节数
  * @retval 乘数计算结果
  */
uint32_t com_bcd2hex_big(uint8_t *datbuf,uint8_t size)
{
	uint32_t ddat1, ddat = 0;
	uint8_t *p;
	uint8_t cdat,coff = 0;
	coff = size - 1;
	p = datbuf;
	while(size)
	{
		size--;
		cdat  = *p;
		ddat1 = (uint32_t )(cdat>>4)&0x0000000f;

		ddat += ddat1*com_pow10(coff--);
		if(size)
		{
				cdat  = (*p)&0x0f;
				ddat1 = (uint32_t )cdat&0x0000000f;
				ddat += ddat1*com_pow10(coff--);
				size--;
		}
		p++;
	}
	return ddat;
}
/**
  * @brief  多位BCD码转换为4字节十六进制
	* @note   
  * @param  datbuf-BCD码高位在前(小端模式)   size-半字节数
  * @retval 乘数计算结果
  */
uint32_t com_bcd2hex_little(uint8_t *datbuf,uint8_t size)
{
	uint32_t ddat1, ddat = 0;
	uint8_t *p;
	uint8_t cdat,coff = 0;
	p = datbuf;
	while(size)
	{
		size--;
		cdat  = *p;
		ddat1 = (uint32_t )cdat&0x0000000f;
		ddat += ddat1*com_pow10(coff++);
		if(size)
		{
				cdat  = (*p)>>4;
				ddat1 = (uint32_t )cdat&0x0000000f;
				ddat += ddat1*com_pow10(coff++);
				size--;
		}
		p++;
	}
	return ddat;
}



unsigned char com_float2bcd(float value, int lint, int lfloat, unsigned char *pbuff)
{
	int lint_test = 0;
	int lfloat_test = 0;
	unsigned char uRe = 0;
	int8_t test_val =0;
	lint_test = (lint + 1)>>1;
	lfloat_test = lfloat;
	memset(pbuff, 0, lint_test);
	uRe = lint_test << 3 | lfloat_test;
	
	if (pbuff && (lint_test > 0) && (lfloat_test >= 0))
	{
		int i=0,nValue = 0;
		char bNegative = 0;

		for (i=0; i<lfloat_test; i++)//放大，去除小数点
		{
			value *= 10;
		}
		
		nValue = (int)value;/* float型数据只保存7位有效数字，value数值太大之后，程序会一直停在for中 */
		/*
		for(ftmp=0;ftmp<value;ftmp++){  
			nValue ++;
		}
		*/
		if(value < 0){
			nValue = -nValue;
		}
		if (nValue < 0)
		{
			bNegative = 1;
			nValue = -nValue;
		}

		i=0;
		while(nValue)
		{
			test_val = lint_test - ((i)>>1) - 1;
			lint--;
			if(lint < 0)
			{
				break;
			}
			else
			{
				pbuff[test_val] += (nValue%10)<<((i%2 != 0)?4:0);
				i++;
				nValue /= 10;
			}
		}
		if (bNegative)
		{
			pbuff[0] = 0xFF;
		}
	}
	return uRe;
}

unsigned int com_time2sec(struct tm *time)
{
	struct tm rtc;
	time_t sec[2] = {0};
	
	rtc = *time;
	rtc.tm_year = 100;
	mktime(&rtc);
	sec[0] = mktime(&rtc);
	
	rtc.tm_year = 100;
	rtc.tm_mon = 0;
	rtc.tm_mday = 1;
	rtc.tm_hour = 0;
	rtc.tm_min = 0;
	rtc.tm_sec = 0;
	mktime(&rtc);
	sec[1] = mktime(&rtc);
	return (sec[0] - sec[1]);
}

/**
	* @brief  计算开始时间和结束时间之间的天数
  * @note   计算天数，最大一年时间
	* @param  start_time：开始时间
	*					end_time：结束时间
  * @retval 
  */
uint16_t com_calc_day(struct tm *start_tim,struct tm *end_tim)
{
	uint16_t sumday = 0;
	struct tm last_tm = {0};//一年中的最后一天
	mktime(start_tim);
	mktime(end_tim);
	if(start_tim->tm_year < end_tim->tm_year)//不是同一年
	{
		if(start_tim->tm_year + 1 < end_tim->tm_year)
		{
			return 366;
		}
		else
		{
			com_tim_format(&last_tm,start_tim->tm_year - 100,12,31,0,0,0);//年月日时分秒
			sumday = last_tm.tm_yday - start_tim->tm_yday + 1 + end_tim->tm_yday + 1;
			if(sumday > 366)
			{
				sumday = 366;
			}
			return sumday;
		}
	}
	else if(start_tim->tm_year == end_tim->tm_year)
	{
		if(start_tim->tm_yday > end_tim->tm_yday)
		{
			return 0;
		}
		else
		{
			return (end_tim->tm_yday - start_tim->tm_yday + 1);
		}
	}
	else
	{
		return 0;
	}
}

/**
	* @brief  计算开始时间和结束时间之间的秒数
  * @note   计算天数，最大一年时间
	* @param  start_time：开始时间
	*					end_time：结束时间
  * @retval 
  */
uint32_t com_calc_time_sec(struct tm start_tim,struct tm end_tim)
{
	//struct tm last_tm = {0};//一年中的最后一天
	uint32_t start_sec = 0;
	uint32_t end_sec = 0;
	mktime(&start_tim);
	mktime(&end_tim);
	start_sec = mktime(&start_tim);
	end_sec = mktime(&end_tim);
	if(end_sec > start_sec)
	{
		return end_sec - start_sec;
	}
	return 0;
}

/**
	* @brief  计算开始时间和结束时间之间的天数
  * @note   计算天数，最大一年时间
	* @param  start_time：开始时间
	*					end_time：结束时间
  * @retval 
  */
int com_tim_format(struct tm *time,uint32_t year,uint32_t mon,uint32_t day,uint32_t hour,uint32_t min,uint32_t sec)
{
	if(time == NULL || mon > 12 || day == 0 || day > 31 || hour > 23 || min > 59 || sec > 60)
	{
		return -1;
	}
	time->tm_year = 100 + year;
	time->tm_mon = mon - 1;
	time->tm_mday = day;
	time->tm_hour = hour;
	time->tm_min = min;
	time->tm_sec = sec;
	time->tm_isdst = -1;
	mktime(time);
	return 0;
}
/**
	* @brief  计算开始时间和结束时间之间的天数
  * @note   计算天数，最大一年时间
	* @param  start_time：开始时间
	*					end_time：结束时间
  * @retval 
  */
void com_tim_add(struct tm *time,uint8_t val,time_type_t time_type)
{
	switch(time_type)
	{
		case TIME_TYPE_YEAR:
			time->tm_year += val;
		break;
		case TIME_TYPE_MON:
			time->tm_mon += val;
		break;
		case TIME_TYPE_DAY:
			time->tm_mday += val;
		break;
		case TIME_TYPE_HOUR:
			time->tm_hour += val;
		break;
		case TIME_TYPE_MIN:
			time->tm_min += val;
		break;
		case TIME_TYPE_SEC:
			time->tm_sec += val;
		break;
	}
	mktime(time);//必须要执行此函数
}



/*************************************************
  * 函数功能: 在给定字符串中查找特定字符串第n次出现的位置
  * 输入参数: char *Str_Local   给定的字符串
						  char *Compare_Local 比较的字符串
							uint8_t Num 次数
  * 返 回 值: 待查找字符串的地址
  * 说    明：
  */
char *com_findstr(char *Str_Local,char *Compare_Local,uint8_t Num)
{
	if(Str_Local!=NULL)//判断接收字符串是否为所需要字符串
	{
		for(uint8_t i=0;i<Num;i++)
		{
			Str_Local=strstr(Str_Local,Compare_Local);
			if(Str_Local!=NULL)
			{
				Str_Local=Str_Local+strlen(Compare_Local);//跨过需要比较的字符串
			}
			else
			{
				return NULL;
			}
		}
		if(Str_Local!=NULL)
		{
			Str_Local=Str_Local-strlen(Compare_Local);
		}
	}
	return Str_Local;
}

uint32_t com_get_str(char *pdata,uint8_t max_len)
{
	uint32_t ret =0;
	while(pdata && max_len)
	{
		max_len--;
		if((*pdata>='0')&&(*pdata<='9'))
		{
			ret = ret*10 + (*pdata-'0');
			pdata++;
		}
		else
		{
			return ret;
		}
	}
	return ret;
}

uint16_t com_big_little_change(uint16_t val)
{
	uint16_t val_change = 0;
	uint8_t *pbuff = (uint8_t *)&val;
	val_change = (uint16_t)(pbuff[0]<<8) | pbuff[1];
	return val_change;
}

//和校验
unsigned char com_and_check(unsigned char *msg,unsigned char lenth)
{
    unsigned char value = 0;
    for(unsigned char i=0;i<lenth;i++)
    {
        value += msg[i];
    }
    return value;
}

#ifdef USER_RTOS

/* 禁止线程切换 */
void thread_switch_disable(void)
{
	if(osKernelGetState() == osKernelRunning)
		osKernelLock();
}
/* 使能线程切换 */
void thread_switch_enable(void)
{
	if(osKernelGetState() == osKernelLocked)
		osKernelUnlock();
}
/**
  * @brief  操作系统错误标志检测.
	* @note   
  * @param  status：标志
  * @retval 0：错误 1：正确
  */
uint8_t osflag_check(uint32_t status)
{
	if(status&0x80000000)
	{
		return 0;
	}
	return 1;
}



#endif
