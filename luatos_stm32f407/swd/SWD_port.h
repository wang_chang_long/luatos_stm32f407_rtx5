#ifndef _SWD_PORT_H
#define _SWD_PORT_H
#include "main.h"
#include "bsp_gpio.h"
#define TRST_O_NAME  	BSP_GPIOB3_89  
#define SWCLK_O_NAME 	BSP_GPIOB4_90
#define SWDIO_NAME		BSP_GPIOB5_91

//#define TRST_O_Pin LL_GPIO_PIN_3
//#define TRST_O_GPIO_Port GPIOB

//#define TCLK_O_Pin LL_GPIO_PIN_4
//#define TCLK_O_GPIO_Port GPIOB
////#define TMS_O_Pin LL_GPIO_PIN_3
////#define TMS_O_GPIO_Port GPIOA

//#define SWDIO_Pin LL_GPIO_PIN_5
//#define SWDIO_GPIO_Port GPIOB


#define PIN_SWCLK_SET bsp_gpio_write(SWCLK_O_NAME,GPIO_PIN_SET)//HAL_GPIO_WritePin(TCLK_O_GPIO_Port,TCLK_O_Pin,GPIO_PIN_SET)
#define PIN_SWCLK_CLR bsp_gpio_write(SWCLK_O_NAME,GPIO_PIN_RESET)

#define PIN_SWDIO_SET bsp_gpio_write(SWDIO_NAME,GPIO_PIN_SET)
#define PIN_SWDIO_CLR bsp_gpio_write(SWDIO_NAME,GPIO_PIN_RESET)
#define PIN_SWDIO_IN 	bsp_gpio_read(SWDIO_NAME)

#define PIN_SWNRST_SET bsp_gpio_write(TRST_O_NAME,GPIO_PIN_SET)
#define PIN_SWNRST_CLR bsp_gpio_write(TRST_O_NAME,GPIO_PIN_RESET)

struct _DAP_SWD_PORT {
	void (*swdClkClr)(void);
	void (*swdClkSet)(void);
	void (*swdIoClr)(void);
	void (*swdIoSet)(void);
	void (*swdnRstClr)(void);
	void (*swdnRstSet)(void);
};

void PORT_SWD_SETUP(void);
void PORT_OFF(void);

void SW_CLOCK_CYCLE(void);
void SW_WRITE_BIT(uint32_t bit);
void PIN_SWDIO_OUT(uint8_t bit);
uint8_t SW_READ_BIT(void);
void PIN_SWDIO_DIR_IN(void);
void PIN_SWDIO_DIR_OUT(void);
#endif
