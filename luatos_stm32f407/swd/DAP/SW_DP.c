/**
 * @file    SW_DP.c
 * @brief   SWD driver
 *
 * DAPLink Interface Firmware
 * Copyright (c) 2009-2016, ARM Limited, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ----------------------------------------------------------------------
 *
 * $Date:        20. May 2015
 * $Revision:    V1.10
 *
 * Project:      CMSIS-DAP Source
 * Title:        SW_DP.c CMSIS-DAP SW DP I/O
 *
 *---------------------------------------------------------------------------*/

#include "DAP_config.h"
#include "DAP.h"
#include "SWD_port.h"
// SW Macros

#if ((DAP_SWD != 0) || (DAP_JTAG != 0))
void SWJ_Sequence(uint32_t count, const uint8_t *data)
{
	uint32_t val;
	uint32_t n;

	val = 0U;
	n = 0U;

	while(count--)
	{
		if(n == 0U)
		{
				val = *data++;
				n = 8U;
		}

		if(val & 1U)
		{
				PIN_SWDIO_SET;
		}
		else
		{
				PIN_SWDIO_CLR;
		}

		SW_CLOCK_CYCLE();
		val >>= 1;
		n--;
	}
}
#endif


#if (DAP_SWD != 0)
uint32_t ack;  
uint8_t SWD_TransferSlow (uint32_t request, uint32_t *data) {                
                                                                 
  uint32_t bit;                                                                 
  uint32_t val;                                                                 
  uint32_t parity;                                                              
	
  uint32_t n;                                                                   
                                                                                
  /* Packet Request */                                                          
  parity = 0U;                                                                  
  SW_WRITE_BIT(1U);                     /* Start Bit */                         
  bit = (request >> 0);                                                           
  SW_WRITE_BIT(bit);                    /* APnDP Bit */                         
  parity += bit;                                                                
  bit = (request >> 1);                                                           
  SW_WRITE_BIT(bit);                    /* RnW Bit */                           
  parity += bit;                                                                
  bit = (request >> 2);                                                           
  SW_WRITE_BIT(bit);                    /* A2 Bit */                            
  parity += bit;                                                                
  bit = (request >> 3);                                                           
  SW_WRITE_BIT(bit);                    /* A3 Bit */                            
  parity += bit;                                                                
  SW_WRITE_BIT(parity);                 /* Parity Bit */                        
  SW_WRITE_BIT(0U);                     /* Stop Bit */                          
  SW_WRITE_BIT(1U);                     /* Park Bit */                          
                                                                                
  /* Turnaround */                                                              
  PIN_SWDIO_DIR_IN();                                                      
  for (n = DAP_Data.swd_conf.turnaround; n; n--) {                              
    SW_CLOCK_CYCLE();                                                           
  }                                                                             
                                                                                
  /* Acknowledge response */                                                    
  bit = SW_READ_BIT();                                                             
  ack  = (bit << 0);                                                              
  bit = SW_READ_BIT();                                                             
  ack |= (bit << 1);                                                              
  bit = SW_READ_BIT();                                                             
  ack |= (bit << 2);                                                              
                                                                                
  if (ack == DAP_TRANSFER_OK) {         /* OK response */                       
    /* Data transfer */                                                         
    if (request & DAP_TRANSFER_RnW) {                                           
      /* Read data */                                                           
      val = 0U;
      parity = 0U;                                                              
      for (n = 32U; n; n--) {                                                   
        bit = SW_READ_BIT();               /* Read RDATA[0:31] */                  
        parity += bit;                                                          
        val >>= 1;                                                              
        val  |= (bit << 31);                                                      
      }                                                                         
      bit = SW_READ_BIT();                 /* Read Parity */
      if ((parity ^ bit) & 1U) {                                                
        ack = DAP_TRANSFER_ERROR;                                               
      }                                                                         
      if (data) { *data = val; }                                                
      /* Turnaround */                                                          
      for (n = DAP_Data.swd_conf.turnaround; n; n--) {                          
        SW_CLOCK_CYCLE();                                                       
      }                                                                         
      PIN_SWDIO_DIR_OUT();
    } else {
      /* Turnaround */
      for (n = DAP_Data.swd_conf.turnaround; n; n--) {                          
        SW_CLOCK_CYCLE();                                                       
      }                                                                         
      PIN_SWDIO_DIR_OUT();                                                   
      /* Write data */                                                          
      val = *data;                                                              
      parity = 0U;                                                              
      for (n = 32U; n; n--) {                                                   
        SW_WRITE_BIT(val);              /* Write WDATA[0:31] */                 
        parity += (val & 0x1);                                                          
        val >>= 1;                                                              
      }                                                                         
      SW_WRITE_BIT(parity);             /* Write Parity Bit */
    }
    /* Idle cycles */
    n = 8;//DAP_Data.transfer.idle_cycles;
    if (n) {
      PIN_SWDIO_OUT(0);
      for (; n; n--) {
        SW_CLOCK_CYCLE();
      }
    }
    PIN_SWDIO_OUT(1);
    return ((uint8_t)ack);                                                      
  }                                                                             
                                                                                
  else if ((ack == DAP_TRANSFER_WAIT) || (ack == DAP_TRANSFER_FAULT)) {              
    
    
  }                                                                             
  else
	{
		
	}
	 PIN_SWDIO_DIR_OUT();
  /* Idle cycles */
	n = 8;//DAP_Data.transfer.idle_cycles;
	if (n) {
		PIN_SWDIO_OUT(0);
		for (; n; n--) {
			SW_CLOCK_CYCLE();
		}
	}
	PIN_SWDIO_OUT(1);
  return ((uint8_t)ack);                                                        
}
// SWD Transfer I/O
//   request: A[3:2] RnW APnDP
//   data:    DATA[31:0]
//   return:  ACK[2:0]
uint8_t  SWD_Transfer(uint32_t request, uint32_t *data)
{
	if(DAP_Data.fast_clock)
	{
		return 0;
			//return SWD_TransferFast(request, data);
	}
	else
	{
		return SWD_TransferSlow(request, data);
	}
}


#endif  /* (DAP_SWD != 0) */
