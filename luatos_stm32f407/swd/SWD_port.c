#include "SWD_port.h"
#include "DAP.h"
#include "main.h"

void PORT_SWD_SETUP(void)
{
	bsp_gpio_init(TRST_O_NAME,GPIO_MODE_OUTPUT_PP,GPIO_PULLUP,GPIO_SPEED_FREQ_VERY_HIGH);
	bsp_gpio_init(SWCLK_O_NAME,GPIO_MODE_OUTPUT_PP,GPIO_PULLUP,GPIO_SPEED_FREQ_VERY_HIGH);
	bsp_gpio_init(SWDIO_NAME,GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_FREQ_VERY_HIGH);	
//	GPIO_InitTypeDef GPIO_InitStruct = {0};

//  /* GPIO Ports Clock Enable */
//  /*Configure GPIO pin Output Level */
//  HAL_GPIO_WritePin(TRST_O_GPIO_Port, TRST_O_Pin, GPIO_PIN_SET);
//	HAL_GPIO_WritePin(TCLK_O_GPIO_Port, TCLK_O_Pin, GPIO_PIN_SET);
//  /*Configure GPIO pin */
//  GPIO_InitStruct.Pin = TRST_O_Pin;
//  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//  GPIO_InitStruct.Pull = GPIO_PULLUP;
//  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
//  HAL_GPIO_Init(TRST_O_GPIO_Port, &GPIO_InitStruct);
//	 
//	GPIO_InitStruct.Pin = TCLK_O_Pin;
//  HAL_GPIO_Init(TCLK_O_GPIO_Port, &GPIO_InitStruct);
//	 
//	GPIO_InitStruct.Pin = SWDIO_Pin;
//  GPIO_InitStruct.Pull = GPIO_NOPULL;
//  HAL_GPIO_Init(SWDIO_GPIO_Port, &GPIO_InitStruct);
}

void PORT_OFF(void)
{
	bsp_gpio_deinit(TRST_O_NAME);
	bsp_gpio_deinit(SWCLK_O_NAME);
	bsp_gpio_deinit(SWDIO_NAME);
//	GPIO_InitTypeDef GPIO_InitStruct = {0};
//  /* GPIO Ports Clock Enable */
//  /*Configure GPIO pin Output Level */
//  HAL_GPIO_WritePin(TRST_O_GPIO_Port, TRST_O_Pin, GPIO_PIN_RESET);
//	HAL_GPIO_WritePin(TCLK_O_GPIO_Port, TCLK_O_Pin, GPIO_PIN_RESET);
//  /*Configure GPIO pin */
//  GPIO_InitStruct.Pin = TRST_O_Pin;
//  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//  HAL_GPIO_Init(TRST_O_GPIO_Port, &GPIO_InitStruct);
//	
//	GPIO_InitStruct.Pin = TCLK_O_Pin;
//	HAL_GPIO_Init(TCLK_O_GPIO_Port, &GPIO_InitStruct);
//	
//	GPIO_InitStruct.Pin = SWDIO_Pin;
//  HAL_GPIO_Init(SWDIO_GPIO_Port, &GPIO_InitStruct);
}

void SW_CLOCK_CYCLE(void)
{  
	PIN_SWCLK_CLR;                      
  PIN_DELAY_SLOW(DAP_Data.clock_delay);                          
  PIN_SWCLK_SET;                      
  PIN_DELAY_SLOW(DAP_Data.clock_delay);
}
void SW_WRITE_BIT(uint32_t bit)
{
  if(bit & 1U)
		PIN_SWDIO_SET;
	else
		PIN_SWDIO_CLR;
  SW_CLOCK_CYCLE();
}
void PIN_SWDIO_OUT(uint8_t bit)
{
	if(bit)
		PIN_SWDIO_SET;
	else
		PIN_SWDIO_CLR;
}
uint8_t SW_READ_BIT(void)
{
	uint8_t bit;
  PIN_SWCLK_CLR;
  PIN_DELAY_SLOW(DAP_Data.clock_delay);
  bit = PIN_SWDIO_IN;
  PIN_SWCLK_SET;
  PIN_DELAY_SLOW(DAP_Data.clock_delay);
	return bit;
}

void PIN_SWDIO_DIR_IN(void)		
{
	PIN_SWDIO_CLR;
	bsp_gpio_init(SWDIO_NAME,GPIO_MODE_INPUT,GPIO_NOPULL,GPIO_SPEED_FREQ_VERY_HIGH);
//	GPIO_InitTypeDef GPIO_InitStruct = {0};
//  /* GPIO Ports Clock Enable */
//  /*Configure GPIO pin Output Level */
//  HAL_GPIO_WritePin(SWDIO_GPIO_Port, SWDIO_Pin, GPIO_PIN_RESET);
//  /*Configure GPIO pin */
//  GPIO_InitStruct.Pin = SWDIO_Pin;
//  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//  GPIO_InitStruct.Pull = GPIO_PULLUP;
//  HAL_GPIO_Init(SWDIO_GPIO_Port, &GPIO_InitStruct);
}
void PIN_SWDIO_DIR_OUT(void)		
{
	PIN_SWDIO_CLR;
	bsp_gpio_init(SWDIO_NAME,GPIO_MODE_OUTPUT_PP,GPIO_NOPULL,GPIO_SPEED_FREQ_VERY_HIGH);	

}
