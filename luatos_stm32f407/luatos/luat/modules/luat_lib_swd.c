#include "luat_base.h"
#include "rotable.h"
#include "luat_swd.h"
static int l_swd_init(lua_State *L);
static int l_swd_erase(lua_State *L);
static int l_swd_program(lua_State *L);
static int l_swd_reset(lua_State *L);
static int l_swd_download(lua_State *L);

static const rotable_Reg swdlib[] = 
{
	{"init",l_swd_init,0},
	{"erase",l_swd_erase,0},
	{"program",l_swd_program,0},
	{"reset",l_swd_reset,0},
	{"download",l_swd_download,0},
	{NULL,NULL,0}
};

LUAMOD_API int luaopen_swd(lua_State *L)
{
	luat_newlib(L,swdlib);
	return 1;
}
/*
SWD初始化函数
@api    swd.init(fireware_name)
@string 固件的名称
@return boolean true:成功  faluse:失败
@usage
status = swd.init("stm32f10x_512.bin")
*/
static int l_swd_init(lua_State *L)
{
	int ret = 0;
	const char *buf;
	if(lua_isstring(L,1) == 0)//判断输入的是字符串
	{
		lua_pushboolean(L,0);//返回值为0
		return 1;//代表lua返回值的个数
	}
	buf = luaL_optstring(L,1,"");
	ret = luat_swd_init((char *)buf);
	if(ret == 0)
	{
		lua_pushboolean(L,1);
	}
	else
	{
		lua_pushboolean(L,0);
	}
	return 1;
	
}
/*
SWD程序擦除
@api    swd.erase(addr)
@int 程序擦除起始地址
@return boolean true:成功  faluse:失败
@usage
status = swd.erase(0x08000000)
*/
static int l_swd_erase(lua_State *L)
{
	int ret = 0;
	uint32_t flash_start_addr = 0;
	if(lua_isinteger(L,1))//判断输入的是数值
	{
		flash_start_addr= luaL_optinteger(L,1,0);
		ret = luat_swd_erase(flash_start_addr);
		if(ret == 0)
		{
			lua_pushboolean(L,1);
		}
		else
		{
			lua_pushboolean(L,0);
		}
		return 1;
	}
	lua_pushboolean(L,0);//返回值为0
	return 1;//代表lua返回值的个数
}
/*
SWD程序编程
@api    swd.program(program_name,addr)
@string 程序文件名称
@int 程序下载起始地址
@return boolean true:成功  faluse:失败
@usage
status = swd.program("test_program.bin",0x08000000)
*/
static int l_swd_program(lua_State *L)
{
	int ret = 0;
	const char *buf;
	uint32_t flash_start_addr = 0;
	if(lua_isstring(L,1) == 0)//判断输入的是字符串
	{
		lua_pushboolean(L,0);//返回值为0
		return 1;//代表lua返回值的个数
	}
	buf= luaL_optstring(L,1,"");
	if(lua_isinteger(L,2) == 0)
	{
		lua_pushboolean(L,0);//返回值为0
		return 1;//代表lua返回值的个数
	}
	flash_start_addr = luaL_optinteger(L,2,0);
	ret = luat_swd_program((char *)buf,flash_start_addr);
	if(ret == 0)
	{
		lua_pushboolean(L,1);
	}
	else
	{
		lua_pushboolean(L,0);
	}
	return 1;
}
/*
SWD程序复位
@api    swd.reset(nil)
@return boolean true:成功  faluse:失败
@usage
status = swd.reset()
*/
static int l_swd_reset(lua_State *L)
{
	int ret = 0;
	ret = luat_swd_reset();
	if(ret == 0)
	{
		lua_pushboolean(L,1);
	}
	else
	{
		lua_pushboolean(L,0);
	}
	return 1;
}
/*
SWD一键程序下载
@api    swd.download(fireware_name,pragram_name,addr)
@string 固件名字
@string 程序文件名称
@addr 程序擦除及下载起始地址
@return boolean true:成功  faluse:失败
@usage
status = swd.download("stm32f10x_512.bin","test_program.bin",0x08000000)
*/
static int l_swd_download(lua_State *L)
{
	int ret = 0;
	uint32_t flash_start_addr = 0;
	if(lua_isstring(L,1))//判断输入的是字符串
	{
		const char *c = luaL_optstring(L,1,"");
		ret = luat_swd_init((char *)c);//swd初始化
		if(ret == 0)
		{
			lua_pushboolean(L,1);
		}
		else
		{
			lua_pushboolean(L,0);
			return 1;
		}
	}
	else
	{
		lua_pushboolean(L,0);
		return 1;
	}
	if(lua_isinteger(L,3))//判断输入的是数值
	{
		flash_start_addr= luaL_optinteger(L,3,0);
	}
	else
	{
		lua_pushboolean(L,0);
		return 1;
	}
	ret = luat_swd_erase(flash_start_addr);//swd程序擦除
	if(ret == 0)
	{
		lua_pushboolean(L,1);
	}
	else
	{
		lua_pushboolean(L,0);
		return 1;
	}
	if(lua_isstring(L,2))//判断输入的是字符串
	{
		const char *c = luaL_optstring(L,2,"");
		ret = luat_swd_program((char *)c,flash_start_addr);//swd程序下载
		if(ret == 0)
		{
			lua_pushboolean(L,1);
		}
		else
		{
			lua_pushboolean(L,0);
			return 1;
		}
	}
	else
	{
		lua_pushboolean(L,0);
		return 1;
	}
	ret = luat_swd_reset();
	if(ret == 0)
	{
		lua_pushboolean(L,1);
	}
	else
	{
		lua_pushboolean(L,0);
		return 1;
	}
	return 1;
}




