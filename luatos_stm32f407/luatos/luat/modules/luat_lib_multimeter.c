#include "luat_base.h"
#include "rotable.h"
#include "luat_multimeter.h"
static int l_multimeter_init(lua_State *L);
static int l_multimeter_measure_dcv(lua_State *L);
static int l_multimeter_measure_dcma(lua_State *L);
static int l_multimeter_measure_dca(lua_State *L);
static int l_multimeter_measure_res(lua_State *L);
static int l_multimeter_measure_beep(lua_State *L);

static const rotable_Reg multimeterlib[] = 
{
	{"init",l_multimeter_init,0},
	{"dcv",l_multimeter_measure_dcv,0},
	{"dcma",l_multimeter_measure_dcma,0},
	{"dca",l_multimeter_measure_dca,0},
	{"res",l_multimeter_measure_res,0},
	{"beep",l_multimeter_measure_beep,0},
	{NULL,NULL,0}
};

LUAMOD_API int luaopen_multimeter(lua_State *L)
{
	luat_newlib(L,multimeterlib);
	return 1;
}
/*
万用表初始化
@api    multimeter.init(nil)
@return nil
@usage
multimeter.init()
*/
static int l_multimeter_init(lua_State *L)
{
	luat_multimeter_init();
	return 0;
}
/*
直流电压测量
@api    multimeter.dcv(nil)
@return int 测量的数值
@usage
dcv = multimeter.dcv()
*/
static int l_multimeter_measure_dcv(lua_State *L)
{
	int measure_val = 0;
	measure_val = luat_multimeter_measure(1);
	lua_pushinteger(L,measure_val);
	return 1;
}
/*
直流电流（mA）测量
@api    multimeter.dcma(nil)
@return int 测量的数值
@usage
dcma = multimeter.dcma()
*/
static int l_multimeter_measure_dcma(lua_State *L)
{
	int measure_val = 0;
	measure_val = luat_multimeter_measure(3);
	lua_pushinteger(L,measure_val);
	return 1;
}
/*
直流电流（A）测量
@api    multimeter.dca(nil)
@return int 测量的数值
@usage
dca = multimeter.dca()
*/
static int l_multimeter_measure_dca(lua_State *L)
{
	int measure_val = 0;
	measure_val = luat_multimeter_measure(4);
	lua_pushinteger(L,measure_val);
	return 1;
}
/*
电阻测量
@api    multimeter.res(nil)
@return int 测量的数值
@usage
res = multimeter.res()
*/
static int l_multimeter_measure_res(lua_State *L)
{
	int measure_val = 0;
	measure_val = luat_multimeter_measure(7);
	lua_pushinteger(L,measure_val);
	return 1;
}
/*
通断测量
@api    multimeter.beep(nil)
@return int 测量的数值
@usage
status = multimeter.beep()
*/
static int l_multimeter_measure_beep(lua_State *L)
{
	int measure_val = 0;
	measure_val = luat_multimeter_measure(9);
	if(measure_val == 1)
	{
		lua_pushboolean(L,1);
	}
	else
	{
		lua_pushboolean(L,0);
	}
	return 1;
}

