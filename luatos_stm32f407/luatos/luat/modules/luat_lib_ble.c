#include "luat_base.h"
#include "luat_ble.h"
#include "luat_malloc.h"
#include "luat_msgbus.h"
#include "luat_fs.h"
#include "string.h"
#include "luat_zbuff.h"
#define LUAT_LOG_TAG "luat.uart"
#include "luat_log.h"

typedef struct luat_uart_cb {
    int received;//回调函数
    int sent;//回调函数
} luat_ble_cb_t;

static luat_ble_cb_t ble_cbs;


int l_ble_handler(lua_State *L, void* ptr) {
    LLOGD("l_uart_handler");
    rtos_msg_t* msg = (rtos_msg_t*)lua_topointer(L, -1);
    lua_pop(L, 1);
    // sent event
    if (msg->arg1 == 0) {
        if (ble_cbs.sent) {
            lua_geti(L, LUA_REGISTRYINDEX, ble_cbs.sent);
            if (lua_isfunction(L, -1)) {
                lua_call(L, 1, 0);
            }
        }
    }
    else {
        if (ble_cbs.received) {
            lua_geti(L, LUA_REGISTRYINDEX, ble_cbs.received);
            if (lua_isfunction(L, -1)) {
                lua_pushinteger(L, msg->arg1);
                lua_call(L, 1, 0);
            }
            else {
                LLOGD("received callback not function\r\n");
            }
        }
        else {
            LLOGD("no received callback");
        }
    }

    // 给rtos.recv方法返回个空数据
    lua_pushinteger(L, 0);
    return 1;
}
/*
蓝牙绑定
@api    ble.bond(name)
@string 蓝牙名称
@return boolean 绑定状态
@usage
ble.bond("123456789")
*/
static int l_ble_bond(lua_State *L)
{
	const char *buf;
	if(lua_isstring(L,1) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	buf = luaL_optstring(L,1,"");
	if(luat_ble_bond(buf) == -1)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	lua_pushboolean(L,1);
	return 1;
}
/*
蓝牙解除绑定
@api    ble.bondc(nil)
@return boolean 解除绑定状态
@usage
ble.bondc()
*/
static int l_ble_bondc(lua_State *L)
{
	if(luat_ble_bondc() == -1)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	lua_pushboolean(L,1);
	return 1;
}

/*
蓝牙数据写入
@api    ble.write(data)
@string/zbuff 待写入的数据，如果是zbuff会从指针起始位置开始读
@int 可选，要发送的数据长度，默认全发
@return int 成功的数据长度
@usage
uart.write( "rdy\r\n")
*/
static int l_ble_write(lua_State *L)
{
    size_t len;
    const char *buf;
    if(lua_isuserdata(L, 1))
    {
        luat_zbuff_t *buff = ((luat_zbuff_t *)luaL_checkudata(L, 1, LUAT_ZBUFF_TYPE));
        len = buff->len - buff->cursor;
        buf = (const char *)(buff->addr + buff->cursor);
    }
    else
    {
        buf = lua_tolstring(L, 1, &len);//取出字符串数据
    }
    if(lua_isinteger(L, 2))
    {
        size_t l = luaL_checkinteger(L, 2);
        if(len > l)
            len = l;
    }
    int result = luat_ble_write((char*)buf, len);
    lua_pushinteger(L, result);
    return 1;
}

/*
蓝牙数据读出
@api    ble.read(len)
@int 读取长度,此处长度无效，读取后返回所有接收到的数据
@file/zbuff 可选：文件句柄或zbuff对象
@return string 读取到的数据 / 传入zbuff时，返回读到的长度，并把zbuff指针后移
@usage
uart.read(16)
*/
static int l_ble_read(lua_State *L)
{
    uint32_t length = luaL_optinteger(L, 1, 1024);
    if(lua_isuserdata(L, 2)){//zbuff对象特殊处理
        luat_zbuff_t *buff = ((luat_zbuff_t *)luaL_checkudata(L, 2, LUAT_ZBUFF_TYPE));
        uint8_t* recv = buff->addr+buff->cursor;
        if(length > buff->len - buff->cursor)
            length = buff->len - buff->cursor;
        int result = luat_ble_read(recv, length);
        if(result < 0)
            result = 0;
        buff->cursor += result;
        lua_pushinteger(L, result);
        return 1;
    }
    uint8_t* recv = luat_heap_malloc(length);
    if (recv == NULL) {
        LLOGE("system is out of memory!!!");
        lua_pushstring(L, "");
        return 1;
    }

    uint32_t read_length = 0;
    while(read_length < length)//循环读完
    {
        int result = luat_ble_read((void*)(recv + read_length), length - read_length);
        if (result > 0) {
            read_length += result;
        }
        else
        {
            break;
        }
    }
    if(read_length > 0)
    {
        if (lua_isinteger(L, 2)) {
            uint32_t fd = luaL_checkinteger(L, 2);
            luat_fs_fwrite(recv, 1, read_length, (FILE*)fd);
        }
        else {
            lua_pushlstring(L, (const char*)recv, read_length);
        }
    }
    else
    {
        lua_pushstring(L, "");
    }
    luat_heap_free(recv);
    return 1;
}

/*
注册蓝牙事件回调
@api    ble.on(event, func)
@string 事件名称
@function 回调方法
@return nil 无返回值
@usage
ble.on(1, "receive", function(len)
    local data = ble.read(len)
    log.info("ble", len, data)
end)
*/
static int l_ble_on(lua_State *L) {
		if (ble_cbs.received != 0) {
				luaL_unref(L, LUA_REGISTRYINDEX, ble_cbs.received);
				ble_cbs.received = 0;
		}
		if (lua_isfunction(L, 1)) {
				lua_pushvalue(L, 1);
				ble_cbs.received = luaL_ref(L, LUA_REGISTRYINDEX);
		}
    return 0;
}

#include "rotable.h"
static const rotable_Reg reg_ble[] =
{
    { "bond",  l_ble_bond,0},
    { "bondc",  l_ble_bondc,0},
    { "write",  l_ble_write,0},
    { "read",   l_ble_read,0},
    { "on",     l_ble_on, 0},
    { NULL,     NULL ,          0}
};

LUAMOD_API int luaopen_ble(lua_State *L)
{
    luat_newlib(L, reg_ble);
    return 1;
}

