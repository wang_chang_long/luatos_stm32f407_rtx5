#include "luat_base.h"
#include "rotable.h"
#include "luat_analog.h"

/*
直流电压输出
@api    analog.voltage(vol)
@number 要输出的电压值
@return boolean true:成功  faluse:失败
@usage
status = analog.voltage(1.0) 输出1V电压
*/
static int l_analog_vol_out(lua_State *L)
{
	int ret = 0;
	float vol = 0;
	if(lua_isnumber(L,1))
	{
		vol = luaL_optnumber(L,1,0.0);
		ret = luat_analog_out_vol(vol);
		if(ret == 0)
		{
			lua_pushboolean(L,1);
		}
		else
		{
			lua_pushboolean(L,0);
		}
		return 1;
	}
	lua_pushboolean(L,0);//返回值为0
	return 1;//代表lua返回值的个数
}
/*
直流电流输出
@api    analog.current(cur)
@number 要输出的电流值
@return boolean true:成功  faluse:失败
@usage
status = analog.current(1.0) 输出1mA电压
*/
static int l_analog_cur_out(lua_State *L)
{
	int ret = 0;
	float cur = 0;
	if(lua_isnumber(L,1))
	{
		cur = luaL_optnumber(L,1,0.0);
		ret = luat_analog_out_cur(cur);
		if(ret == 0)
		{
			lua_pushboolean(L,1);
		}
		else
		{
			lua_pushboolean(L,0);
		}
		return 1;
	}
	lua_pushboolean(L,0);//返回值为0
	return 1;//代表lua返回值的个数
}

static const rotable_Reg analoglib[] = 
{
	{"voltage",l_analog_vol_out,0},
	{"current",l_analog_cur_out,0},
	{NULL,NULL,0}
};

LUAMOD_API int luaopen_analog(lua_State *L)
{
	luat_newlib(L,analoglib);
	return 1;
}




