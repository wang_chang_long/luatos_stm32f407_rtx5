#include "luat_base.h"
#include "rotable.h"
#include "luat_pdflib.h"
/*
pdf�ĵ���ʼ��
@api    pdflib.init(nil)
@return nil
@usage
pdflib.init()
*/
int l_pdflib_init(lua_State *L)
{
	luat_pdflib_init();
	return 0;
}
/*
pdf����һҳ
@api    pdflib.addpage(nil)
@return boolean ״̬
@return int ҳ���
@usage
status,page_num = pdflib.addpage()
*/
int l_pdflib_add_page(lua_State *L)
{
	int ret = 0;
	uint8_t pagnum = 0;
	ret = luat_pdflib_add_page(&pagnum);
	if(ret == 0)
	{
		lua_pushboolean(L,1);
		lua_pushinteger(L,pagnum);
		return 2;
	}
	lua_pushboolean(L,0);
	lua_pushinteger(L,0);
	return 2;//����lua����ֵ�ĸ���
}
/*
pdf��ȡҳ��С
@api    pdflib.getsize(nil)
@return boolean,״̬
@return int ����
@return int �߶�
@usage
status,width,hight = pdflib.getsize()
*/
int l_pdflib_page_GetSize(lua_State *L)
{
	uint8_t pag_num = 0;
	uint16_t width=0,hight=0;
	
	if(lua_isinteger(L,1) == 0)
	{
		lua_pushboolean(L,0);
		return 3;
	}
	pag_num = luaL_optinteger(L,1,0);
	if(luat_pdflib_Page_GetWidth(pag_num,&width) == -1)
	{
		lua_pushboolean(L,0);
		return 3;
	}
	if(luat_pdflib_Page_GetHeight(pag_num,&hight) == -1)
	{
		lua_pushboolean(L,0);
		return 3;
	}
	lua_pushboolean(L,1);
	lua_pushinteger(L,width);
	lua_pushinteger(L,hight);
	return 3;
}
/*
pdf������ɫ
@api    pdflib.setrgb(pag_num,r,g,b)
@int ҳ���
@number ��ɫ
@number ��ɫ
@number ��ɫ
@return boolean,״̬
@usage
status = pdflib.setrgb(0,1.0,0.0,0.0) //��ɫ
*/
int l_pdflib_page_setRGBFill(lua_State *L)
{
	uint8_t pag_num = 0;
	float rgb[3] = {0.0};
	if(lua_isinteger(L,1) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	pag_num = luaL_optinteger(L,1,0);
	for(uint8_t i=2;i<5;i++)
	{
		if(lua_isnumber(L,i) == 0)
		{
			lua_pushboolean(L,0);
			return 1;
		}
		rgb[i-2] = luaL_optnumber(L,i,0.0);
	}
	if(luat_pdflib_page_setRGBFill(pag_num,rgb[0],rgb[1],rgb[2]) == 0)
	{
		lua_pushboolean(L,1);
		return 1;
	}
	lua_pushboolean(L,0);
	return 1;
}
/*
pdf��ʼ��������
@api    pdflib.begintest(pag_num)
@int ҳ���
@return boolean,״̬
@usage
status = pdflib.begintest(0) 
*/
int l_pdflib_page_BeginText(lua_State *L)
{
	uint8_t pag_num = 0;
	if(lua_isinteger(L,1) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	pag_num = luaL_optinteger(L,1,0);
	if(luat_pdflib_Page_BeginText(pag_num) == 0)
	{
		lua_pushboolean(L,1);
		return 1;
	}
	lua_pushboolean(L,0);
	return 1;
}
/*
pdf�ƶ���дλ��
@api    pdflib.setpos(pag_num,x,y)
@int ҳ���
@int ������
@int ������
@return boolean,״̬
@usage
status = pdflib.setpos(0,10,10) 
*/
int l_pdflib_page_MoveTextPos(lua_State *L)
{
	uint8_t pag_num = 0;
	int x=0,y=0;
	if(lua_isinteger(L,1) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	pag_num = luaL_optinteger(L,1,0);
	
	if(lua_isinteger(L,2) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	x = luaL_optinteger(L,2,0);
	
	if(lua_isinteger(L,3) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	y = luaL_optinteger(L,3,0);
	if(luat_pdflib_Page_MoveTextPos(pag_num,x,y) == 0)
	{
		lua_pushboolean(L,1);
		return 1;
	}
	lua_pushboolean(L,0);
	return 1;
}
/*
pdf�������弰��С
@api    pdflib.setfont(pag_num,font,size)
@int ҳ���
@int ����
@int ��С
@return boolean,״̬
@usage
status = pdflib.setfont(0,"Courier",24) //24����������
*/
int l_pdflib_page_SetFontAndSize(lua_State *L)
{
	uint8_t pag_num = 0;
	const char *buf = NULL;
	uint8_t font_size = 0;
	if(lua_isinteger(L,1) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	pag_num = luaL_optinteger(L,1,0);
	
	if(lua_isstring(L,2) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	buf = luaL_optstring(L,2,"SimSun");
	if(lua_isinteger(L,3) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	font_size = luaL_optinteger(L,3,24);
	if(luat_pdflib_Page_SetFontAndSize(pag_num,buf,font_size) == 0)
	{
		lua_pushboolean(L,1);
		return 1;
	}
	lua_pushboolean(L,0);
	return 1;
}
/*
pdfд������
@api    pdflib.writetest(pag_num,text)
@int ҳ���
@string ����
@return boolean,״̬
@usage
status = pdflib.writetest(0,"д������") 
*/
int l_pdflib_page_ShowText(lua_State *L)
{
	uint8_t pag_num = 0;
	const char *buf = NULL;
	if(lua_isinteger(L,1) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	pag_num = luaL_optinteger(L,1,0);
	
	if(lua_isstring(L,2) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	buf = luaL_optstring(L,2,"");
	if(luat_pdflib_page_ShowText(pag_num,buf) == 0)
	{
		lua_pushboolean(L,1);
		return 1;
	}
	lua_pushboolean(L,0);
	return 1;
}
/*
pdf��������д��
@api    pdflib.endtest(pag_num)
@int ҳ���
@return boolean,״̬
@usage
status = pdflib.endtest(0) 
*/
int l_pdflib_page_EndText(lua_State *L)
{
	uint8_t pag_num = 0;
	if(lua_isinteger(L,1) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	pag_num = luaL_optinteger(L,1,0);
	if(luat_pdflib_page_EndText(pag_num) == 0)
	{
		lua_pushboolean(L,1);
		return 1;
	}
	lua_pushboolean(L,0);
	return 1;
}
/*
pdfд������
@api    pdflib.write(pag_num,x,y,text,color)
@int ҳ���
@int x ������
@int y ������
@string Ҫд�������
@int ��ɫ 1����ɫ 2����ɫ 3����ɫ  
@return boolean,״̬
@usage
status = pdflib.write(0,10,10,"д������",1) 
*/
int l_pdflib_page_write(lua_State *L)
{
	uint8_t pag_num = 0,color = 0;
	int x=0,y=0;
	const char *buf = NULL;
	if(lua_isinteger(L,1) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	pag_num = luaL_optinteger(L,1,0);
	
	if(lua_isinteger(L,2) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	x = luaL_optinteger(L,2,0);
	
	if(lua_isinteger(L,3) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	y = luaL_optinteger(L,3,0);
	if(lua_isstring(L,4) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	buf = luaL_optstring(L,4,"");
	color = luaL_optinteger(L,5,1);
	if(luat_pdflib_page_write(pag_num,x,y,buf,color) == 0)
	{
		lua_pushboolean(L,1);
		return 1;
	}
	lua_pushboolean(L,0);
	return 1;
}
/*
pdf����ҳ��
@api    pdflib.savepage(pag_num)
@int ҳ���
@return boolean,״̬
@usage
status = pdflib.savepage(0) 
*/
int l_pdflib_page_SaveContext(lua_State *L)
{
	uint8_t pag_num = 0;
	if(lua_isinteger(L,1) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	pag_num = luaL_optinteger(L,1,0);
	if(luat_pdflib_page_SaveContext(pag_num) == 0)
	{
		lua_pushboolean(L,1);
		return 1;
	}
	lua_pushboolean(L,0);
	return 1;
}
/*
pdf�����ļ�
@api    pdflib.savefile(path)
@string �ļ�·��
@return boolean,״̬
@usage
status = pdflib.savefile("0:log/123456789.pdf") 
*/
int l_pdflib_page_SaveToFile(lua_State *L)
{
	const char *buf = NULL;
	if(lua_isstring(L,1) == 0)
	{
		lua_pushboolean(L,0);
		return 1;
	}
	buf = luaL_optstring(L,1,"error");
	if(luat_pdflib_SaveToFile(buf) == 0)
	{
		lua_pushboolean(L,1);
		return 1;
	}
	lua_pushboolean(L,0);
	return 1;
}


static const rotable_Reg pdflib[] = 
{
	{"init",l_pdflib_init,0},
	{"addpage",l_pdflib_add_page,0},
	{"getsize",l_pdflib_page_GetSize,0},
	{"setrgb",l_pdflib_page_setRGBFill,0},
	{"begintest",l_pdflib_page_BeginText,0},
	{"setpos",l_pdflib_page_MoveTextPos,0},
	{"setfont",l_pdflib_page_SetFontAndSize,0},
	{"writetest",l_pdflib_page_ShowText,0},
	{"endtest",l_pdflib_page_EndText,0},
	{"write",l_pdflib_page_write,0},
	{"savepage",l_pdflib_page_SaveContext,0},
	{"savefile",l_pdflib_page_SaveToFile,0},
	{NULL,NULL,0}
};

LUAMOD_API int luaopen_pdflib(lua_State *L)
{
	luat_newlib(L,pdflib);
	return 1;
}

