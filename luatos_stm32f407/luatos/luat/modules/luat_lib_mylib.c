#include "luat_base.h"
#include "rotable.h"
static int l_jiaFa(lua_State *L);

static const rotable_Reg mylib[] = 
{
	{"jiaFa",l_jiaFa,0},
	{NULL,NULL,0}
};

LUAMOD_API int luaopen_mylib(lua_State *L)
{
	luat_newlib(L,mylib);
	return 1;
}

static int l_jiaFa(lua_State *L)
{
	int a = lua_checkstack(L,1);
	int b = luaL_optinteger(L,2,0);
	lua_pushinteger(L,a+b);
	return 1;
}

