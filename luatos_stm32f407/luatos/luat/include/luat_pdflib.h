#ifndef __LUAT_PDFLIB_H
#define __LUAT_PDFLIB_H

#include "stdint.h"

int luat_pdflib_init(void);
int luat_pdflib_add_page(uint8_t *pag_num);
int luat_pdflib_Page_GetWidth(uint8_t pagenum,uint16_t *width);
int luat_pdflib_Page_GetHeight(uint8_t pagenum,uint16_t *height);
int luat_pdflib_page_setRGBFill(uint8_t pagenum,float r,float g,float b);
int luat_pdflib_Page_BeginText(uint8_t pagenum);
int luat_pdflib_Page_MoveTextPos(uint8_t pagenum,int x,int y);
int luat_pdflib_Page_SetFontAndSize(uint8_t pagenum,const char *fontname,uint8_t size);
int luat_pdflib_page_ShowText(uint8_t pagenum,const char *test);
int luat_pdflib_page_EndText(uint8_t pagenum);
int luat_pdflib_page_SaveContext(uint8_t pagenum);
int luat_pdflib_SaveToFile(const char *filename);
int luat_pdflib_page_write(uint8_t pagenum,int x,int y,const char *buf,uint8_t color);

#endif

