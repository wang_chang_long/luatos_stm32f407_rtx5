#ifndef __LUAT_BLE_H
#define __LUAT_BLE_H

#include "luat_msgbus.h"

int l_ble_handler(lua_State *L, void* ptr);
int luat_ble_write(void* data, size_t length);
int luat_ble_read(void* buffer, size_t length);
int luat_ble_bond(const char *ble_name);
int luat_ble_bondc(void);
#endif

