#ifndef __LUAT_SWD_H
#define __LUAT_SWD_H

#include "stdint.h"

int luat_swd_init(char *firmware_name);
int luat_swd_erase(uint32_t flash_start_addr);
int luat_swd_program(char *program_name,uint32_t flash_start_addr);
int luat_swd_reset(void);

#endif



