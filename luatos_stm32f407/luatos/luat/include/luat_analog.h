#ifndef __LUAT_ANALOG_H
#define __LUAT_ANALOG_H

void luat_analog_init(void);
int luat_analog_out_vol(float vol);
int luat_analog_out_cur(float cur);

#endif
