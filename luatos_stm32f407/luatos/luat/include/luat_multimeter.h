#ifndef __LUAT_MULTIMETER_H
#define __LUAT_MULTIMETER_H

#include "stdint.h"

void luat_multimeter_init(void);
int luat_multimeter_measure(uint8_t current_fun);

#endif

