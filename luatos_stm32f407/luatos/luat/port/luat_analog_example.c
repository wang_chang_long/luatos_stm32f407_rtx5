#include "common.h"
#include "periph.h"
#include "luat_analog.h"

/**
	* @brief  模拟量电压输出 0-5V
	* @note   
	* @param  vol：要输出的电压值
  * @retval	0：成功 1：失败
  */
int luat_analog_out_vol(float vol)
{
	return per_gp8201_set_voltage(vol);
}

/**
	* @brief  模拟量电流输出 0-24ma
	* @note   
	* @param  vol：要输出的电压值
  * @retval	0：成功 1：失败
  */
int luat_analog_out_cur(float cur)
{
	return per_gp8212s_set_current(cur);
}
