#include "luat_multimeter.h"
#include "driver.h"
#include "periph.h"
#include "math.h"
#include "stdlib.h"
static drv_multimeter_desc_t multimeter_desc;

typedef struct
{
	uint8_t fun_code;
	int (*measure_fun)(drv_multimeter_desc_t *desc,uint32_t adc_val);
}luat_multimeter_fun_t;

const luat_multimeter_fun_t multimeter_fun[] =
{
	{1,drv_measure_dcv},
	{2,drv_measure_acv},
	{3,drv_measure_dcma},
	{4,drv_measure_dca},
	{5,drv_measure_acma},
	{6,drv_measure_aca},
	{7,drv_measure_r},
	{8,drv_measure_c},
	{9,drv_measure_beep},
};

void luat_multimeter_init(void)
{
	drv_multimeter_init(&multimeter_desc);
	multimeter_desc.current_fun = 9;//此值一开始必须初始化为9
	per_mcp3421_set_param(MCP3421_MODE_CONTINUITY,MCP3421_PGA_X1,MCP3421_SPS_60);
}
//当前功能 01直流电压  02交流电压  03直流电流mA  04直流电流A  05交流电流mA  06交流电流A  07电阻  08电容  09蜂鸣器
int luat_multimeter_measure(uint8_t current_fun)
{
	uint32_t adc_value = 0;
	int measured_old = 0;
	int measured_new = 0;
	int ret = 0;
	uint8_t cnt = 10;
	if(current_fun == 9)
	{
		if(multimeter_desc.current_fun != 9)
		{
			multimeter_desc.current_fun = current_fun;
			per_mcp3421_set_param(MCP3421_MODE_CONTINUITY,MCP3421_PGA_X1,MCP3421_SPS_60);
			osDelay(500);
		}
		while(cnt--)
		{
			adc_value = per_mcp3421_read();
			if(multimeter_fun[current_fun-1].fun_code == current_fun)
			{
				ret = multimeter_fun[current_fun-1].measure_fun(&multimeter_desc,adc_value);
				if(ret == 0)
				{
						return multimeter_desc.measured_curval;
				}
			}
			osDelay(1000);
		}
		return 0;
	}
	else
	{
		if(multimeter_desc.current_fun == 9)
		{
			multimeter_desc.current_fun = current_fun;
			per_mcp3421_set_param(MCP3421_MODE_CONTINUITY,MCP3421_PGA_X1,MCP3421_SPS_3_75);
			osDelay(500);
		}
		while(cnt--)
		{
			adc_value = per_mcp3421_read();
			if(multimeter_fun[current_fun-1].fun_code == current_fun)
			{
				ret = multimeter_fun[current_fun-1].measure_fun(&multimeter_desc,adc_value);
				if(ret == 0)
				{
					measured_new = abs(multimeter_desc.measured_curval);
					if((measured_old < measured_new * 1.1) && (measured_old > measured_new * 0.9))
					{
						return multimeter_desc.measured_curval;
					}
					measured_old = measured_new;
				}
			}
			osDelay(1000);
		}
		return -1;
	}
}

