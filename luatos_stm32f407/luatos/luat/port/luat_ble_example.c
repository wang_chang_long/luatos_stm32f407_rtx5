#include "luat_ble.h"
#include "periph_ecb02s1.h"
#include "luat_base.h"
#include "luat_msgbus.h"
#include "driver_alloc.h"

typedef struct
{
	uint8_t *recv_buf;
	uint8_t recv_len;
}luat_ble_t;

static luat_ble_t luat_ble = {0};

static void luat_ble_cb(uint8_t *pbuf,uint16_t len)
{
	if(luat_ble.recv_buf == NULL)
	{
		luat_ble.recv_buf = drv_alloc_malloc(DRV_ALLOC_SIZE_128);
	}
	if(luat_ble.recv_buf == NULL)
	{
		return;
	}
	memcpy(luat_ble.recv_buf,pbuf,len);
	luat_ble.recv_len = len;
	rtos_msg_t msg;
	msg.handler = l_ble_handler;
	msg.ptr = NULL;
	msg.arg1 = len;
	luat_msgbus_put(&msg,1);
}

int luat_ble_write(void* data, size_t length)
{
	uint8_t *pdata = data;
	per_ecb02s1_send(pdata,length);
	return length;
}

int luat_ble_read(void* buffer, size_t length)
{
	uint8_t *pbuf = buffer;
	if(luat_ble.recv_buf != NULL && luat_ble.recv_len > 0)
	{
		memcpy(pbuf,luat_ble.recv_buf,luat_ble.recv_len);
		luat_ble.recv_len = 0;
		drv_alloc_free(DRV_ALLOC_SIZE_128,luat_ble.recv_buf);
		luat_ble.recv_buf = NULL;
		return length;
	}
	return -1;
}

int luat_ble_bond(const char *ble_name)
{
	int ret = 0;
	ret = per_ecb02s1_bond(ble_name);
	if(ret == 0)
	{
		per_ecb02s1_recv_register(luat_ble_cb);
	}
	return ret;
}

int luat_ble_bondc(void)
{
	int ret = 0;
	ret = per_ecb02s1_bondc();
	if(ret == 0)
	{
		per_ecb02s1_recv_register(NULL);
	}
	return ret;
}




