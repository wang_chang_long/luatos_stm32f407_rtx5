#include "luat_base.h"
#include "luat_malloc.h"
#include "luat_fs.h"
#include "luat_msgbus.h"
#include "ff.h"
#include "fatfs.h"
extern const struct luat_vfs_filesystem vfs_fs_fatfs;
extern FATFS fs; 

int luat_fs_init(void)
{
//	fatfs_init();
	luat_vfs_reg(&vfs_fs_fatfs);
	luat_fs_conf_t conf = {
	.busname = (char *)&fs,
	.type = "fatfs",
	.filesystem = "fatfs",
	.mount_point = ""
	};
	luat_fs_mount(&conf);
	return 0;
}


