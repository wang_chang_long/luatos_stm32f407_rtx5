#include "luat_base.h"
#include "luat_malloc.h"
#include "luat_msgbus.h"
#include "luat_gpio.h"
#include "luat_irq.h"
#include "bsp_gpio.h"
typedef struct
{
	int pin;
	bsp_gpio_name_t gpio_name;
}luat_gpio_list_t;

#define LUAT_GPIO_MAX    (sizeof(gpio_list)/sizeof(luat_gpio_list_t))

luat_gpio_list_t gpio_list[] =
{
	{39,BSP_GPIOE8_39},
	{88,BSP_GPIOD7_88},
	{84,BSP_GPIOD3_84},
	{85,BSP_GPIOD4_85}
};

/**
	* @brief  通过芯片管脚号查找对应的端口号在列表中的位置
	* @note   
	* @param  pin：芯片管脚号
  * @retval 该管脚号在gpio_list数组中的位置
  */
static uint16_t luat_gpio_search_index(int pin)
{
	uint16_t i =0;
	for(i = 0;i<LUAT_GPIO_MAX;i++)
	{
		if(gpio_list[i].pin == pin)
		{
			return i;
		}
	}
	return i;
}

static void luat_gpio_irq_callback(bsp_gpio_name_t gpio_name)
{
	uint16_t i =0;
	for(i = 0;i<LUAT_GPIO_MAX;i++)
	{
		if(gpio_list[i].gpio_name == gpio_name)
		{
			break;
		}
	}
	if(i == LUAT_GPIO_MAX)
	{
		return;
	}
  luat_irq_gpio_cb(gpio_list[i].pin, NULL);
}

int luat_gpio_setup(luat_gpio_t* gpio)
{
	uint16_t index = 0;
	uint32_t gpio_mode = 0;
	uint32_t gpio_pull = 0;
	index = luat_gpio_search_index(gpio->pin);
	if(index == LUAT_GPIO_MAX)
	{
		return -1;
	}
	switch(gpio->mode)
	{
		case Luat_GPIO_OUTPUT:
			gpio_mode = GPIO_MODE_OUTPUT_PP;
			if(gpio->irq == 1)//输出模式的时候此值为输出默认状态
			{
				bsp_gpio_write(gpio_list[index].gpio_name,GPIO_PIN_SET);
			}
			else
			{
				bsp_gpio_write(gpio_list[index].gpio_name,GPIO_PIN_RESET);
			}
		break;
		case Luat_GPIO_INPUT:
			gpio_mode = GPIO_MODE_INPUT;
		break;
		case Luat_GPIO_IRQ:
			switch(gpio->irq)
			{
				case Luat_GPIO_RISING:
					gpio_mode = GPIO_MODE_IT_RISING;
				break;
				case Luat_GPIO_FALLING:
					gpio_mode = GPIO_MODE_IT_FALLING;
				break;
				case Luat_GPIO_BOTH:
					gpio_mode = GPIO_MODE_IT_RISING_FALLING;
				break;
			}
			bsp_gpio_irq_register(gpio_list[index].gpio_name,luat_gpio_irq_callback);
		break;
	}
	switch(gpio->pull)
	{
		case Luat_GPIO_DEFAULT:
			gpio_pull = GPIO_NOPULL;
		break;
		case Luat_GPIO_PULLUP:
			gpio_pull = GPIO_PULLUP;
		break;
		case Luat_GPIO_PULLDOWN:
			gpio_pull = GPIO_PULLDOWN;
		break;
	}
	bsp_gpio_init(gpio_list[index].gpio_name,gpio_mode,gpio_pull,GPIO_SPEED_FREQ_LOW);
	return 0;
}
int luat_gpio_set(int pin, int level)
{
	uint16_t index = 0;
	index = luat_gpio_search_index(pin);
	if(index == LUAT_GPIO_MAX)
	{
		return -1;
	}
	if(level == Luat_GPIO_HIGH)
	{
		bsp_gpio_write(gpio_list[index].gpio_name,GPIO_PIN_SET);
	}
	else
	{
		bsp_gpio_write(gpio_list[index].gpio_name,GPIO_PIN_RESET);
	}
	return 0;
}
int luat_gpio_get(int pin)
{
	uint16_t index = 0;
	GPIO_PinState bitstatus = GPIO_PIN_RESET;
	index = luat_gpio_search_index(pin);
	if(index == LUAT_GPIO_MAX)
	{
		return -1;
	}
	bitstatus = bsp_gpio_read(gpio_list[index].gpio_name);
	if(bitstatus == GPIO_PIN_SET)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
void luat_gpio_close(int pin)
{
	uint16_t index = 0;
	index = luat_gpio_search_index(pin);
	if(index == LUAT_GPIO_MAX)
	{
		return;
	}
	bsp_gpio_deinit(gpio_list[index].gpio_name);
}

int luat_gpio_set_irq_cb(int pin, luat_gpio_irq_cb cb, void* args)
{
	return 0;
}


