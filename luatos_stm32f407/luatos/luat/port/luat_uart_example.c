#include "luat_base.h"
#include "driver.h"
#include "luat_malloc.h"
#include "luat_msgbus.h"
#include "luat_uart.h"
#include "stm32f4xx_hal.h"
#include "main.h"
#include "periph.h"
#define LUAT_UART_MAX  (sizeof(uart_list)/sizeof(uart_list_t))

static void luat_uart_cb(per_uart_index_t index,uint8_t *pbuf,uint16_t len);

typedef struct
{
	uint8_t uartid;
	per_uart_index_t uart_name;
	per_uart_recv_finish_cb cb;
	uint8_t *recv_buf;
	uint16_t recv_len;
}uart_list_t;

uart_list_t uart_list[] =
{
	{1,PER_UART_INDEX_RS232,luat_uart_cb,NULL,0},
	{6,PER_UART_INDEX_RS485,luat_uart_cb,NULL,0}
};

static uint16_t luat_uart_search_index(int uartid)
{
	uint16_t i = 0;
	for(i = 0;i<LUAT_UART_MAX;i++)
	{
		if(uartid == uart_list[i].uartid)
		{
			return i;
		}
	}
	return i;
}

static uint16_t luat_uart_search_uartid(per_uart_index_t uart_name)
{
	uint16_t i = 0;
	for(i = 0;i<LUAT_UART_MAX;i++)
	{
		if(uart_name == uart_list[i].uart_name)
		{
			return i;
		}
	}
	return i;
}

static void luat_uart_cb(per_uart_index_t uart_name,uint8_t *pbuf,uint16_t len)
{
	uint16_t index = luat_uart_search_uartid(uart_name);
	if(index == LUAT_UART_MAX)
	{
		return;
	}
	if(uart_list[index].recv_buf == NULL)
	{
		uart_list[index].recv_buf = drv_alloc_malloc(DRV_ALLOC_SIZE_512);
	}
	if(uart_list[index].recv_buf == NULL)
	{
		return;
	}
	memcpy(uart_list[index].recv_buf,pbuf,len);
	uart_list[index].recv_len = len;
	rtos_msg_t msg;
	msg.handler = l_uart_handler;
	msg.ptr = NULL;
	msg.arg1 = uart_list[index].uartid;
	msg.arg2 = len;
	luat_msgbus_put(&msg,1);
}

int luat_uart_setup(luat_uart_t* uart)
{
	uint8_t even = 0;
	uint16_t index = luat_uart_search_index(uart->id);
	if(index == LUAT_UART_MAX)
	{
		return -1;
	}
	switch(uart->parity)
	{
		case LUAT_PARITY_NONE:
			even = 'N';
		break;
		case LUAT_PARITY_ODD:
			even = 'O';
		break;
		case LUAT_PARITY_EVEN:
			even = 'E';
		break;
	}
	per_uart_init(uart_list[index].uart_name,uart->baud_rate,even,uart->data_bits,uart->stop_bits);
	return 0;
}
int luat_uart_write(int uartid, void* data, size_t length)
{
	uint8_t *pdata = data;
	uint16_t index = luat_uart_search_index(uartid);
	if(index == LUAT_UART_MAX)
	{
		return -1;
	}
	per_uart_send_data(uart_list[index].uart_name,pdata,length);
	return length;
}
int luat_uart_read(int uartid, void* buffer, size_t length)
{
	uint8_t *pbuf = buffer;
	uint16_t index = luat_uart_search_index(uartid);
	if(index == LUAT_UART_MAX)
	{
		return -1;
	}
	if(uart_list[index].recv_buf != NULL && uart_list[index].recv_len > 0)
	{
		memcpy(pbuf,uart_list[index].recv_buf,uart_list[index].recv_len);
		length = uart_list[index].recv_len;
		uart_list[index].recv_len = 0;
		drv_alloc_free(DRV_ALLOC_SIZE_512,uart_list[index].recv_buf);
		uart_list[index].recv_buf = NULL;
		return length;
	}
	return -1;
}
int luat_uart_close(int uartid)
{
	uint16_t index = luat_uart_search_index(uartid);
	per_uart_deinit(uart_list[index].uart_name);
	return 0;
}
int luat_uart_exist(int uartid)
{
	uint16_t index = luat_uart_search_index(uartid);
	if(index >= LUAT_UART_MAX)
	{
		return 0;
	}
	return 1;
}

int luat_setup_cb(int uartid, int received, int sent)
{
	uint16_t index = luat_uart_search_index(uartid);
	if(index == LUAT_UART_MAX)
	{
		return -1;
	}
	if(received)
	{
		per_uart_recv_finish_register(uart_list[index].uart_name,uart_list[index].cb);
	}
	return 0;
}
