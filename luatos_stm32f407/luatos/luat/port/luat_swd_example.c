#include "luat_swd.h"
#include "swd_host.h"
#include "SWD_flash.h"
#include "ff.h"
#include "string.h"
#include "stdio.h"
#include "common.h"
#include "driver_log.h"
#define FLASH_BUFF_MAX_SIZE   0X400
static uint32_t flash_code[256]; // __attribute__((section("ccmram")));
static program_target_t flash_algo; // __attribute__((section("ccmram")));

/**
	* @brief  swd初始化函数
	* @note   主要用于读取程序下载固件
	* @param  firmware_name：文件名字
  * @retval 0：正确 1：错误
  */
int luat_swd_init(char *firmware_name)
{
	FIL fnew;											/* 文件对象 */
	UINT fnum;            			  /* 文件成功读写数量 */
	FRESULT res_sd;                /* 文件操作结果 */
	char path[50] = {0};
	sprintf(path,"0:firmware/%s",firmware_name);
	res_sd = f_open(&fnew, path, FA_OPEN_EXISTING | FA_READ);
	if(res_sd != FR_OK)
	{
		drv_uart_debug(DEBUG_LOG,"文件打开失败\r\n");
		return -1; 
	}
	res_sd = f_read(&fnew,&flash_algo,sizeof(program_target_t),&fnum);
	if(fnum != sizeof(program_target_t))
	{
		drv_uart_debug(DEBUG_LOG,"固件文件读取错误\r\n");
		return -1;
	}
	flash_algo.algo_blob = flash_code;
	res_sd = f_read(&fnew,flash_code,flash_algo.algo_size,&fnum);
	if(flash_algo.algo_size != fnum)
	{
		drv_uart_debug(DEBUG_LOG,"固件文件读取错误\r\n");
	}
	f_close(&fnew);
	return 0;
}
/**
	* @brief  swd程序擦除
	* @note   用于擦除程序
	* @param  flash_start_addr：起始地址
  * @retval 0：正确 1：错误
  */
int luat_swd_erase(uint32_t flash_start_addr)
{
	error_t err = ERROR_SUCCESS;
	err = target_flash_init(&flash_algo,flash_start_addr);
	if(err != ERROR_SUCCESS)
	{
		drv_uart_debug(DEBUG_LOG,"程序擦除失败\r\n");
		return -1;
	}
	err = target_flash_erase_chip(&flash_algo);
	if(err != ERROR_SUCCESS)
	{
		drv_uart_debug(DEBUG_LOG,"程序擦除失败\r\n");
		return -1;
	}
	return 0;
}
/**
	* @brief  swd程序编程
	* @note   用于程序下载
	* @param  program_name：程序名称
	*					flash_start_addr：起始地址
  * @retval 0：正确 1：错误
  */
int luat_swd_program(char *program_name,uint32_t flash_start_addr)
{
	uint32_t cnt = 0;
	FIL fnew;											/* 文件对象 */
	UINT fnum;            			  /* 文件成功读写数量 */
	FRESULT res_sd;                /* 文件操作结果 */
	uint8_t program_buff[FLASH_BUFF_MAX_SIZE];
	uint8_t check_buff[FLASH_BUFF_MAX_SIZE];
	char path[50] = {0};  				/* 文件路径 */
	sprintf(path,"0:program/%s",program_name);
	res_sd = f_open(&fnew,path,FA_OPEN_EXISTING | FA_READ);
	if(res_sd != FR_OK)
	{
		return -1;
	}
	cnt = f_size(&fnew) / FLASH_BUFF_MAX_SIZE;
	if((f_size(&fnew) % FLASH_BUFF_MAX_SIZE) != 0)
	{
		cnt+=1;
	}
	for(uint32_t i = 0;i<cnt;i++)
	{
		res_sd = f_read(&fnew, program_buff,FLASH_BUFF_MAX_SIZE, &fnum); 
		if(fnum > 0)
		{
			if(target_flash_program_page(&flash_algo,flash_start_addr + i * FLASH_BUFF_MAX_SIZE,program_buff,FLASH_BUFF_MAX_SIZE) != ERROR_SUCCESS)
			{
				drv_uart_debug(DEBUG_LOG,"Program Fault = %d\r\n",i);
				f_close(&fnew);
				return -1;
			}
			drv_uart_debug(DEBUG_LOG,"Program write cnt = %d\r\n",i);
			swd_read_memory(flash_start_addr + i * FLASH_BUFF_MAX_SIZE,check_buff,FLASH_BUFF_MAX_SIZE);
			if(memcmp(check_buff,program_buff,fnum)!=0)
			{
				drv_uart_debug(DEBUG_LOG,"Verify Flash Fault %d\r\n",i);
				f_close(&fnew);
				return -1;
			}
			memset(program_buff,0xff,FLASH_BUFF_MAX_SIZE);
			memset(check_buff,0xff,FLASH_BUFF_MAX_SIZE);
		}
		else
		{
			drv_uart_debug(DEBUG_LOG,"program flie error\r\n");
			f_close(&fnew);
			return -1;
		}
	}
	return 0;
}
/**
	* @brief  swd程序复位
	* @note   用于程序复位
	* @param  program_name：程序名称
	*					flash_start_addr：起始地址
  * @retval 0：正确 1：错误
  */
int luat_swd_reset(void)
{
	drv_uart_debug(DEBUG_LOG,"program flie ok\r\n");
	if(target_flash_uninit() != ERROR_SUCCESS)
	{
		drv_uart_debug(DEBUG_LOG,"程序复位失败，请手动复位\r\n");
		return -1;
	}
	return 0;
}
