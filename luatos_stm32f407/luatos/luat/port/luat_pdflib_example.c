#include "luat_pdflib.h"
#include "pdflib.h"
#include "string.h"

#define LUAT_PDFLIB_MAX_PAGE    10

typedef struct
{
	LPDF_Doc pdf;
	LPDF_Page page[LUAT_PDFLIB_MAX_PAGE];
}luat_pdflib_t;

luat_pdflib_t luat_pdflib; // __attribute__((section("ccmram")));

/**
	* @brief  查找空闲页
	* @note   
	* @param  无
  * @retval	无
  */
static uint8_t luat_pdflib_seatch_freepage(void)
{
	uint8_t i = 0;
	for(i = 0; i< LUAT_PDFLIB_MAX_PAGE;i++)
	{
		if(luat_pdflib.page[i] == NULL)
		{
			break;
		}
	}
	return i;
}


/**
	* @brief  PDF初始化
	* @note   
	* @param  无
  * @retval	无
  */
int luat_pdflib_init(void)
{
	memset(&luat_pdflib,0,sizeof(luat_pdflib));
	luat_pdflib.pdf = LPDF_New();
	if(luat_pdflib.pdf == NULL)
	{
		return -1;
	}
	return 0;
}

/**
	* @brief  PDF文档增加一页
	* @note   
	* @param  无
  * @retval	-1：错误 0：正确  
  */
int luat_pdflib_add_page(uint8_t *pag_num)
{
	uint8_t freepage = 0;
	freepage = luat_pdflib_seatch_freepage();
	if(freepage >= LUAT_PDFLIB_MAX_PAGE)
	{
		return -1;
	}
	if(luat_pdflib.pdf == NULL)
	{
		return -1;
	}
	luat_pdflib.page[freepage] = LPDF_AddPage(luat_pdflib.pdf);
	if(luat_pdflib.page[freepage] == NULL)
	{
		*pag_num = 0;
		return -1;
	}
	*pag_num = freepage;
	return 0;
}
/**
	* @brief  获取pdf的宽度
	* @note   
	* @param  pagenum：页码
						width：获取到的宽度指针
  * @retval	-1：错误 0：正确  
  */
int luat_pdflib_Page_GetWidth(uint8_t pagenum,uint16_t *width)
{
	if(pagenum >= LUAT_PDFLIB_MAX_PAGE)
	{
		return -1;
	}
	if(luat_pdflib.page[pagenum] == NULL)
	{
		return -1;
	}
	*width = LPDF_Page_GetWidth(luat_pdflib.page[pagenum]);
	return 0;
}
/**
	* @brief  获取pdf的高度
	* @note   
	* @param  pagenum：页码
						height：获取到的高度指针
  * @retval	-1：错误 0：正确  
  */
int luat_pdflib_Page_GetHeight(uint8_t pagenum,uint16_t *height)
{
	if(pagenum >= LUAT_PDFLIB_MAX_PAGE)
	{
		return -1;
	}
	if(luat_pdflib.page[pagenum] == NULL)
	{
		return -1;
	}
	*height = LPDF_Page_GetHeight(luat_pdflib.page[pagenum]);
	return 0;
}


/**
	* @brief  设置填充颜色
	* @note   
	* @param  pagenum：页码
						rgb：颜色
  * @retval	-1：错误 0：正确  
  */
int luat_pdflib_page_setRGBFill(uint8_t pagenum,float r,float g,float b)
{
	if(pagenum >= LUAT_PDFLIB_MAX_PAGE)
	{
		return -1;
	}
	if(luat_pdflib.page[pagenum] == NULL)
	{
		return -1;
	}
	if(LPDF_Page_SetRGBFill(luat_pdflib.page[pagenum],r,g,b) == LPDF_OK)
	{
		return 0;
	}
	return -1;
}
/**
	* @brief  开始写入文字
	* @note   
	* @param  pagenum：页码
  * @retval	-1：错误 0：正确  
  */
int luat_pdflib_Page_BeginText(uint8_t pagenum)
{
	if(pagenum >= LUAT_PDFLIB_MAX_PAGE)
	{
		return -1;
	}
	if(luat_pdflib.page[pagenum] == NULL)
	{
		return -1;
	}
	if(LPDF_Page_BeginText(luat_pdflib.page[pagenum]) == LPDF_OK)
	{
		return 0;
	}
	return -1;
}
/**
	* @brief  位置移动
	* @note   坐标相对于左下角
	* @param  pagenum：页码
						x：横坐标
						y：纵坐标
  * @retval	-1：错误 0：正确  
  */
int luat_pdflib_Page_MoveTextPos(uint8_t pagenum,int x,int y)
{
	if(pagenum >= LUAT_PDFLIB_MAX_PAGE)
	{
		return -1;
	}
	if(luat_pdflib.page[pagenum] == NULL)
	{
		return -1;
	}
	if(LPDF_Page_MoveTextPos(luat_pdflib.page[pagenum],x,y) == LPDF_OK)
	{
		return 0;
	}
	return -1;
}
/**
	* @brief  设置字体及大小
	* @note   
	* @param  pagenum：页码
						fontname：字体名字
						size：字体大小
  * @retval	-1：错误 0：正确  
  */
int luat_pdflib_Page_SetFontAndSize(uint8_t pagenum,const char *fontname,uint8_t size)
{
	if(pagenum >= LUAT_PDFLIB_MAX_PAGE)
	{
		return -1;
	}
	if(luat_pdflib.page[pagenum] == NULL)
	{
		return -1;
	}
	if(LPDF_Page_SetFontAndSize(luat_pdflib.page[pagenum],fontname,size) == LPDF_OK)
	{
		return 0;
	}
	return -1;
}
/**
	* @brief  写入文字
	* @note   
	* @param  pagenum：页码
						test：待写入的文字
  * @retval	-1：错误 0：正确  
  */
int luat_pdflib_page_ShowText(uint8_t pagenum,const char *test)
{
	if(pagenum >= LUAT_PDFLIB_MAX_PAGE)
	{
		return -1;
	}
	if(luat_pdflib.page[pagenum] == NULL)
	{
		return -1;
	}
	if(LPDF_Page_ShowText(luat_pdflib.page[pagenum],test) == LPDF_OK)
	{
		return 0;
	}
	return -1;
}
/**
	* @brief  结束写入文字
	* @note   
	* @param  pagenum：页码
  * @retval	-1：错误 0：正确  
  */
int luat_pdflib_page_EndText(uint8_t pagenum)
{
	if(pagenum >= LUAT_PDFLIB_MAX_PAGE)
	{
		return -1;
	}
	if(luat_pdflib.page[pagenum] == NULL)
	{
		return -1;
	}
	if(LPDF_Page_EndText(luat_pdflib.page[pagenum]) == LPDF_OK)
	{
		return 0;
	}
	return -1;
}

int luat_pdflib_page_write(uint8_t pagenum,int x,int y,const char *buf,uint8_t color)
{
	float r,g,b;
	if(pagenum >= LUAT_PDFLIB_MAX_PAGE)
	{
		return -1;
	}
	if(luat_pdflib.page[pagenum] == NULL)
	{
		return -1;
	}
	switch(color)
	{
		case 1://黑色
			r = 0.0;
			g = 0.0;
			b = 0.0;
		break;
		case 2://红色
			r = 1.0;
			g = 0.0;
			b = 0.0;
		break;
		case 3://绿色
			r = 0.0;
			g = 1.0;
			b = 0.0;
		break;
		default:
			r = 0.0;
			g = 0.0;
			b = 0.0;
	}
	if(LPDF_Page_SetRGBFill(luat_pdflib.page[pagenum],r,g,b) != LPDF_OK)
	{
		return -1;
	}
	if(LPDF_Page_BeginText(luat_pdflib.page[pagenum]) != LPDF_OK)
	{
		return -1;
	}
	if(LPDF_Page_MoveTextPos(luat_pdflib.page[pagenum],x,y) != LPDF_OK)
	{
		return -1;
	}
	if(LPDF_Page_ShowText(luat_pdflib.page[pagenum],buf) != LPDF_OK)
	{
		return -1;
	}
	if(LPDF_Page_MoveTextPos(luat_pdflib.page[pagenum],0-x,0-y) != LPDF_OK)
	{
		return -1;
	}
	if(LPDF_Page_EndText(luat_pdflib.page[pagenum]) != LPDF_OK)
	{
		return -1;
	}
	return 0;
}

/**
	* @brief  保存页
	* @note   
	* @param  pagenum：页码
  * @retval	-1：错误 0：正确  
  */
int luat_pdflib_page_SaveContext(uint8_t pagenum)
{
	if(pagenum >= LUAT_PDFLIB_MAX_PAGE)
	{
		return -1;
	}
	if(luat_pdflib.page[pagenum] == NULL)
	{
		return -1;
	}
	if(LPDF_Page_SaveContext(luat_pdflib.page[pagenum]) == LPDF_OK)
	{
		luat_pdflib.page[pagenum] = NULL;
		return 0;
	}
	return -1;
}

/**
	* @brief  保存PDF文档
	* @note   
	* @param  pagenum：页码
  * @retval	-1：错误 0：正确  
  */
int luat_pdflib_SaveToFile(const char *filename)
{
	if(filename == NULL)
	{
		return -1;
	}
	if(luat_pdflib.pdf == NULL)
	{
		return -1;
	}
	if(LPDF_SaveToFile(luat_pdflib.pdf,filename) == LPDF_OK)
	{
		memset(&luat_pdflib,0,sizeof(luat_pdflib));
		return 0;
	}
	return -1;
}





