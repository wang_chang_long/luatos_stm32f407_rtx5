#include "lfs_port.h"
#include "bsp_sdio_sd.h"

// Read a region in a block. Negative error codes are propogated
// to the user.
int lfs_port_read(const struct lfs_config *c, lfs_block_t block,
            lfs_off_t off, void *buffer, lfs_size_t size)
{
	 SD_Error status;
	/* Read block of many bytes from address 0 */
    status = SD_ReadBlock(buffer, block * c->block_size + off, size);
    /* Check if the Transfer is finished */
    status = SD_WaitReadOperation();
    while(SD_GetStatus() != SD_TRANSFER_OK);
	return 0;
}

// Program a region in a block. The block must have previously
// been erased. Negative error codes are propogated to the user.
// May return LFS_ERR_CORRUPT if the block should be considered bad.
int lfs_port_prog(const struct lfs_config *c, lfs_block_t block,
            lfs_off_t off, const void *buffer, lfs_size_t size)
{
	SD_Error status;
	/* Write block of 512 bytes on address 0 */
	status = SD_WriteBlock(buffer, block * c->block_size + off, size);
	/* Check if the Transfer is finished */
	status = SD_WaitWriteOperation();
	while(SD_GetStatus() != SD_TRANSFER_OK);
	return 0;
}

// Erase a block. A block must be erased before being programmed.
// The state of an erased block is undefined. Negative error codes
// are propogated to the user.
// May return LFS_ERR_CORRUPT if the block should be considered bad.
int lfs_port_erase(const struct lfs_config *c, lfs_block_t block)
{
	SD_Error status;
	status = SD_Erase(0x00, block * c->block_size);
	return 0;
}

// Sync the state of the underlying block device. Negative error codes
// are propogated to the user.
int lfs_port_sync(const struct lfs_config *c)
{
	return 0;
}

