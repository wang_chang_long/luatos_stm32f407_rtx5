#ifndef __PERIPH_MCP3421_H
#define	__PERIPH_MCP3421_H

#include "bsp.h"

//#define PER_MCP3421_TEST

//模式
typedef enum
{
	MCP3421_MODE_SIGNAL = 0,
	MCP3421_MODE_CONTINUITY,
}mcp3421_mode_e;
//分辨率
typedef enum
{
	MCP3421_SPS_240 = 0,
	MCP3421_SPS_60,
	MCP3421_SPS_15,
	MCP3421_SPS_3_75,
}mcp3421_sps_e;
//增益
typedef enum
{
	MCP3421_PGA_X1 = 0,
	MCP3421_PGA_X2,
	MCP3421_PGA_X4,
	MCP3421_PGA_X8,
}mcp3421_pga_e;

void per_mcp3421_init(void);
uint32_t per_mcp3421_read(void);
double per_mcp3421_get_voltage(mcp3421_mode_e mode,mcp3421_pga_e pga,mcp3421_sps_e sps);
void per_mcp3421_set_param(mcp3421_mode_e mode,mcp3421_pga_e pga,mcp3421_sps_e sps);

#ifdef PER_MCP3421_TEST
void per_mcp3421_test(void);
#endif

#endif

