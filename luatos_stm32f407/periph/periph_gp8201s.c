#include "periph_gp8201s.h"
#include "math.h"

#define GP8201S_I2C_NAME    BSP_INDEX_IIC2
#define GP8201S_DEV_ADDR    0XB0


/**
	* @brief  GP8201S初始化
	* @note   
	* @param  无
  * @retval 无
  */
void per_gp8201s_init(void)
{
	bsp_i2c_init(GP8201S_I2C_NAME);
}
/**
	* @brief  GP8201S写入数据
	* @note   
	* @param  无
  * @retval 无
  */
void per_gp8201s_write(uint16_t adc_val)
{
	uint8_t dat_buf[4] = {0};
	dat_buf[0] = GP8201S_DEV_ADDR;
	dat_buf[1] = 0x02;
	dat_buf[2] =  (adc_val & 0x0f)<<4;
	dat_buf[3] = (adc_val >> 4) & 0xff;
	bsp_i2c_start(GP8201S_I2C_NAME);
	for(uint8_t i=0;i<4;i++)
	{
		bsp_i2c_send_byte(GP8201S_I2C_NAME,dat_buf[i]);
		bsp_i2c_wait_ack(GP8201S_I2C_NAME);
	}
	bsp_i2c_stop(GP8201S_I2C_NAME);
}
/**
	* @brief  GP8201S读取数据
	* @note   
	* @param  无
  * @retval 无
  */
uint16_t per_gp8201s_read(void)
{
	uint8_t dat[3] = {0};
	uint16_t adc_val = 0;
	bsp_i2c_start(GP8201S_I2C_NAME);
	bsp_i2c_send_byte(GP8201S_I2C_NAME,GP8201S_DEV_ADDR | 0x01);
	bsp_i2c_wait_ack(GP8201S_I2C_NAME);
	for(uint8_t i =0;i<3;i++)
	{
		dat[i] = bsp_i2c_read_byte(GP8201S_I2C_NAME,1);
	}
	bsp_i2c_stop(GP8201S_I2C_NAME);
	adc_val = dat[2] & 0x0f;
	adc_val = ((adc_val << 8) & dat[1]);
	return adc_val;
}
/**
	* @brief  GP8201s设置输出的电压值
	* @note   
	* @param  sel：模式
	*					voltage：电压值
  * @retval 0：成功 -1：失败
  */
int per_gp8201s_set_voltage(gp8201s_sel_e sel,double voltage)
{
	uint16_t adc_val = 0;
	if(sel == GP8201S_SEL_5V)
	{
		adc_val = voltage * 0xFFF / 5.0;
	}
	else
	{
		adc_val = voltage * 0xFFF / 10.0;
	}
	per_gp8201s_write(adc_val);
//	if(per_gp8201s_read() == adc_val)
//	{
//		return 0;
//	}
	return -1;
}

#ifdef PER_GP8201S_TEST
void per_gp8201s_test(void)
{
	per_gp8201s_init();
	per_gp8201s_set_voltage(GP8201S_SEL_5V,5);
}


#endif
	


