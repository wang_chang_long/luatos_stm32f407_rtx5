#ifndef __PERIPH_GP8201_H
#define __PERIPH_GP8201_H

#include "stdint.h"
#include "bsp.h"

//#define PER_GP8201_TEST

void per_gp8201_init(void);
int per_gp8201_set_voltage(float voltage);

#ifdef PER_GP8201_TEST
void per_gp8201_test(void);

#endif

#endif


