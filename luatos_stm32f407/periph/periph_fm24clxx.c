#include "periph_fm24clxx.h"
#include "bsp.h"

#define FM24CLXX_I2C  BSP_INDEX_IIC0
/**
	* @brief  fm24clxx初始化
	* @note   
	* @param  无
  * @retval 无
  */
void per_fm24clxx_init(void)
{
	bsp_i2c_init(FM24CLXX_I2C);
}
/**
	* @brief  fm24clxx写一个字节数据
	* @note   
	* @param  add：地址
	*					wbyte：字节数据
  * @retval 无
  */
void per_fm24clxx_write_byte(uint16_t add, uint8_t wbyte)
{
	uint8_t temph,templ;
	temph = (uint8_t)(add >> 8);
	templ = (uint8_t)(add & 0xFF);	

	bsp_i2c_start(FM24CLXX_I2C);
	bsp_i2c_send_byte(FM24CLXX_I2C,0xa0);
	
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	bsp_i2c_send_byte(FM24CLXX_I2C,temph);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	bsp_i2c_send_byte(FM24CLXX_I2C,templ);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	
	bsp_i2c_send_byte(FM24CLXX_I2C,wbyte);
	bsp_i2c_wait_ack(FM24CLXX_I2C);

	bsp_i2c_stop(FM24CLXX_I2C);
}

/**
	* @brief  fm24clxx读取一个字节数据
	* @note   
	* @param  add：地址
  * @retval 读取的数据
  */
uint8_t per_fm24clxx_read_byte (uint16_t add)
{
  uint8_t temph,templ,temp;
	
	temph = (uint8_t)(add >> 8);
	templ = (uint8_t)(add & 0xff);
	
	bsp_i2c_start(BSP_INDEX_IIC0);
  bsp_i2c_send_byte(FM24CLXX_I2C,0xa0);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	bsp_i2c_send_byte(FM24CLXX_I2C,temph);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	bsp_i2c_send_byte(FM24CLXX_I2C,templ);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	bsp_i2c_start(BSP_INDEX_IIC0);
	bsp_i2c_send_byte(FM24CLXX_I2C,0xa1);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	temp = bsp_i2c_read_byte(FM24CLXX_I2C,1);
	bsp_i2c_stop(FM24CLXX_I2C);

  return(temp);
}

/**
	* @brief  fm24clxx写多个字节数据
	* @note   
	* @param  add：地址
	*					dat：字节数据
	*					len：数据长度
  * @retval 无
  */
void per_fm24clxx_write(uint16_t add, uint8_t *dat, uint16_t len)
{
	uint8_t temph,templ;
	uint16_t i;
	
	temph = (uint8_t)(add >> 8);
	templ = (uint8_t)(add & 0xff);
	
	bsp_i2c_start(BSP_INDEX_IIC0); 		
	bsp_i2c_send_byte(FM24CLXX_I2C,0xa0);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	bsp_i2c_send_byte(FM24CLXX_I2C,temph);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	bsp_i2c_send_byte(FM24CLXX_I2C,templ);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	
	for(i=0;i<len;i++)
	{
		bsp_i2c_send_byte(FM24CLXX_I2C,*(dat + i));
		bsp_i2c_wait_ack(FM24CLXX_I2C);
	}
	
	bsp_i2c_stop(FM24CLXX_I2C);	

}

/**
	* @brief  fm24clxx读取多个字节数据
	* @note   
	* @param  add：地址
	*					dat：字节数据
	*					len：数据长度
  * @retval 无
  */
void per_fm24clxx_read(uint16_t add, uint8_t *dat, uint16_t len)
{
	uint8_t temph,templ;
	uint16_t  i;
	
	temph=(uint8_t)(add >> 8);
	templ=(uint8_t)(add & 0xff);
	
	bsp_i2c_start(BSP_INDEX_IIC0);
  bsp_i2c_send_byte(FM24CLXX_I2C,0xa0);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	bsp_i2c_send_byte(FM24CLXX_I2C,temph);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	bsp_i2c_send_byte(FM24CLXX_I2C,templ);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	bsp_i2c_start(BSP_INDEX_IIC0);
	bsp_i2c_send_byte(FM24CLXX_I2C,0xa1);
	bsp_i2c_wait_ack(FM24CLXX_I2C);
	for(i=0;i<len-1;i++)
	{
		*(dat + i) = bsp_i2c_read_byte(FM24CLXX_I2C,1);
	}
	*(dat + len - 1) = bsp_i2c_read_byte(FM24CLXX_I2C,0);
	
	bsp_i2c_stop(FM24CLXX_I2C);
	
}

#ifdef PER_FM24CLXX_TEST
uint8_t read_buff[5] = {0};
uint8_t write_buff[5] = {0x11,0x22,0x33,0x44,0x55}; 
void per_fm24clxx_test(void)
{
	per_fm24clxx_write(0,write_buff,5);
	com_delay_ms(1000);
	per_fm24clxx_read(0,read_buff,5);
}

#endif














