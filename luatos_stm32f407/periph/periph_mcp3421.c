#include "periph_mcp3421.h"
#include "cmsis_os2.h"
#include "math.h"
#define MCP3421_I2C_NAME    BSP_INDEX_IIC1
#define MCP3421_DEV_ADDR    0XD0

const static float mcp3421_lsb_list[4] = {1000,250,62.5,15.625};
const static uint8_t mcp3421_pga_list[4] = {1,2,4,8};

/**
	* @brief  MCP3421初始化
	* @note   
	* @param  无
  * @retval 无
  */
void per_mcp3421_init(void)
{
	bsp_i2c_init(MCP3421_I2C_NAME);
}
/**
	* @brief  MCP3421写数据
	* @note   
	* @param  dat：一个字节数据
  * @retval 无
  */
void per_mcp3421_write(uint8_t dat)
{
	bsp_i2c_start(MCP3421_I2C_NAME);
	bsp_i2c_send_byte(MCP3421_I2C_NAME,MCP3421_DEV_ADDR);//写设备地址
	bsp_i2c_wait_ack(MCP3421_I2C_NAME);
	bsp_i2c_send_byte(MCP3421_I2C_NAME,dat);
	bsp_i2c_wait_ack(MCP3421_I2C_NAME);
	bsp_i2c_stop(MCP3421_I2C_NAME);
}
/**
	* @brief  MCP3421读数据
	* @note   
	* @param  无
  * @retval 返回4个字节数据
  */
uint32_t per_mcp3421_read(void)
{
	uint32_t adc_val = 0;
	uint8_t step = 0,temp = 0;
	bsp_i2c_start(MCP3421_I2C_NAME);
	bsp_i2c_send_byte(MCP3421_I2C_NAME,MCP3421_DEV_ADDR | 0x01);//读数据
	if(bsp_i2c_wait_ack(MCP3421_I2C_NAME) == -1)
	{
		return 0;
	}
	for(step = 0; step < 3; step++)
	{
		if(step < 2) 
		{
			temp = bsp_i2c_read_byte(MCP3421_I2C_NAME,1);//ACK
		}
		else
		{
			temp = bsp_i2c_read_byte(MCP3421_I2C_NAME,0);//NACK
		}
		adc_val = (adc_val << 8) | temp;
		adc_val &= 0x03ffff;
	}
	bsp_i2c_stop(MCP3421_I2C_NAME);
	return adc_val;
}


void per_mcp3421_set_param(mcp3421_mode_e mode,mcp3421_pga_e pga,mcp3421_sps_e sps)
{
	uint8_t cfg_param = 0x90;

	if(mode == MCP3421_MODE_CONTINUITY)
	{
		cfg_param |= (1<<4);
	}
	else
	{
		cfg_param &= (~(1<<4));
	}
	cfg_param |= (pga<<0);
	cfg_param |= (sps<<2);
	per_mcp3421_write(cfg_param);
}

/**
	* @brief  MCP3421获取电压值
	* @note   
	* @param  无
  * @retval 电压值,单位为uV
  */
double per_mcp3421_get_voltage(mcp3421_mode_e mode,mcp3421_pga_e pga,mcp3421_sps_e sps)
{
	uint32_t adc_val = 0;
	double voltage = 0.0;
	if(mode == MCP3421_MODE_SIGNAL)
	{
		per_mcp3421_set_param(MCP3421_MODE_SIGNAL,pga,sps);
	}
	adc_val = per_mcp3421_read();
	if(adc_val & (1<<17))
	{
		adc_val &= 0x01ffff;
		voltage = -(double)(mcp3421_lsb_list[sps] * (adc_val))/mcp3421_pga_list[pga];
	}
	else
	{
		adc_val &= 0x03ffff;
		voltage = (double)(mcp3421_lsb_list[sps] * (adc_val))/mcp3421_pga_list[pga];
	}
	return voltage;
}

#ifdef PER_MCP3421_TEST
uint32_t adc_val_test = 0;
void per_mcp3421_test(void)
{
	per_mcp3421_init();
	per_mcp3421_set_param(MCP3421_MODE_CONTINUITY,MCP3421_PGA_X1,MCP3421_SPS_60);
	osDelay(1000);
	while(1)
	{
		adc_val_test = per_mcp3421_read();
		osDelay(3000);
	}
	
}

#endif
