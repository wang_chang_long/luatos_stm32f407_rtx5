#ifndef __PER_FM24CLXX_H
#define __PER_FM24CLXX_H

#include "stdint.h"

//#define PER_FM24CLXX_TEST

void per_fm24clxx_init(void);
void per_fm24clxx_write(unsigned short add, unsigned char *dat, unsigned short len);
void per_fm24clxx_read(unsigned short add, unsigned char *dat, unsigned short len);
void per_fm24clxx_write_byte(unsigned short add, unsigned char wbyte);
uint8_t per_fm24clxx_read_byte (unsigned short add);

#ifdef PER_FM24CLXX_TEST
void per_fm24clxx_test(void);
#endif

#endif

