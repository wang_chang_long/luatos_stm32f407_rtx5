#ifndef __PERIPH_GP8212S_H
#define __PERIPH_GP8212S_H

#include "stdint.h"
#include "bsp.h"

//#define PER_GP8212S_TEST

void per_gp8212s_init(void);
int per_gp8212s_set_current(float voltage);

#ifdef PER_GP8212S_TEST
void per_gp8212s_test(void);

#endif

#endif


