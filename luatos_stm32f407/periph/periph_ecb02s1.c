#include "periph_ecb02s1.h"
#include "driver_alloc.h"
#include "string.h"
#include "driver_at.h"
#include "cmsis_os2.h"
#include "stdio.h"
osMessageQueueId_t msgqueueid_ble_recv = NULL;



per_ble_recv_finish_cb recv_cb;

void per_ecb02s1_send(uint8_t *pdat,uint16_t len)
{
	per_uart_send_data(PER_UART_INDEX_BLE,pdat,len);
}

void per_ecb02s1_recv_cb(per_uart_index_t uart_name,uint8_t *pbuf,uint16_t len)
{
	if(recv_cb != NULL)
	{
		recv_cb(pbuf,len);
	}
}

uint8_t *per_ecb02s1_wait_recv(uint32_t timeout,uint16_t *len)
{
	return per_uart_wait_recv(PER_UART_INDEX_BLE,timeout,len);
}

static void per_ecb02s1_clear(void)
{
	per_uart_clr(PER_UART_INDEX_BLE);
}

void per_ecb02s1_recv_register(per_ble_recv_finish_cb cb)
{
	recv_cb = cb;
	per_uart_recv_finish_register(PER_UART_INDEX_BLE,per_ecb02s1_recv_cb);
}

/**
	* @brief  发送数据后等待数据接收
  * @note   当正确接收到数据后将数据缓冲区返回
	* @param  _ackstr ：数据返回对比字符串
	*					_tick_timeOut ：等待时长，当值为0时表示无限等待
  * @retval 接收到的数据包
  */
static uint8_t *per_ecb02s1_wait_respond(char *_ackstr, uint32_t _tick_timeOut)
{
	uint8_t *msg=NULL;
	uint32_t startostick=0;
	uint16_t recv_len=0;

	startostick=osKernelGetTickCount();
	while(1)
	{
		if(com_diff_time(startostick,osKernelGetTickCount())>=_tick_timeOut)
		{
			break;
		}
		msg = per_ecb02s1_wait_recv(_tick_timeOut,&recv_len);

		if((msg != NULL)&&(recv_len > 0))
		{
			if(strstr((char *)msg,_ackstr) != NULL)
			{
				return msg;
			}
			per_ecb02s1_clear();
		}
	}
	return NULL;
}
/**
	* @brief  配置参数
	* @note   
	* @param  None
  * @retval None
  */
static void per_ecb02s1_cfg(void)
{
	BLE_SLEEP(0);
	BLE_AT(0);
	BLE_DIS(1);
}


int per_ecb02s1_at(void)
{
	char *pbuff = NULL;
	uint8_t *recv_msg = NULL;
	pbuff = drv_alloc_malloc(DRV_ALLOC_SIZE_128);
	if(pbuff == NULL)
	{
		return -1;
	}
	sprintf(pbuff,"AT\r\n");
	per_ecb02s1_clear();
	per_ecb02s1_send((uint8_t *)pbuff,strlen(pbuff));
	drv_alloc_free(DRV_ALLOC_SIZE_128,pbuff);
	recv_msg = per_ecb02s1_wait_respond("OK\r\n",1000);
	if(recv_msg != NULL)
	{
		per_ecb02s1_clear();
		return 0;
	}
	return -1;
}

int per_ecb02s1_scan(const char *ble_name)
{
	char *pbuff = NULL;
	uint8_t *recv_msg = NULL;
	uint8_t cnt = 5;
	uint8_t num = 0;
	char *pmsg = NULL;
	pbuff = drv_alloc_malloc(DRV_ALLOC_SIZE_128);
	if(pbuff == NULL)
	{
		return -1;
	}
	sprintf(pbuff,"AT+SCAN\r\n");
	per_ecb02s1_clear();
	per_ecb02s1_send((uint8_t *)pbuff,strlen(pbuff));
	recv_msg = per_ecb02s1_wait_respond("+SCAN:",10000);
	if(recv_msg != NULL)
	{
		pmsg = strstr((char *)recv_msg,ble_name);
		if(pmsg != NULL)
		{
			while(cnt--)
			{
				if(*pmsg == '\n')
				{
					pmsg++;
					break;
				}
				pmsg--;
			}
			num = com_get_str(pmsg,2);
			sprintf(pbuff,"AT+CONNECT=%d\r\n",num);
			per_ecb02s1_clear();
			per_ecb02s1_send((uint8_t *)pbuff,strlen(pbuff));
			drv_alloc_free(DRV_ALLOC_SIZE_128,pbuff);
			recv_msg = per_ecb02s1_wait_respond("OK\r\n",5000);
			if(recv_msg != NULL)
			{
				per_ecb02s1_clear();
				return 0;
			}
		}
	}
	per_ecb02s1_clear();
	drv_alloc_free(DRV_ALLOC_SIZE_128,pbuff);
	return -1;
}
/**
	* @brief  ecb02s1初始化
	* @note   
	* @param  pbuf：数据缓存区
	*					len：数据长度
  * @retval None
  */
void per_ecb02s1_init(void)
{
	uint8_t cnt = 0;
	bsp_gpio_init(BLE_STA_PORT,GPIO_MODE_INPUT,GPIO_NOPULL,GPIO_SPEED_LOW);
	bsp_gpio_init(BLE_SLEEP_PORT,GPIO_MODE_OUTPUT_PP,GPIO_PULLUP,GPIO_SPEED_LOW);
	bsp_gpio_init(BLE_AT_PORT,GPIO_MODE_OUTPUT_PP,GPIO_PULLUP,GPIO_SPEED_LOW);
	bsp_gpio_init(BLE_DIS_PORT,GPIO_MODE_OUTPUT_PP,GPIO_PULLUP,GPIO_SPEED_LOW);
	per_uart_init(PER_UART_INDEX_BLE,115200,'N',8,1);
	per_ecb02s1_cfg();
	osDelay(1000);
	while(1)
	{
		if(per_ecb02s1_at() == 0)
		{
			break;
		}
		cnt++;
		if(cnt > 5)
		{
			return;
		}
		osDelay(1000);
	}
	per_ecb02s1_bondc();
	osDelay(1000);
	per_ecb02s1_scantime(10000);
}

/**
	* @brief  断开连接
	* @note   
	* @param  None
  * @retval None
  */
void per_ecb02s1_discon(void)
{
	BLE_DIS(0);
	osDelay(1000);
	BLE_DIS(1);
}
/**
	* @brief  配置扫描时间
	* @note   数值 500 - 10000ms
	* @param  None
  * @retval None
  */
int per_ecb02s1_scantime(uint32_t timeout)
{
	char *pbuff = NULL;
	uint8_t *recv_msg = NULL;
	if(timeout <500 || timeout > 10000)
	{
		return -1;
	}
	pbuff = drv_alloc_malloc(DRV_ALLOC_SIZE_128);
	if(pbuff == NULL)
	{
		return -1;
	}
	sprintf(pbuff,"AT+SCANTIME=%d\r\n",timeout);
	per_uart_recv_clr(PER_UART_INDEX_BLE);
	per_ecb02s1_send((uint8_t *)pbuff,strlen(pbuff));
	drv_alloc_free(DRV_ALLOC_SIZE_128,pbuff);
	recv_msg = per_ecb02s1_wait_respond("OK\r\n",10000);
	if(recv_msg != NULL)
	{
		per_ecb02s1_clear();
		return 0;
	}
	return -1;
}



/**
	* @brief  绑定从机
	* @note   
	* @param  None
  * @retval None
  */
int per_ecb02s1_bond(const char *ble_name)
{
	char *pbuff = NULL;
	uint8_t *recv_msg = NULL;
	pbuff = drv_alloc_malloc(DRV_ALLOC_SIZE_128);
	if(pbuff == NULL)
	{
		return -1;
	}
	osDelay(3000);
	sprintf(pbuff,"AT+BONDNAME=%s\r\n",ble_name);
	per_ecb02s1_clear();
	per_ecb02s1_send((uint8_t *)pbuff,strlen(pbuff));
	drv_alloc_free(DRV_ALLOC_SIZE_128,pbuff);
	recv_msg = per_ecb02s1_wait_respond("CONNECT OK\r\n",60000);
	if(recv_msg != NULL)
	{
		per_ecb02s1_clear();
		return 0;
	}
	return -1;
}

/**
	* @brief  解除绑定从机
	* @note   
	* @param  None
  * @retval None
  */
int per_ecb02s1_bondc(void)
{
	char *pbuff = NULL;
	uint8_t *recv_msg = NULL;
	pbuff = drv_alloc_malloc(DRV_ALLOC_SIZE_128);
	if(pbuff == NULL)
	{
		return -1;
	}
	per_ecb02s1_discon();
	sprintf(pbuff,"AT+BONDC\r\n");
	per_ecb02s1_clear();
	per_ecb02s1_send((uint8_t *)pbuff,strlen(pbuff));
	drv_alloc_free(DRV_ALLOC_SIZE_128,pbuff);
	recv_msg = per_ecb02s1_wait_respond("OK\r\n",10000);
	if(recv_msg != NULL)
	{
		per_ecb02s1_clear();
		return 0;
	}
	return -1;
}


#ifdef MODULE_ECB02S1_TEST

void module_ecb02s1_test(void)
{
	uint8_t *pmsg = NULL;
	uint16_t recv_len = 0;
//	osDelay(5000);
	per_ecb02s1_bond("12345678");
	while(1)
	{
		pmsg = per_ecb02s1_wait_recv(osWaitForever,&recv_len);
		if(pmsg != NULL && recv_len > 0)
		{
//			per_uart_send_data(PER_UART_INDEX_TTL,pmsg,recv_len);
			per_ecb02s1_send(pmsg,recv_len);
			per_ecb02s1_clear();
//			per_ecb02s1_bondc();
		}
	}
}
#endif


