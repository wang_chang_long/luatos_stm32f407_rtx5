#include "periph_gp8201.h"
#include "math.h"
#include "cmsis_os2.h"
#define GP8201_SEL_5V
#define GP8201_SEL_10V

#define GP8201_I2C_NAME    BSP_INDEX_IIC2
#define GP8201_DEV_ADDR    0XB0


/**
	* @brief  GP8201初始化
	* @note   
	* @param  无
  * @retval 无
  */
void per_gp8201_init(void)
{
	bsp_i2c_init(GP8201_I2C_NAME);
}
/**
	* @brief  GP8201S写入数据
	* @note   
	* @param  无
  * @retval 无
  */
void per_gp8201_write(uint16_t adc_val)
{
	uint8_t dat_buf[4] = {0};
	dat_buf[0] = GP8201_DEV_ADDR;
	dat_buf[1] = 0x02;
	dat_buf[2] = adc_val & 0xff;
	dat_buf[3] = (adc_val >> 8) & 0x0f;
	bsp_i2c_start(GP8201_I2C_NAME);
	for(uint8_t i=0;i<4;i++)
	{
		bsp_i2c_send_byte(GP8201_I2C_NAME,dat_buf[i]);
		bsp_i2c_wait_ack(GP8201_I2C_NAME);
	}
	bsp_i2c_stop(GP8201_I2C_NAME);
}
/**
	* @brief  GP8201S读取数据
	* @note   
	* @param  无
  * @retval 无
  */
uint16_t per_gp8201_read()
{
	uint8_t dat[3] = {0};
	uint16_t adc_val = 0;
	bsp_i2c_start(GP8201_I2C_NAME);
	bsp_i2c_send_byte(GP8201_I2C_NAME,GP8201_DEV_ADDR | 0x01);
	bsp_i2c_wait_ack(GP8201_I2C_NAME);
	for(uint8_t i =0;i<3;i++)
	{
		dat[i] = bsp_i2c_read_byte(GP8201_I2C_NAME,1);
	}
	bsp_i2c_stop(GP8201_I2C_NAME);
	adc_val = dat[2] & 0x0f;
	adc_val = ((adc_val << 8) & dat[1]);
	return adc_val;
}
/**
	* @brief  GP8201s设置输出的电压值
	* @note   
	* @param  sel：模式
	*					voltage：电压值
  * @retval 0：成功 -1：失败
  */
int per_gp8201_set_voltage(float voltage)
{
	uint16_t adc_val_write = 0;
	uint16_t adc_val_read = 0;
	
	#ifdef GP8201_SEL_5V
		if(voltage > 5)
		{
			voltage = 5.0;
		}
		adc_val_write = voltage * 0xFFF / 5;
	#else
		if(voltage > 10)
		{
			voltage = 10.0;
		}
		adc_val_write = voltage * 0xFFF / 10.0;
	#endif
	per_gp8201_write(adc_val_write);
	osDelay(100);
	adc_val_read = per_gp8201_read();
	if(adc_val_read == adc_val_write)
	{
		return 0;
	}
	return -1;
}

#ifdef PER_GP8201_TEST
void per_gp8201_test(void)
{
	per_gp8201_init();
	per_gp8201_set_voltage(3);
}


#endif
	


