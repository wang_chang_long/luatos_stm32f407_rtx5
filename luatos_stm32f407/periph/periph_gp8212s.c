#include "periph_gp8212s.h"
#include "math.h"


#define GP8212S_I2C_NAME    BSP_INDEX_IIC2
#define GP8212S_DEV_ADDR    0XB0


/**
	* @brief  GP8201初始化
	* @note   
	* @param  无
  * @retval 无
  */
void per_gp8212s_init(void)
{
	bsp_i2c_init(GP8212S_I2C_NAME);
}
/**
	* @brief  GP8201S写入数据
	* @note   
	* @param  无
  * @retval 无
  */
int per_gp8212s_write(uint16_t adc_val)
{
	uint8_t dat_buf[4] = {0};
	dat_buf[0] = GP8212S_DEV_ADDR;
	dat_buf[1] = 0x02;
	dat_buf[2] = adc_val & 0xff;
	dat_buf[3] = (adc_val >> 8) & 0x7f;
	bsp_i2c_start(GP8212S_I2C_NAME);
	for(uint8_t i=0;i<4;i++)
	{
		bsp_i2c_send_byte(GP8212S_I2C_NAME,dat_buf[i]);
		if(bsp_i2c_wait_ack(GP8212S_I2C_NAME) == -1)
		{
			return -1;
		}
	}
	bsp_i2c_stop(GP8212S_I2C_NAME);
	return 0;
}
/**
	* @brief  GP8201S读取数据
	* @note   
	* @param  无
  * @retval 无
  */
uint16_t per_gp8212s_read(uint8_t slave_addr)
{
	uint8_t dat[3] = {0};
	uint16_t adc_val = 0;
	if(slave_addr > 7)
	{
		return 0;
	}
	bsp_i2c_start(GP8212S_I2C_NAME);
	bsp_i2c_send_byte(GP8212S_I2C_NAME,GP8212S_DEV_ADDR | 0x01);
	bsp_i2c_wait_ack(GP8212S_I2C_NAME);
	for(uint8_t i =0;i<3;i++)
	{
		dat[i] = bsp_i2c_read_byte(GP8212S_I2C_NAME,1);
	}
	bsp_i2c_stop(GP8212S_I2C_NAME);
	adc_val = dat[2] & 0x0f;
	adc_val = ((adc_val << 8) & dat[1]);
	return adc_val;
}
/**
	* @brief  GP8201s设置输出的电流
	* @note   
	* @param  sel：模式
	*					voltage：电压值
  * @retval 0：成功 -1：失败
  */
int per_gp8212s_set_current(float iout)
{
	uint16_t adc_val = 0;
	if(iout > 20)
	{
		iout = 20;
	}
	adc_val = iout * 0x7FFF * 100 / 2500;
	return per_gp8212s_write(adc_val);
	
}

#ifdef PER_GP8212S_TEST
void per_gp8212s_test(void)
{
	per_gp8212s_init();
	per_gp8212s_set_current(5);
}


#endif
	


