#ifndef __PERIPH_GP8201S_H
#define __PERIPH_GP8201S_H

#include "stdint.h"
#include "bsp.h"

#define PER_GP8201S_TEST

typedef enum
{
	GP8201S_SEL_5V = 0,
	GP8201S_SEL_10V
}gp8201s_sel_e;

void per_gp8201s_init(void);
int per_gp8201s_set_voltage(gp8201s_sel_e sel,double voltage);

#ifdef PER_GP8201S_TEST
void per_gp8201s_test(void);

#endif

#endif


