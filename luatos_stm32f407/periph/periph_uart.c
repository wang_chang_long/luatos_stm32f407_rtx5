#include <cmsis_os2.h>
#include "periph_uart.h"
#include "common.h"

static uint8_t rs232_recv_buf[MAX_RS232_RCV]; //__attribute__((section("ccmram")));
static uint8_t ttl_recv_buf[MAX_TTL_RCV]; //__attribute__((section("ccmram")));
static uint8_t ble_recv_buf[MAX_BLE_RCV]; //__attribute__((section("ccmram")));
static uint8_t rs485_recv_buf[MAX_RS485_RCV]; //__attribute__((section("ccmram")));
//此结构体的顺序应与per_uart_index_t枚举里面定义的顺序对应起来
static per_uart_t per_uart[PER_UART_INDEX_END] = 
{
	{BSP_UART1,BSP_GPIO_NONE	,0,0,0,EVENT_RS232_RCV,NULL,MAX_RS232_RCV	,rs232_recv_buf,NULL},
	{BSP_UART2,BSP_GPIO_NONE	,0,0,0,EVENT_TTL_RCV	,NULL,MAX_TTL_RCV   ,ttl_recv_buf	 ,NULL},
	{BSP_UART3,BSP_GPIO_NONE	,0,0,0,EVENT_BLE_RCV	,NULL,MAX_BLE_RCV   ,ble_recv_buf	 ,NULL},
	{BSP_UART6,BSP_GPIOA15_77	,0,0,0,EVENT_RS485_RCV,NULL,MAX_RS485_RCV	,rs485_recv_buf,NULL},
};

osTimerId_t timerid_uart_recv;
osEventFlagsId_t eventid_uart_recv;
const osMutexAttr_t mutex_uart_attr = {
  .attr_bits = osMutexRecursive | osMutexPrioInherit | osMutexRobust,  // attr_bits
  .cb_mem = NULL,                                     // memory for control block   
  .cb_size = 0U                                        // size for control block
};

/**
	* @brief  串口接收定时器回调函数.
	* @note   
	* @param  无
  * @retval 无
  */
static void timer_cb(void *param)
{
	for(int i=0;i<PER_UART_INDEX_END;i++)
	{
		if(per_uart[i].recv_flag == 1)
		{
			if(com_diff_time(per_uart[i].recv_time,osKernelGetTickCount()) >= 100)
			{
				per_uart[i].recv_flag = 0;
				per_uart[i].recv_buff[per_uart[i].recv_size] = 0;//避免毡包，避免解析字符串时出错
				if(per_uart[i].recv_finish_cb != NULL)
				{
					per_uart[i].recv_finish_cb((per_uart_index_t)i,per_uart[i].recv_buff,per_uart[i].recv_size);
					per_uart[i].recv_size = 0;
				}
				else
				{
					osEventFlagsSet(eventid_uart_recv,per_uart[i].recv_event);
				}
			}
		}
	}
}
static uint8_t per_uart_search_name(bsp_uart_name_t uart_name)
{
	uint8_t index = 0;
	for(index = 0;index < PER_UART_INDEX_END;index++)
	{
		if(per_uart[index].uart_name == uart_name)
		{
			return index;
		}
	}
	return PER_UART_INDEX_END;
}

/**
	* @brief  串口接收一个字节数据
	* @note   
	* @param  index：设备名称
  * @retval 无
  */
static void per_uart_isr_cb(bsp_uart_name_t uart_name, uint8_t data)
{
	uint8_t index = 0;
	index = per_uart_search_name(uart_name);
	if(index >= PER_UART_INDEX_END)
	{
		return;
	}
	if(per_uart[index].recv_buff != NULL)
	{
		per_uart[index].recv_buff[per_uart[index].recv_size++] = data;
		per_uart[index].recv_size %= per_uart[index].max_len;
		per_uart[index].recv_flag = 1;
		per_uart[index].recv_time = osKernelGetTickCount();
	}
}

/**
	* @brief  注册串口接收完成回调函数
	* @note   
	* @param  index：第几路串口
	*					recv_finish_cb：接收完成回调函数
  * @retval 无
  */
void per_uart_recv_finish_register(per_uart_index_t index,per_uart_recv_finish_cb recv_finish_cb)
{
	if(index >= PER_UART_INDEX_END)
	{
		return;
	}
	per_uart[index].recv_finish_cb = recv_finish_cb;
}

/**
	* @brief  串口接口初始化函数
	* @note   
	* @param  index：设备名称
	*					bau：波特率
	*					even：检验位
	*					dat_len：数据位
	*         stop_bit：停止位
  * @retval 无
  */
void per_uart_init(per_uart_index_t index,uint32_t bau, uint8_t even, uint8_t dat_len, uint8_t stop_bits)
{
	if(index >= PER_UART_INDEX_END)
	{
		return;
	}
	if(eventid_uart_recv == NULL)
	{
		eventid_uart_recv = osEventFlagsNew(NULL);
	}
	if(timerid_uart_recv == NULL)
	{
		timerid_uart_recv = osTimerNew(timer_cb,osTimerPeriodic,(void *)0,NULL);
		if(timerid_uart_recv)
		{
			osTimerStart(timerid_uart_recv, 20);
		}
	}
	if(per_uart[index].recv_mutexid == NULL)
	{
		per_uart[index].recv_mutexid = osMutexNew(&mutex_uart_attr);//创建互斥量
		if(per_uart[index].recv_mutexid)
		{
			osMutexRelease(per_uart[index].recv_mutexid);//释放互斥量
		}
	}
	bsp_uart_recv_register(per_uart[index].uart_name,per_uart_isr_cb);
	bsp_gpio_init(per_uart[index].rs485_enpin_name,GPIO_MODE_OUTPUT_PP,GPIO_PULLDOWN,GPIO_SPEED_FREQ_HIGH);
	bsp_gpio_write(per_uart[index].rs485_enpin_name,GPIO_PIN_RESET);
	bsp_uart_init(per_uart[index].uart_name,bau,even,dat_len,stop_bits);
}
/**
	* @brief  串口接口反初始化函数
	* @note   
	* @param  index：设备名称
  * @retval 无
  */
void per_uart_deinit(per_uart_index_t index)
{
	if(index >= PER_UART_INDEX_END)
	{
		return;
	}
	if(per_uart[index].recv_mutexid)
	{
		osMutexDelete(per_uart[index].recv_mutexid);
		per_uart[index].recv_mutexid = NULL;
	}
	bsp_uart_deinit(per_uart[index].uart_name);
	bsp_uart_recv_register(per_uart[index].uart_name,NULL);
	bsp_gpio_deinit(per_uart[index].rs485_enpin_name);
}

/**
	* @brief  串口发送数据
	* @note   
	* @param  index：设备名称
	*					pdat：数据指针
	*         len：数据长度
  * @retval 无
  */
void per_uart_send_data(per_uart_index_t index,uint8_t *pdat,uint16_t len)
{
	if(index >= PER_UART_INDEX_END)
	{
		return;
	}
//	thread_switch_disable();
	bsp_gpio_write(per_uart[index].rs485_enpin_name,GPIO_PIN_SET);
	bsp_uart_send_dat(per_uart[index].uart_name,pdat,len);
	bsp_gpio_write(per_uart[index].rs485_enpin_name,GPIO_PIN_RESET);
//	thread_switch_enable();
}

/**
	* @brief  串口接收不定长数据
	* @note   
	* @param  index：设备名称
	*					time：等待时间
	*         len：接收数据长度
  * @retval 返回数据指针
  */
uint8_t *per_uart_wait_recv(per_uart_index_t index, uint32_t time, uint16_t *len)
{
	uint8_t *tmp = NULL;
	uint32_t wait_flag = 0;
	if(index >= PER_UART_INDEX_END)
	{
		*len = 0;
		return NULL;
	}
	wait_flag = osEventFlagsWait(eventid_uart_recv,per_uart[index].recv_event,osFlagsWaitAny, time);
	if(osflag_check(wait_flag))
	{
		tmp = per_uart[index].recv_buff;
		*len = per_uart[index].recv_size;
		return tmp;
	}
	else
	{
		tmp = NULL;
		*len = 0;
		return tmp;
	}
}

/**
	* @brief  清除串口接收数据
	* @note   
	* @param  index：设备名称
  * @retval 返回数据指针
  */
uint32_t per_uart_recv_clr(per_uart_index_t index)
{
	uint32_t ilen = 0;
	osStatus_t status = osOK;
	if(index >= PER_UART_INDEX_END)
	{
		return NULL;
	}
	ilen = per_uart[index].recv_size;
	status = osMutexAcquire(per_uart[index].recv_mutexid,2000);
	if(status == osOK)
	{
		per_uart[index].recv_flag = 0;
		per_uart[index].recv_size = 0;
		osMutexRelease(per_uart[index].recv_mutexid);
	}
	return ilen;
}
/**
	* @brief  清除串口接收事件标志
	* @note   
	* @param  index：设备名称
  * @retval 返回数据指针
  */
void per_uart_event_clr(per_uart_index_t index)
{
	if(index >= PER_UART_INDEX_END)
	{
		return;
	}
	osEventFlagsClear(eventid_uart_recv,per_uart[index].recv_event);
}

/**
	* @brief  清除串口接收事件标志
	* @note   
	* @param  index：设备名称
  * @retval 返回数据指针
  */
void per_uart_clr(per_uart_index_t index)
{
	per_uart_event_clr(index);
	per_uart_recv_clr(index);
}

#ifdef PER_UART_TEST
void per_uart_test(void)
{
	uint8_t *pmsg = NULL;
	uint16_t recv_len = 0;
	per_uart_init(PER_UART_INDEX_RS232,115200,'N',8,1);
	per_uart_init(PER_UART_INDEX_RS485,115200,'N',8,1);
	while(1)
	{
//		pmsg = per_uart_wait_recv(PER_UART_INDEX_RS485,osWaitForever,&recv_len);
//		if(pmsg && recv_len > 0)
//		{
//			per_uart_send_data(PER_UART_INDEX_RS485,pmsg,recv_len);
//		}
		pmsg = per_uart_wait_recv(PER_UART_INDEX_RS232,osWaitForever,&recv_len);
		if(pmsg && recv_len > 0)
		{
			per_uart_send_data(PER_UART_INDEX_RS232,pmsg,recv_len);
		}
	}
}
#endif




