#ifndef __PERIPH_UART_H
#define __PERIPH_UART_H

#include "bsp.h"
#include "cmsis_os2.h"

//#define PER_UART_TEST

#define MAX_RS232_RCV 		256
#define MAX_TTL_RCV				256
#define MAX_BLE_RCV				256
#define MAX_RS485_RCV     256

#define EVENT_RS232_RCV  (1<<0)
#define EVENT_TTL_RCV    (1<<1)
#define EVENT_BLE_RCV    (1<<2)
#define EVENT_RS485_RCV  (1<<3)

typedef enum{
	PER_UART_INDEX_RS232 = 0,
	PER_UART_INDEX_TTL,
	PER_UART_INDEX_BLE,
	PER_UART_INDEX_RS485,
	PER_UART_INDEX_END,
}per_uart_index_t;

typedef void (*per_uart_recv_finish_cb)(per_uart_index_t uart_name,uint8_t *pbuf,uint16_t len);

typedef struct{
	const bsp_uart_name_t uart_name;
	const bsp_gpio_name_t  rs485_enpin_name;
	uint8_t		recv_flag;
	uint32_t	recv_time;
	uint32_t	recv_size;
	uint32_t  recv_event;
	osMutexId_t recv_mutexid;
	const uint32_t  max_len;
	uint8_t 	*recv_buff;
	per_uart_recv_finish_cb recv_finish_cb;
}per_uart_t;
void per_uart_init(per_uart_index_t index,uint32_t bau, uint8_t even, uint8_t dat_len, uint8_t stop_bits);
void per_uart_deinit(per_uart_index_t index);
void per_uart_send_data(per_uart_index_t index,uint8_t *pdat,uint16_t len);
void per_uart_recv_finish_register(per_uart_index_t index,per_uart_recv_finish_cb recv_finish_cb);
uint32_t per_uart_recv_clr(per_uart_index_t index);
void per_uart_event_clr(per_uart_index_t index);
void per_uart_clr(per_uart_index_t index);
uint8_t *per_uart_wait_recv(per_uart_index_t index, uint32_t time, uint16_t *len);
#ifdef PER_UART_TEST
void per_uart_test(void);
#endif

#endif


