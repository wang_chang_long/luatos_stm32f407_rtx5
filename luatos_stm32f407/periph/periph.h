#ifndef __PERIPH_H
#define __PERIPH_H

#include "periph_uart.h"
#include "periph_fm24clxx.h"
#include "periph_gp8201.h"
#include "periph_gp8212s.h"
#include "periph_mcp3421.h"
#include "periph_ecb02s1.h"
void per_init(void);

#endif



