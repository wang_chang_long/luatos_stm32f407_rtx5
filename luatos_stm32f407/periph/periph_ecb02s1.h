#ifndef __MODULE_ECB02S1_H
#define __MODULE_ECB02S1_H

#include "bsp.h"

#define MODULE_ECB02S1_TEST


#define BLE_MAX_RECV_LEN   128

#define BLE_STA_PORT       BSP_GPIOE12_43
#define BLE_SLEEP_PORT     BSP_GPIOE13_44
#define BLE_AT_PORT        BSP_GPIOE14_45
#define BLE_DIS_PORT       BSP_GPIOE15_46

#define BLE_STA            bsp_gpio_read(BLE_STA_PORT)      
#define BLE_SLEEP(level)   bsp_gpio_write(BLE_SLEEP_PORT,(GPIO_PinState)level)
#define BLE_AT(level)      bsp_gpio_write(BLE_AT_PORT,(GPIO_PinState)level)
#define BLE_DIS(level)     bsp_gpio_write(BLE_DIS_PORT,(GPIO_PinState)level)

#define BLE_EVENT_RECV_FINISH   (1<<0)

typedef void (*per_ble_recv_finish_cb)(uint8_t *pbuf,uint16_t len);

typedef struct
{
	uint16_t recv_len;
	uint8_t recv_buff[BLE_MAX_RECV_LEN];
}ble_recvdata_t;

void per_ecb02s1_init(void);
int per_ecb02s1_bondc(void);
void per_ecb02s1_send(uint8_t *pdat,uint16_t len);
void per_ecb02s1_discon(void);
int per_ecb02s1_scantime(uint32_t timeout);
int per_ecb02s1_bond(const char *ble_name);
uint8_t *per_ecb02s1_wait_recv(uint32_t timeout,uint16_t *len);
void per_ecb02s1_recv_register(per_ble_recv_finish_cb cb);

#ifdef MODULE_ECB02S1_TEST
void module_ecb02s1_test(void);
#endif


#endif


