5、模拟量输出
电压输出范围0-5V
电流输出范围0-20mA
（1）
/*
直流电压输出
@api    analog.voltage(vol)
@number 要输出的电压值
@return boolean true:成功  faluse:失败
@usage
status = analog.voltage(1.0) 输出1V电压
*/

（2）
/*
直流电流输出
@api    analog.current(cur)
@number 要输出的电流值
@return boolean true:成功  faluse:失败
@usage
status = analog.current(1.0) 输出1mA电压
*/