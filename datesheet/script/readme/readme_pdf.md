6、pdf文档输出
（1）
/*
pdf文档初始化
@api    pdflib.init(nil)
@return nil
@usage
pdflib.init()
*/

（2）
/*
pdf增加一页
@api    pdflib.addpage(nil)
@return boolean 状态
@return int 页编号
@usage
status,page_num = pdflib.addpage()
*/

（3）
/*
pdf获取页大小
@api    pdflib.getsize(nil)
@return boolean,状态
@return int 宽度
@return int 高度
@usage
status,width,hight = pdflib.getsize()
*/

（4）
/*
pdf设置颜色
@api    pdflib.setrgb(pag_num,r,g,b)
@int 页编号
@number 红色
@number 绿色
@number 蓝色
@return boolean,状态
@usage
status = pdflib.setrgb(0,1.0,0.0,0.0) //红色
*/

（5）
/*
pdf开始输入文字
@api    pdflib.begintest(pag_num)
@int 页编号
@return boolean,状态
@usage
status = pdflib.begintest(0) 
*/

（6）
/*
pdf移动书写位置
@api    pdflib.setpos(pag_num,x,y)
@int 页编号
@int 横坐标
@int 纵坐标
@return boolean,状态
@usage
status = pdflib.setpos(0,10,10) 
*/

（7）
/*
pdf设置字体及大小
@api    pdflib.setfont(pag_num,font,size)
@int 页编号
@int 字体
@int 大小
@return boolean,状态
@usage
status = pdflib.setfont(0,"Courier",24) //24号中文字体
*/

（8）
/*
pdf写入文字
@api    pdflib.writetest(pag_num,text)
@int 页编号
@string 文字
@return boolean,状态
@usage
status = pdflib.writetest(0,"写入文字") 
*/

（9）
/*
pdf结束文字写入
@api    pdflib.endtest(pag_num)
@int 页编号
@return boolean,状态
@usage
status = pdflib.endtest(0) 
*/

（10）
/*
pdf写入文字
@api    pdflib.write(pag_num,x,y,text,color)
@int 页编号
@int x 横坐标
@int y 纵坐标
@string 要写入的文字
@int 颜色 1：黑色 2：红色 3：绿色  
@return boolean,状态
@usage
status = pdflib.write(0,10,10,"写入文字",1) 
*/

（11）
/*
pdf保存页面
@api    pdflib.savepage(pag_num)
@int 页编号
@return boolean,状态
@usage
status = pdflib.savepage(0) 
*/

（12）
/*
pdf保存文件
@api    pdflib.savefile(path)
@string 文件路径
@return boolean,状态
@usage
status = pdflib.savefile("0:log/123456789.pdf") 
*/

(13)
具体应用可参考例程