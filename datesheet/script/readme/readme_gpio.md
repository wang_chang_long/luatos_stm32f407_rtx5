1、GPIO功能：
(1)可直接参考luatos文档里面的内容对GPIO进行操作，或者参考example里面的例程。
目前可以通过GPIO进行操作的IO口很少，如下所示
pin      功能
39		蜂鸣器
88		引出
84		DI
85		DO	

(2)
A、/*
设置管脚功能
@api gpio.setup(pin, mode, pull, irq)
@int pin 针脚编号,必须是数值
@any mode 输入输出模式. 数字0/1代表输出模式,nil代表输入模式,function代表中断模式
@int pull 上拉下列模式, 可以是gpio.PULLUP 或 gpio.PULLDOWN, 需要根据实际硬件选用
@int irq 默认gpio.BOTH。中断触发模式, 上升沿gpio.RISING, 下降沿gpio.FALLING, 上升和下降都要gpio.BOTH
@return any 输出模式返回设置电平的闭包, 输入模式和中断模式返回获取电平的闭包
@usage
-- 设置gpio17为输入
gpio.setup(17, nil)
@usage
-- 设置gpio17为输出
gpio.setup(17, 0)
@usage
-- 设置gpio27为中断
gpio.setup(27, function(val) print("IRQ_27",val) end, gpio.PULLUP)
*/

(3) 
/*
设置管脚电平
@api gpio.set(pin, value)
@int pin 针脚编号,必须是数值
@int value 电平, 可以是 高电平gpio.HIGH, 低电平gpio.LOW, 或者直接写数值1或0
@return nil 无返回值
@usage
-- 设置gpio17为低电平
gpio.set(17, 0)
*/

(4)
/*
获取管脚电平
@api gpio.get(pin)
@int pin 针脚编号,必须是数值
@return value 电平, 高电平gpio.HIGH, 低电平gpio.LOW, 对应数值1和0
@usage
-- 获取gpio17的当前电平
gpio.get(17)
*/

(5)
/*
关闭管脚功能(高阻输入态),关掉中断
@api gpio.close(pin)
@int pin 针脚编号,必须是数值
@return nil 无返回值,总是执行成功
@usage
-- 关闭gpio17
gpio.close(17)
*/

(6)
/*
设置GPIO脚的默认上拉/下拉设置, 默认是平台自定义(一般为开漏).
@api gpio.setDefaultPull(val)
@int val 0平台自定义,1上拉, 2下拉
@return boolean 传值正确返回true,否则返回false
@usage
-- 设置gpio.setup的pull默认值为上拉
gpio.setDefaultPull(1)
*/