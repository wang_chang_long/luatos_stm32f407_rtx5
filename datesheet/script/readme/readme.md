参考内容：
https://blog.csdn.net/qq_22547725/article/details/108992994
https://wiki.luatos.com/api/gpio.html
使用说明
一、在SD卡根目录下需要创建如下几个文件夹
1、fireware:此文件夹下用于存放通过swd下载需要用到的编程算法文件。编程算法文件需要将单片机对应的xxx.FLM文件通过tool/FlashAlgo软件生成xxx.c，然后打开该文件，并将里面的内容生成bin文件排列好，flash_code内容在后，flash_algo内容在前，生成好xxx.bin文件后将文件放入fireware文件夹下。(通过xxx.FLM生成xxx.bin后期需要专门做一个工具)。
2、log:此文件夹下存放输出的PDF文档。
3、luatos:此文件夹下存放编写的luatos脚本程序，其中的sys.luac不用修改，main.luac是用户自己编写的脚本程序。其实用户编写的脚本程序是xxx.lua,但是此时直接放入会占用较大内存，因此需要通过工具luac.exe将xxx.lua编译成xxx.luac,编译方法为：将luac.exe放入脚本文件，然后打开命令行工具，在命令行中输入luac.exe -s -o xxx.lua xxx.luac,这样就生成了xxx.luac,然后将编译好的xxx.luac,放入luatos文件夹并修改名字为main.luac即可。
4、profram:程序文件，xxx.bin文件，此文件夹下存放的就是烧写到目标测试机中的程序文件，直接放入即可。
必须创建后4个文件夹，并且将对应的文件放入指定的文件夹，否则会出错。

二、定制功能函数使用说明



4、万用表
（1）
/*
万用表初始化
@api    multimeter.init(nil)
@return nil
@usage
multimeter.init()
*/

（2）
/*
直流电压测量
@api    multimeter.dcv(nil)
@return int 测量的数值
@usage
dcv = multimeter.dcv()
*/

（3）
/*
直流电流（mA）测量
@api    multimeter.dcma(nil)
@return int 测量的数值
@usage
dcma = multimeter.dcma()
*/
（4）
/*
直流电流（A）测量
@api    multimeter.dca(nil)
@return int 测量的数值
@usage
dca = multimeter.dca()
*/
（5）
/*
电阻测量
@api    multimeter.res(nil)
@return int 测量的数值
@usage
res = multimeter.res()
*/
（6）
/*
通断测量
@api    multimeter.beep(nil)
@return int 测量的数值
@usage
status = multimeter.beep()
*/

5、模拟量输出
（1）
/*
直流电压输出
@api    analog.voltage(vol)
@number 要输出的电压值
@return boolean true:成功  faluse:失败
@usage
status = analog.voltage(1.0) 输出1V电压
*/

（2）
/*
直流电流输出
@api    analog.current(cur)
@number 要输出的电流值
@return boolean true:成功  faluse:失败
@usage
status = analog.current(1.0) 输出1mA电压
*/

6、pdf文档输出
（1）
/*
pdf文档初始化
@api    pdflib.init(nil)
@return nil
@usage
pdflib.init()
*/

（2）
/*
pdf增加一页
@api    pdflib.addpage(nil)
@return boolean 状态
@return int 页编号
@usage
status,page_num = pdflib.addpage()
*/

（3）
/*
pdf获取页大小
@api    pdflib.getsize(nil)
@return boolean,状态
@return int 宽度
@return int 高度
@usage
status,width,hight = pdflib.getsize()
*/

（4）
/*
pdf设置颜色
@api    pdflib.setrgb(pag_num,r,g,b)
@int 页编号
@number 红色
@number 绿色
@number 蓝色
@return boolean,状态
@usage
status = pdflib.setrgb(0,1.0,0.0,0.0) //红色
*/

（5）
/*
pdf开始输入文字
@api    pdflib.begintest(pag_num)
@int 页编号
@return boolean,状态
@usage
status = pdflib.begintest(0) 
*/

（6）
/*
pdf移动书写位置
@api    pdflib.setpos(pag_num,x,y)
@int 页编号
@int 横坐标
@int 纵坐标
@return boolean,状态
@usage
status = pdflib.setpos(0,10,10) 
*/

（7）
/*
pdf设置字体及大小
@api    pdflib.setfont(pag_num,font,size)
@int 页编号
@int 字体
@int 大小
@return boolean,状态
@usage
status = pdflib.setfont(0,"Courier",24) //24号中文字体
*/

（8）
/*
pdf写入文字
@api    pdflib.writetest(pag_num,text)
@int 页编号
@string 文字
@return boolean,状态
@usage
status = pdflib.writetest(0,"写入文字") 
*/

（9）
/*
pdf结束文字写入
@api    pdflib.endtest(pag_num)
@int 页编号
@return boolean,状态
@usage
status = pdflib.endtest(0) 
*/

（10）
/*
pdf写入文字
@api    pdflib.write(pag_num,x,y,text,color)
@int 页编号
@int x 横坐标
@int y 纵坐标
@string 要写入的文字
@int 颜色 1：黑色 2：红色 3：绿色  
@return boolean,状态
@usage
status = pdflib.write(0,10,10,"写入文字",1) 
*/

（11）
/*
pdf保存页面
@api    pdflib.savepage(pag_num)
@int 页编号
@return boolean,状态
@usage
status = pdflib.savepage(0) 
*/

（12）
/*
pdf保存文件
@api    pdflib.savefile(path)
@string 文件路径
@return boolean,状态
@usage
status = pdflib.savefile("0:log/123456789.pdf") 
*/

7、蓝牙
（1）
/*
蓝牙绑定
@api    ble.bond(name)
@string 蓝牙名称
@return boolean 绑定状态
@usage
ble.bond("123456789")
*/

(2)
/*
蓝牙解除绑定
@api    ble.bondc(nil)
@return boolean 解除绑定状态
@usage
ble.bondc()
*/

(3)
/*
蓝牙数据写入
@api    ble.write(data)
@string/zbuff 待写入的数据，如果是zbuff会从指针起始位置开始读
@int 可选，要发送的数据长度，默认全发
@return int 成功的数据长度
@usage
uart.write( "rdy\r\n")
*/

(4)
/*
蓝牙数据读出
@api    ble.read(len)
@int 读取长度,此处长度无效，读取后返回所有接收到的数据
@file/zbuff 可选：文件句柄或zbuff对象
@return string 读取到的数据 / 传入zbuff时，返回读到的长度，并把zbuff指针后移
@usage
uart.read(16)
*/

(5)
/*
注册蓝牙事件回调
@api    ble.on(event, func)
@string 事件名称
@function 回调方法
@return nil 无返回值
@usage
ble.on(1, "receive", function(len)
    local data = ble.read(len)
    log.info("ble", len, data)
end)
*/

