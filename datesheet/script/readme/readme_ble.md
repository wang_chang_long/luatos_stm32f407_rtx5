
7、蓝牙
（1）
/*
蓝牙绑定
@api    ble.bond(name)
@string 蓝牙名称
@return boolean 绑定状态
@usage
ble.bond("123456789")
*/

(2)
/*
蓝牙解除绑定
@api    ble.bondc(nil)
@return boolean 解除绑定状态
@usage
ble.bondc()
*/

(3)
/*
蓝牙数据写入
@api    ble.write(data)
@string/zbuff 待写入的数据，如果是zbuff会从指针起始位置开始读
@int 可选，要发送的数据长度，默认全发
@return int 成功的数据长度
@usage
uart.write( "rdy\r\n")
*/

(4)
/*
蓝牙数据读出
@api    ble.read(len)
@int 读取长度,此处长度无效，读取后返回所有接收到的数据
@file/zbuff 可选：文件句柄或zbuff对象
@return string 读取到的数据 / 传入zbuff时，返回读到的长度，并把zbuff指针后移
@usage
uart.read(16)
*/

(5)
/*
注册蓝牙事件回调
@api    ble.on(event, func)
@string 事件名称
@function 回调方法
@return nil 无返回值
@usage
ble.on(1, "receive", function(len)
    local data = ble.read(len)
    log.info("ble", len, data)
end)
*/
