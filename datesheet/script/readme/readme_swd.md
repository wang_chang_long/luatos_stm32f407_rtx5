3、SWD
(1)、swd接口
PB3		SWD_RESET   
PB4		SWD_SWCLK
PB5		SWD_SWDIO

(2)
/*
SWD初始化函数
@api    swd.init(fireware_name)
@string 固件的名称
@return boolean true:成功  faluse:失败
@usage
status = swd.init("stm32f10x_512.bin")
*/

(3)
/*
SWD程序擦除
@api    swd.erase(addr)
@int 程序擦除起始地址
@return boolean true:成功  faluse:失败
@usage
status = swd.erase(0x08000000)
*/

(4)
/*
SWD程序编程
@api    swd.program(program_name,addr)
@string 程序文件名称
@int 程序下载起始地址
@return boolean true:成功  faluse:失败
@usage
status = swd.program("test_program.bin",0x08000000)
*/

(5)
/*
SWD程序复位
@api    swd.reset(nil)
@return boolean true:成功  faluse:失败
@usage
status = swd.reset()
*/

(6)
/*
SWD一键程序下载
@api    swd.download(fireware_name,pragram_name,addr)
@string 固件名字
@string 程序文件名称
@addr 程序擦除及下载起始地址
@return boolean true:成功  faluse:失败
@usage
status = swd.download("stm32f10x_512.bin","test_program.bin",0x08000000)
*/

(7)大部分场合在swd初始化完成后直接调用swd.download(),即可一键将程序下载。