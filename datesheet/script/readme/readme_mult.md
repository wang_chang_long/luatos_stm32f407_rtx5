4、万用表
（1）
/*
万用表初始化
@api    multimeter.init(nil)
@return nil
@usage
multimeter.init()
*/

（2）
/*
直流电压测量
@api    multimeter.dcv(nil)
@return int 测量的数值
@usage
dcv = multimeter.dcv()
*/

（3）
/*
直流电流（mA）测量
@api    multimeter.dcma(nil)
@return int 测量的数值
@usage
dcma = multimeter.dcma()
*/
（4）
/*
直流电流（A）测量
@api    multimeter.dca(nil)
@return int 测量的数值
@usage
dca = multimeter.dca()
*/
（5）
/*
电阻测量
@api    multimeter.res(nil)
@return int 测量的数值
@usage
res = multimeter.res()
*/
（6）
/*
通断测量
@api    multimeter.beep(nil)
@return int 测量的数值
@usage
status = multimeter.beep()
*/