uart
(1)目前支持的串口为
1    串口1
6    串口6
(2)
/*
配置串口参数
@api    uart.setup(id, baud_rate, data_bits, stop_bits, partiy, bit_order, buff_size)
@int 串口id, uart0写0, uart1写1, 如此类推, 最大值取决于设备
@int 波特率, 默认115200
@int 数据位，默认为8
@int 停止位，默认为1
@int 校验位，可选 uart.None/uart.Even/uart.Odd
@int 大小端，默认小端 uart.LSB, 可选 uart.MSB
@int 缓冲区大小，默认值1024
@int 485模式下的转换GPIO, 默认值0xffffffff
@int 485模式下的rx方向GPIO的电平, 默认值0
@int 485模式下tx向rx转换的延迟时间，默认值12bit的时间，单位us
@return int 成功返回0,失败返回其他值
@usage
-- 最常用115200 8N1
uart.setup(1, 115200, 8, 1, uart.NONE)
-- 可以简写为 uart.setup(1)
*/

(3)
/*
写串口
@api    uart.write(id, data)
@int 串口id, uart0写0, uart1写1
@string/zbuff 待写入的数据，如果是zbuff会从指针起始位置开始读
@int 可选，要发送的数据长度，默认全发
@return int 成功的数据长度
@usage
uart.write(1, "rdy\r\n")
*/

(4)
/*
读串口
@api    uart.read(id, len)
@int 串口id, uart0写0, uart1写1
@int 读取长度
@file/zbuff 可选：文件句柄或zbuff对象
@return string 读取到的数据 / 传入zbuff时，返回读到的长度，并把zbuff指针后移
@usage
uart.read(1, 16)
*/

(5)
/*
关闭串口
@api    uart.close(id)
@int 串口id, uart0写0, uart1写1
@return nil 无返回值
@usage
uart.close(1)
*/

(6)
/*
注册串口事件回调
@api    uart.on(id, event, func)
@int 串口id, uart0写0, uart1写1
@string 事件名称
@function 回调方法
@return nil 无返回值
@usage
uart.on(1, "receive", function(id, len)
    local data = uart.read(id, len)
    log.info("uart", id, len, data)
end)
*/