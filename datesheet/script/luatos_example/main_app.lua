sys = require("sys")
log.info("mem.lua",rtos.meminfo())
uartid = 6
uart.setup(
	uartid,
	115200,
	8,
	1
	)
sys.taskInit(function()
	multimeter.init()
	pdflib.init()
	ret,pagnum = pdflib.addpage()
	if ret then
		log.info("add page",pagnum)
		ret,width,hight = pdflib.getsize(pagnum)
		half_width = math.modf(width/2)
		log.info("pdfsize",width,half_width,hight)
		pdflib.setfont(pagnum,"SimSun",24)
		row = 1
		pdflib.write(pagnum,50,hight-row * 30,"开始测试，设备编号为:123456789")
		pdflib.setfont(pagnum,"SimSun",20)
		row = row +1
		log.info("pdfinit","ok")
	end
	while true do 
		local result,data= sys.waitUntil("test_beep",1000)
		if result then
			log.info("beep","start")
			pdflib.write(pagnum,0,hight-row * 30,"短路测试")
			local measure_val = multimeter.beep()
			if measure_val  then
				log.info("vlatage","short")
				pdflib.write(pagnum,half_width,hight-row * 30,"电源短路",2)
				break;
			else
				log.info("vlatage","normal")
				pdflib.write(pagnum,half_width,hight-row * 30,"电源正常",3)
				break;
			end
		end
	end
	row = row + 1
    while true do
		local result,data= sys.waitUntil("test_vol",1000)
		if result then
		log.info("vol","start")
		pdflib.write(pagnum,0,hight-row * 30,"电压测试")
			local measure_val = multimeter.dcv()
			if (measure_val < 3300000 * 1.1) and (measure_val > 3300000 * 0.9) then
				log.info("measure_dcv ok",measure_val)
				pdflib.write(pagnum,half_width,hight-row * 30,"电源电压正常",3)
				break
			else
				log.info("measure_dcv err",measure_val)
				pdflib.write(pagnum,half_width,hight-row * 30,"电源电压异常",2)
				break
			end
		end
    end
	row = row + 1
	log.info("swd","start download program\r\n")
	pdflib.write(pagnum,0,hight-row * 30,"程序下载")
	ret = swd.download("stm32f10x_512.bin","test_program.bin",0x08000000)
	if ret then
		log.info("task1","swd download ok\r\n")
		pdflib.write(pagnum,half_width,hight-row * 30,"程序下载完成",3)
	else 
		log.info("task1","swd download err\r\n")
		pdflib.write(pagnum,half_width,hight-row * 30,"程序下载失败",2)
	end
	row = row + 1
	uart.write(uartid,"clock test")
	pdflib.write(pagnum,0,hight-row * 30,"时钟测试")
	while true do
		local result,data= sys.waitUntil("test_dev",1000)
		if result then
			log.info("clock",data)
			local first = data:find("\r\nok\r\n")
			if first then
				log.info("clock","ok")
				pdflib.write(pagnum,half_width,hight-row * 30,"时钟正常",3)
				break
			end
			local first = data:find("\r\nerr\r\n")
			if first then
				log.info("clock","err")
				pdflib.write(pagnum,half_width,hight-row * 30,"时钟异常",2)
				break
			end
		end
	end
	pdflib.savepage(pagnum)
	pdflib.savefile("0:log/123456789.pdf")
	log.info("test","test finish")
end)

uart.on(uartid,"receive",function(id,len)
	log.info("uarton",uartid)
	local s = ""
	s = uart.read(id,len)
	if #s > 0 then
		log.info("uarton",uartid)
		local first,tail = s:find("\r\nbeep\r\n")
		if first then
			sys.publish("test_beep")
		end
		first,tail = s:find("\r\nvol\r\n")
		if first then
			sys.publish("test_vol")
		end
		first,tail = s:find("dev_test:")
		if first then
			log.info("uarton",s)
			sys.publish("test_dev",s)
		end
	end
end)

sys.run()